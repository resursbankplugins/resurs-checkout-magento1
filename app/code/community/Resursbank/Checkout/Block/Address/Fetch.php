<?php
/**
 * Copyright 2016 Resurs Bank AB
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 * Address fetch widget.
 *
 * Class Resursbank_Checkout_Block_Address_Fetch
 */
class Resursbank_Checkout_Block_Address_Fetch extends Mage_Core_Block_Template
{
    /**
     * Constructor.
     */
    protected function _construct()
    {
        $this->setTemplate('resursbank/checkout/address/fetch.phtml');

        parent::_construct();
    }

    /**
     * Render block HTML if customer is not logged in, and if the module is
     * configured for Sweden.
     *
     * @return string
     */
    protected function _toHtml()
    {
        $result = '';

        if (!Mage::getSingleton('customer/session')->isLoggedIn() &&
            $this->getCountry() === Resursbank_Checkout_Model_System_Config_Source_Country::SWEDEN
        ) {
            $result = parent::_toHtml();
        }

        return $result;
    }

    /**
     * Returns the chosen country in the config.
     *
     * @return string | null
     */
    public function getCountry()
    {
        return Mage::getStoreConfig('resursbank_checkout/api/country');
    }

    /**
     * Returns the label text for the SSN-field input when the customer is a
     * company.
     *
     * @return string
     */
    public function getLegalLabel()
    {
        return $this->__('Org. number (XXXXXXXXXX)');
    }

    /**
     * Returns the label text for the SSN-field input when the customer is a
     * regular user.
     *
     * @return string
     */
    public function getNaturalLabel()
    {
        return $this->__('SSN (YYYYMMDDXXXX)');
    }

    /**
     * Retrieve SSN stored in session.
     *
     * @return string
     */
    public function getSsn()
    {
        return Mage::getModel('resursbank_checkout/api_adapter_simplified')
            ->getSsn();
    }

    /**
     * Check if customer type selection is enabled.
     *
     * @return bool
     */
    public function customerTypeSelectionEnabled()
    {
        return (bool) Mage::getSingleton('resursbank_checkout/api')
            ->getApiSetting('enable_customer_type', true);
    }

    /**
     * Check if FireCheckout is being used.
     *
     * @return bool
     */
    public function usesFireCheckout()
    {
        return (bool) Mage::getStoreConfigFlag('firecheckout/general/enabled');
    }

    /**
     * Returns the chosen layout for FireCheckout.
     *
     * @return string
     */
    public function getFireCheckoutLayout()
    {
        return (string) Mage::getStoreConfig('firecheckout/general/layout');
    }
}
