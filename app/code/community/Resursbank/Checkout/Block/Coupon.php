<?php
/**
 * Copyright 2016 Resurs Bank AB
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 * Coupon block rendered on checkout page.
 *
 * Class Resursbank_Checkout_Block_Coupon
 */
class Resursbank_Checkout_Block_Coupon extends Mage_Core_Block_Template
{
    /**
     * @var Resursbank_Checkout_Helper_Data
     */
    protected $helper;

    /**
     * Constructor.
     */
    protected function _construct()
    {
        $this->helper = Mage::helper('resursbank_checkout');

        $this->setTemplate('resursbank/checkout/coupon.phtml');

        parent::_construct();
    }

    /**
     * Retrieve applied coupon code.
     *
     * @return string
     */
    public function getCode()
    {
        return (string) $this->helper->getQuote()->getCouponCode();
    }

    /**
     * Retreive currency formatted discount amount.
     *
     * @return string
     */
    public function getDiscountAmount()
    {
        $amount = (float) (
            $this->helper->getQuote()->getSubtotal() -
            $this->helper->getQuote()->getSubtotalWithDiscount()
        );

        return Mage::helper('core')->formatCurrency(-$amount);
    }

    /**
     * Check if coupon code has been applied.
     *
     * @return bool
     */
    public function couponIsApplied()
    {
        return $this->getCode() !== '';
    }
}
