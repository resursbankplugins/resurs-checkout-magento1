<?php
/**
 * Copyright 2016 Resurs Bank AB
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 * This class is used to render the payment session iframe at checkout.
 *
 * Class Resursbank_Checkout_Block_Css
 */
class Resursbank_Checkout_Block_Iframe extends Mage_Core_Block_Template
{
    /**
     * This must be public, otherwise we cannot reach it from the template file.
     *
     * @var Resursbank_Checkout_Helper_Data
     */
    public $helper;

    /**
     * Constructor.
     */
    protected function _construct()
    {
        parent::_construct();

        $this->helper = Mage::helper('resursbank_checkout');
    }

    /**
     * Retrieve session iframe from API singleton.
     *
     * @return string
     */
    public function getIframe()
    {
        return $this->helper->getApiModel()->getSessionIframeHtml();
    }

    /**
     * Check if a payment session has been initialized.
     *
     * @return bool
     */
    public function paymentSessionInitialized()
    {
        $result = false;

        try {
            $result = $this->helper->getApiModel()->paymentSessionInitialized();
        } catch (Exception $e) {
            $result = false;
        }

        return $result;
    }
}
