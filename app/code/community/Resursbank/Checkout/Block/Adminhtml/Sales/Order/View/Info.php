<?php
/**
 * Copyright 2016 Resurs Bank AB
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 * Displays payment information on order view in administration panel.
 *
 * Class Resursbank_Checkout_Block_Adminhtml_Sales_Order_View_Info
 */
class Resursbank_Checkout_Block_Adminhtml_Sales_Order_View_Info extends Mage_Core_Block_Template
{
    /**
     * Payment information.
     *
     * @var array
     */
    private $paymentInfo;

    /**
     * Constructor.
     */
    protected function _construct()
    {
        parent::_construct();

        $this->setTemplate('resursbank/checkout/sales/order/info.phtml');
    }

    /**
     * Only render this view for orders paid using Resurs Bank payment methods.
     *
     * @return string
     */
    protected function _toHtml()
    {
        return $this->getPaymentInformation('id') !== null ?
            parent::_toHtml() :
            '';
    }

    /**
     * Get payment status.
     *
     * @return string
     */
    public function getStatus()
    {
        $result = '';

        $status = $this->getPaymentInformation('status');

        if (is_string($status)) {
            $result = $this->__($status);
        } elseif (is_array($status) && count($status)) {
            foreach ($status as $piece) {
                $result .= $result !== '' ?
                    (' | ' . $this->__($piece)) :
                    $this->__($piece);
            }
        }

        if (empty($result)) {
            $result = $this->__('PENDING');
        }

        return $result;
    }

    /**
     * Retrieve order reference.
     *
     * @return string
     */
    public function getPaymentId()
    {
        return $this->getPaymentInformation('id');
    }

    /**
     * Retrieve payment total.
     *
     * @return float
     */
    public function getPaymentTotal()
    {
        return (float) $this->getPaymentInformation('totalAmount');
    }

    /**
     * Retrieve payment limit.
     *
     * @return float
     */
    public function getPaymentLimit()
    {
        return (float) $this->getPaymentInformation('limit');
    }

    /**
     * Check if payment is frozen.
     *
     * @return bool
     */
    public function isFrozen()
    {
        return ($this->getPaymentInformation('frozen') === true);
    }

    /**
     * Check if payment is fraud marked.
     *
     * @return bool
     */
    public function isFraud()
    {
        return ($this->getPaymentInformation('fraud') === true);
    }

    /**
     * Retrieve payment method name.
     *
     * @return string
     */
    public function getPaymentMethodName()
    {
        return $this->getPaymentInformation('paymentMethodName');
    }

    /**
     * Retrieve full customer name attached to payment.
     *
     * @return mixed
     */
    public function getCustomerName()
    {
        return $this->getCustomerInformation('fullName', true);
    }

    /**
     * Retrieve full customer address attached to payment.
     *
     * @return mixed
     */
    public function getCustomerAddress()
    {
        $street = $this->getCustomerInformation('addressRow1', true);
        $street2 = $this->getCustomerInformation('addressRow2', true);
        $postal = $this->getCustomerInformation('postalCode', true);
        $city = $this->getCustomerInformation('postalArea', true);
        $country = $this->getCustomerInformation('country', true);

        $result = "{$street}<br />";

        if ($street2) {
            $result .= "{$street2}<br />";
        }

        $result .= "{$city}<br />";
        $result .= "{$country} - {$postal}";

        return $result;
    }

    /**
     * Retrieve customer telephone number attached to payment.
     *
     * @return mixed
     */
    public function getCustomerTelephone()
    {
        return $this->getCustomerInformation('telephone');
    }

    /**
     * Retrieve customer email attached to payment.
     *
     * @return mixed
     */
    public function getCustomerEmail()
    {
        return $this->getCustomerInformation('email');
    }

    /**
     * Retrieve customer information from Resursbank payment.
     *
     * @param string $key
     * @param bool $address
     * @return mixed
     */
    public function getCustomerInformation($key = '', $address = false)
    {
        $result = (array) $this->getPaymentInformation('customer');

        if ($address) {
            $result = (is_array($result) && isset($result['address'])) ?
                (array) $result['address'] :
                null;
        }

        if (!empty($key)) {
            $result = isset($result[$key]) ? $result[$key] : null;
        }

        return $result;
    }

    /**
     * Retrieve payment information from Resursbank.
     *
     * @param string $key
     * @return mixed|null|stdClass
     */
    public function getPaymentInformation($key = '')
    {
        $result = null;

        $key = (string) $key;

        if (is_null($this->paymentInfo)) {
            try {
                $this->paymentInfo = (array) Mage::helper('resursbank_checkout')
                    ->getPayment($this->getOrder());
            } catch (Exception $e) {
                // Avoid any further calls to fetch payment information.
                $this->paymentInfo = array();

                // Clear any errors from the session since they will otherwise
                // show up on the next page load.
                Mage::getSingleton('core/session')->getMessages(true);
            }
        }

        if (empty($key)) {
            $result = $this->paymentInfo;
        } elseif ($this->paymentInfoHasKey($key)) {
            $result = $this->paymentInfo[$key];
        }

        return $result;
    }

    /**
     * Check whether or not the paymentInfo object (obtained from the
     * getPaymentInformation method) is an array and has the $key defined.
     *
     * @param string $key
     * @return bool
     */
    protected function paymentInfoHasKey($key)
    {
        return is_array($this->paymentInfo) && isset($this->paymentInfo[$key]);
    }

    /**
     * Attach order object to this block instance.
     *
     * @param Mage_Sales_Model_Order $order
     * @return Varien_Object
     */
    public function setOrder(Mage_Sales_Model_Order $order)
    {
        return $this->setData('order', $order);
    }

    /**
     * @return Mage_Sales_Model_Order
     */
    public function getOrder()
    {
        return $this->_getData('order');
    }
}
