<?php
/**
 * Copyright 2016 Resurs Bank AB
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 * Callback registration button.
 *
 * Class Resursbank_Checkout_Block_Adminhtml_System_Config_Callback_Register
 */
class Resursbank_Checkout_Block_Adminhtml_System_Config_Callback_Register extends Mage_Adminhtml_Block_System_Config_Form_Field
{
    /**
     * Render button to update callback URLs.
     *
     * @param Varien_Data_Form_Element_Abstract $element
     * @return mixed
     */
    protected function _getElementHtml(
        Varien_Data_Form_Element_Abstract $element
    ) {
        $this->setElement($element);

        $url = Mage::helper('resursbank_checkout')->getBackendUrl(
            'adminhtml/checkout_callback/register'
        );

        return $this->getLayout()
            ->createBlock('adminhtml/widget_button')
            ->setType('button')
            ->setClass('scalable')
            ->setLabel(
                Mage::helper('resursbank_checkout/callback')
                    ->__('Callback Registration')
            )
            ->setOnClick("setLocation('$url')")
            ->toHtml();
    }
}
