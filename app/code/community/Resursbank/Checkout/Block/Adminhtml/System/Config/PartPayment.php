<?php
/**
 * Copyright 2016 Resurs Bank AB
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 * Class Resursbank_Checkout_Block_Adminhtml_System_Config_PartPayment
 */
class Resursbank_Checkout_Block_Adminhtml_System_Config_PartPayment extends Mage_Adminhtml_Block_System_Config_Form_Field
{
    /**
     * @var Resursbank_Checkout_Helper_Payment_Method
     */
    private $methodHelper;

    /**
     * @var Resursbank_Checkout_Helper_PartPayment
     */
    private $partPaymentHelper;

    /**
     * @var Resursbank_Checkout_Helper_Log
     */
    private $log;

    /**
     * Set block template. Setup commonly used resources.
     */
    protected function _construct()
    {
        parent::_construct();

        $this->setTemplate(
            'resursbank/checkout/system/config/part-payment.phtml'
        );

        $this->methodHelper = Mage::helper(
            'resursbank_checkout/payment_method'
        );

        $this->partPaymentHelper = Mage::helper(
            'resursbank_checkout/partPayment'
        );

        $this->log = Mage::helper(
            'resursbank_checkout/log'
        );
    }

    /**
     * Render element.
     *
     * @param Varien_Data_Form_Element_Abstract $element
     * @return string
     */
    protected function _getElementHtml(
        Varien_Data_Form_Element_Abstract $element
    ) {
        return $this->_toHtml();
    }

    /**
     * Returns the min and max limit for the chosen payment method.
     *
     * @return string
     */
    public function getChosenPaymentMethodMinMaxLimits()
    {
        $result = [
            'minLimit' => Resursbank_Checkout_Helper_Payment_Method::DEFAULT_MIN_ORDER_TOTAL,
            'maxLimit' => Resursbank_Checkout_Helper_Payment_Method::DEFAULT_MAX_ORDER_TOTAL
        ];

        $paymentMethod = $this->partPaymentHelper->getMethod();

        try {
            if ($paymentMethod->getId()) {
                $result['minLimit'] = $paymentMethod->getMinOrderTotal();
                $result['maxLimit'] = $paymentMethod->getMaxOrderTotal();
            }
        } catch (Exception $e) {
            $this->log->entry($e);
        }

        return json_encode($result);
    }

    /**
     * Returns the default min and max limit.
     *
     * @return string
     */
    public function getDefaultMinMaxLimits()
    {
        return json_encode([
            'minLimit' => Resursbank_Checkout_Helper_Payment_Method::DEFAULT_MIN_ORDER_TOTAL,
            'maxLimit' => Resursbank_Checkout_Helper_Payment_Method::DEFAULT_MAX_ORDER_TOTAL
        ]);
    }
}
