<?php
/**
 * Copyright 2016 Resurs Bank AB
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 * List displaying all registered callbacks.
 *
 * Class Resursbank_Checkout_Block_Adminhtml_System_Config_Callback_List
 */
class Resursbank_Checkout_Block_Adminhtml_System_Config_Callback_List extends Mage_Adminhtml_Block_System_Config_Form_Field
{
    /**
     * Potential error message received while fetching registered callback URLs.
     *
     * @var string
     */
    private $error = '';

    /**
     * Constructor.
     */
    protected function _construct()
    {
        parent::_construct();

        $this->setTemplate(
            'resursbank/checkout/system/config/callback/list.phtml'
        );
    }

    /**
     * Render element.
     *
     * @param Varien_Data_Form_Element_Abstract $element
     * @return string
     */
    protected function _getElementHtml(
        Varien_Data_Form_Element_Abstract $element
    ) {
        return $this->_toHtml();
    }

    /**
     * Retrieve list of all registered callbacks.
     *
     * @return array
     */
    public function getCallbacks()
    {
        $result = array();

        /** @var Resursbank_Checkout_Helper_Callback $helper */
        $helper = Mage::helper('resursbank_checkout/callback');

        try {
            if ($helper->getApiModel()->hasCredentials()) {
                $result = $helper->getApiModel()->getCallbacks();
            }
        } catch (Exception $e) {
            $this->setErrorMessage($helper->__('
                The connection to the payment gateway failed and thus we could 
                not retrieve a list of your callback URLs. Please try again in 
                a few minutes.
            '));
        }

        return is_array($result) ? $result : array();
    }

    /**
     * Set error message received while fetching registered callback URLs.
     *
     * @param string $message
     */
    public function setErrorMessage($message)
    {
        $this->error = (string) $message;
    }

    /**
     * Retrieve error message.
     *
     * @return string
     */
    public function getErrorMessage()
    {
        return (string) $this->error;
    }
}
