<?php
/**
 * Copyright 2016 Resurs Bank AB
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 * This class injects jQuery on legacy installations. jQuery is required by the
 * iframe re-sizer script bundled with the iframe.
 *
 * Class Resursbank_Checkout_Block_Jquery
 */
class Resursbank_Checkout_Block_Jquery extends Mage_Core_Block_Text
{
    /**
     * Retrieve JQuery script tags.
     *
     * @return string
     */
    public function getText()
    {
        $result = '';

        if (Mage::helper('resursbank_checkout')->legacySetup()) {
            $result = (
                '<script type="text/javascript" href="' .
                Mage::getBaseUrl(
                    Mage_Core_Model_Store::URL_TYPE_JS,
                    array('_secure' => true)
                ) .
                'resursbank/checkout/jquery/jquery-1.10.2.min.js"></script>' .
                PHP_EOL .
                '<script type="text/javascript" href="' .
                Mage::getBaseUrl(
                    Mage_Core_Model_Store::URL_TYPE_JS,
                    array('_secure' => true)
                ) .
                'noconflict.js"></script>'
                . PHP_EOL
            );
        }

        return $result;
    }
}
