<?php
/**
 * Copyright 2016 Resurs Bank AB
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 * Main block of checkout page.
 *
 * Class Resursbank_Checkout_Block_Checkout
 */
class Resursbank_Checkout_Block_Catalog_Product_View_PartPayment extends Mage_Core_Block_Template
{
    /**
     * Retrieved product price (we store this value to avoid fetching it too often, it requires loading a stock model).
     *
     * @var float
     */
    protected $productPrice;

    /**
     * @var Resursbank_Checkout_Helper_PartPayment
     */
    protected $helper;

    /**
     * @var Resursbank_Checkout_Helper_Ecom
     */
    protected $ecom;

    /**
     * Resursbank_Checkout_Block_Catalog_Product_View_PartPayment constructor.
     */
    public function __construct()
    {
        parent::__construct();

        $this->helper = Mage::helper('resursbank_checkout/partPayment');
        $this->ecom = Mage::helper('resursbank_checkout/ecom');

        // Cache block output.
        if (Mage::helper('resursbank_checkout/cache')->isCacheEnabled()) {
            $productId = ($this->getProduct() ?
                (int) $this->getProduct()->getId() :
                0
            );

            if ($productId > 0) {
                $this->addData(array(
                    'cache_lifetime' => null,
                    'cache_tags' => array(
                        Resursbank_Checkout_Helper_Cache::CACHE_TAG,
                        (
                            Mage_Catalog_Model_Product::CACHE_TAG .
                            '_' .
                            $productId
                        )
                    ),
                    'cache_key' => (
                        'resursbank_checkout_part_payment_widget_' .
                        $productId
                    )
                ));
            }
        }

        $this->setTemplate(
            'resursbank/checkout/catalog/product/view/part-payment.phtml'
        );
    }

    /**
     * Retrieve current product model
     *
     * @return Mage_Catalog_Model_Product
     */
    public function getProduct()
    {
        return Mage::registry('product');
    }

    /**
     * Retrieve suggested part payment price based on product price.
     *
     * @return string
     */
    public function getSuggestedPartPaymentPrice()
    {
        $result = '';

        $price = $this->getProductPrice();

        if ($price > 0) {
            $suggestedPrice = $this->helper->calculate(
                $price,
                $this->getAnnuityFactor()
            );

            if ($suggestedPrice > 0) {
                $result = Mage::helper('core')->formatCurrency($suggestedPrice);
            }
        }

        return $result;
    }

    /**
     * Get configured annuity factor for part payment price calculations.
     *
     * @return float
     */
    public function getAnnuityFactor()
    {
        return $this->helper->getAnnuity()->getFactor();
    }

    /**
     * Retrieve configured part payment duration.
     *
     * @return string
     */
    public function getDuration()
    {
        return $this->helper->getAnnuity()->getDuration();
    }

    /**
     * Retrieve base price for calculating suggested product price.
     *
     * @return float
     */
    public function getProductPrice()
    {
        $product = null;

        if ($this->productPrice === null) {
            $result = 0;

            if ($this->productTypeIsOneOf(array('simple', 'configurable', 'downloadable', 'virtual'))) {
                /** @var Mage_CatalogInventory_Model_Stock_Item $item */
                $item = Mage::getModel('cataloginventory/stock_item')
                    ->loadByProduct($this->getProduct());

                if ($item->getId()) {
                    $product = $this->getProduct();

                    // Get price including tax.
                    $result = Mage::helper('tax')
                      ->getPrice($product, $product->getFinalPrice(), true);
                }
            } elseif ($this->getProduct()->getTypeId() === 'bundle') {
                $result = Mage::getModel('bundle/product_price')
                    ->getTotalPrices($this->getProduct(), 'min', 1);

                if (!is_numeric($result)) {
                    $result = 0;
                }
            } else {
                $simpleProducts = $this->getProduct()
                    ->getTypeInstance(true)
                    ->getAssociatedProducts($this->getProduct());

                foreach ($simpleProducts as $product) {
                    $result += Mage::helper('tax')
                        ->getPrice($product, $product->getFinalPrice(), true);
                }
            }

            $this->productPrice = (float)$result;
        }

        return $this->productPrice;
    }

    /**
     * Check if the type of the currently loaded product matches one of the
     * provided alternatives.
     *
     * @param array $options
     * @return bool
     */
    protected function productTypeIsOneOf(array $options)
    {
        return in_array($this->getProduct()->getTypeId(), $options);
    }

    /**
     * Get purchase cost HTML.
     *
     * @return string
     * @throws Exception
     */
    public function getPurchaseCostHtml()
    {
        return $this->ecom->getConnection()->getCostOfPurchase(
            $this->helper->getMethodIdentifier(),
            $this->getProductPrice()
        );
    }

    /**
     * Returns the product type as a string.
     *
     * @return string
     */
    public function getProductType()
    {
        return $this->getProduct()->getTypeId();
    }

    /**
     * Returns the code for the configured payment method for part payment.
     *
     * @return string
     */
    public function getConfiguredPaymentMethodCode()
    {
        return (string) $this->helper->getMethod()->getCode();
    }
}
