<?php
/**
 * Copyright 2016 Resurs Bank AB
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 * Contains common methods for all subclasses used to produce forms for Resurs
 * Bank based payment methods.
 *
 * Class Resursbank_Checkout_Block_Payment_Method_Form
 */
class Resursbank_Checkout_Block_Payment_Method_Form extends Mage_Payment_Block_Form
{
    /**
     * Constructor.
     */
    protected function _construct()
    {
        parent::_construct();

        $this->setTemplate('resursbank/checkout/payment/method/form.phtml');
    }

    /**
     * This form is not available when using Hosted Flow, because the values are
     * provided at the gateway instead.
     *
     * @return string
     */
    protected function _toHtml()
    {
        $result = '';

        if ($this->getApiFlow() !== Resursbank_Checkout_Model_System_Config_Source_Flow::FLOW_HOSTED) {
            $result = parent::_toHtml();
        }

        return $result;
    }

    /**
     * Retrieve Simplified Flow API adapter.
     *
     * @return Resursbank_Checkout_Model_Api_Adapter_Simplified
     */
    public function getAdapter()
    {
        return Mage::getSingleton('resursbank_checkout/api_adapter_simplified');
    }

    /**
     * Retrieve array of Form/Component blocks for this form.
     *
     * @return array
     */
    public function getComponents()
    {
        $result = [];

        if ($this->getAdapter()->getIsCompany()) {
            $result[] = $this->getComponent('company_government_id');
            $result[] = $this->getComponent('contact_government_id');
        } else {
            $result[] = $this->getComponent('government_id');

            if ($this->getResursBankMethod()->getRawData('type') === 'CARD') {
                $result[] = $this->getComponent('card');
            }
        }

        $result[] = $this->getComponent('read_more');

        return $result;
    }

    /**
     * Retrieve Resurs Bank Payment Method instance.
     *
     * @return Resursbank_Checkout_Model_Account_Method
     */
    public function getResursBankMethod()
    {
        return $this->getMethod()->getResursBankMethod();
    }

    /**
     * Retrieve instance of Form/Component with the appropriate template set.
     *
     * @param string $component
     * @return Resursbank_Checkout_Block_Payment_Method_Form_Component
     */
    public function getComponent($component)
    {
        $component = (string) $component;

        // Convert snake to camel-case so we can load the appropriate component
        // Block class.
        $class = str_replace(
            ' ',
            '',
            ucwords(str_replace('_', ' ', $component))
        );

        $class[0] = strtolower($class[0]);

        return Mage::getBlockSingleton(
            'resursbank_checkout/payment_method_form_component_' . $class
        )->setMethod($this->getMethod())
            ->setMethodCode($this->getMethodCode())
            ->setTemplate(
                'resursbank/checkout/payment/method/form/component/' .
                 $component .
                '.phtml'
            );
    }

    /**
     * Returns configured API flow.
     *
     * @return string
     */
    private function getApiFlow()
    {
        return (string) Mage::getStoreConfig('resursbank_checkout/api/flow');
    }
}
