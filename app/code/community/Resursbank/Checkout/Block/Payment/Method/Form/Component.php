<?php
/**
 * Copyright 2016 Resurs Bank AB
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 * Contains common methods for all subclasses used to produce forms for Resurs
 * Bank based payment methods.
 *
 * Class Resursbank_Checkout_Block_Payment_Method_Form_Component
 */
class Resursbank_Checkout_Block_Payment_Method_Form_Component extends Mage_Core_Block_Template
{
    /**
     * Gets the current SSN entered by customer from checkout session. Used to
     * inherit data from getAddress to special form fields.
     *
     * @return string
     */
    public function getSsn()
    {
        return Mage::getModel('resursbank_checkout/api_adapter_simplified')
            ->getSsn();
    }

    /**
     * Returns the payment method from the database.
     *
     * @return mixed
     */
    public function getResursBankMethod()
    {
        return $this->getMethod()->getResursBankMethod();
    }
}
