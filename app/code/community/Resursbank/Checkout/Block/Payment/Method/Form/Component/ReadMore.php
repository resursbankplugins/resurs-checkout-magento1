<?php
/**
 * Copyright 2016 Resurs Bank AB
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 * Represents the read more link component.
 *
 * Class Resursbank_Checkout_Block_Payment_Method_Form_Component_ReadMore
 */
class Resursbank_Checkout_Block_Payment_Method_Form_Component_ReadMore extends Resursbank_Checkout_Block_Payment_Method_Form_Component
{
    /**
     * Get purchase cost HTML.
     *
     * @return string
     */
    public function getCostOfPurchaseHtml()
    {
        $result = '';

        /** @var Resursbank_Checkout_Helper_Ecom $ecom */
        $ecom = Mage::helper('resursbank_checkout/ecom');

        try {
            $result = $ecom->getConnection()
                ->getCostOfPurchase(
                    $this->getResursBankMethod()->getRawData('id'),
                    Mage::getSingleton('checkout/session')
                        ->getQuote()
                        ->getGrandTotal()
                );
        } catch (Exception $e) {
            $ecom->debugLog($e);
        }

        return (string) $result;
    }
}
