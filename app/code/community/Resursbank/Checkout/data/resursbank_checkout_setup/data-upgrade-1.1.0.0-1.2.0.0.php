<?php
/**
 * Copyright 2016 Resurs Bank AB
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

$username = Mage::getStoreConfig('resursbank_checkout/api/username');
$password = Mage::getStoreConfig('resursbank_checkout/api/password');

if (is_string($username) && !empty($username)) {
    if (!Mage::getStoreConfig('resursbank_checkout/api/username_test')) {
        Mage::getConfig()->saveConfig(
            'resursbank_checkout/api/username_test',
            $username
        );
    }

    if (!Mage::getStoreConfig('resursbank_checkout/api/username_production')) {
        Mage::getConfig()->saveConfig(
            'resursbank_checkout/api/username_production',
            $username
        );
    }
}

if (is_string($password) && !empty($password)) {
    if (!Mage::getStoreConfig('resursbank_checkout/api/password_test')) {
        Mage::getConfig()->saveConfig(
            'resursbank_checkout/api/password_test',
            $password
        );
    }

    if (!Mage::getStoreConfig('resursbank_checkout/api/password_production')) {
        Mage::getConfig()->saveConfig(
            'resursbank_checkout/api/password_production',
            $password
        );
    }
}
