<?php
/**
 * Copyright 2016 Resurs Bank AB
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 * Class Resursbank_Checkout_Model_Resource_Account_Method_Annuity
 */
class Resursbank_Checkout_Model_Resource_Account_Method_Annuity extends Mage_Core_Model_Resource_Db_Abstract
{
    /**
     * Initialize resource model.
     *
     * @return void
     */
    protected function _construct()
    {
        $this->_init(
            'resursbank_checkout/account_method_annuity',
            'annuity_id'
        );
    }

    /**
     * Modify select statement to include method data with requested annuity.
     *
     * @inheritdoc
     */
    protected function _getLoadSelect($field, $value, $object)
    {
        /** @var string $methodTable */
        $methodTable = $this->getTable('resursbank_checkout/account_method');

        /** @var Zend_Db_Select $select */
        $select = parent::_getLoadSelect($field, $value, $object);

        $select->joinLeft(
            $methodTable,
            $this->getMainTable() . ".method_id = {$methodTable}.method_id"
        );

        return $select;
    }
}
