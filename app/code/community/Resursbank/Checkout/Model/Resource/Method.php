<?php
/**
 * Copyright 2016 Resurs Bank AB
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 * Class Resursbank_Checkout_Model_Resource_Method
 */
class Resursbank_Checkout_Model_Resource_Method extends Mage_Core_Model_Resource_Db_Abstract
{
    /**
     * Initialize resource model
     */
    protected function _construct()
    {
        $this->_init('resursbank_checkout/method', 'id');
    }

    /**
     * Load an object using 'code' field if there's no field specified and value is not numeric.
     *
     * @param Mage_Core_Model_Abstract $object
     * @param mixed $value
     * @param string $field
     * @return Mage_Cms_Model_Resource_Block
     */
    public function load(
        Mage_Core_Model_Abstract $object,
        $value,
        $field = null
    ) {
        if (is_null($field)) {
            $field = 'code';
        }

        // If method code came in prefixed with "resursbank_" then remove it.
        $prefix = Resursbank_Checkout_Model_Payment_Method_Resursbank_Default::CODE_PREFIX;

        if (substr($value, 0, strlen($prefix)) === $prefix) {
            $value = substr($value, strlen($prefix));
        }

        return parent::load($object, $value, $field);
    }
}
