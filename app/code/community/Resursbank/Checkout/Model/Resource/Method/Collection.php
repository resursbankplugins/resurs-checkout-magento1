<?php
/**
 * Copyright 2016 Resurs Bank AB
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 * Class Resursbank_Checkout_Model_Resource_Method_Collection
 */
class Resursbank_Checkout_Model_Resource_Method_Collection extends Mage_Core_Model_Resource_Db_Collection_Abstract
{
    /**
     * Define resource model.
     */
    protected function _construct()
    {
        $this->_init('resursbank_checkout/method');
    }

    /**
     * Returns pairs code - title.
     *
     * @return array
     */
    public function toOptionArray()
    {
        return $this->_toOptionArray('code', 'title');
    }

    /**
     * Retrieve collection as associative array.
     *
     * @return array
     */
    public function toAssocArray()
    {
        $result = array();

        if (!empty($this)) {
            foreach ($this as $item) {
                $result[$item->getData('code')] = $item->getData('title');
            }
        }

        return $result;
    }

    /**
     * This patches code filtering at checkout.
     *
     * @param array|string $field
     * @param null $condition
     * @return Mage_Eav_Model_Entity_Collection_Abstract
     */
    public function addFieldToFilter($field, $condition = null)
    {
        if ($field === 'code') {
            // If method code came in prefixed with "resursbank_" then remove it.
            $prefix = Resursbank_Checkout_Model_Payment_Method_Resursbank_Default::CODE_PREFIX;

            if (substr($condition, 0, strlen($prefix)) === $prefix) {
                $condition = substr($condition, strlen($prefix));
            }
        }

        return parent::addFieldToFilter($field, $condition);
    }
}
