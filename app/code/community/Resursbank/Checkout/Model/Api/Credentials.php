<?php
/**
 * Copyright 2016 Resurs Bank AB
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

//define('ECOM_SKIP_AUTOLOAD', true);
require_once(Mage::getBaseDir() . '/lib/Resursbank/Checkout/vendor/autoload.php');

/**
 * Class Resursbank_Checkout_Model_Api_Credentials
 */
class Resursbank_Checkout_Model_Api_Credentials extends Mage_Core_Model_Abstract
{
    /**
     * @var string
     */
    const ENVIRONMENT_CODE_TEST = 'test';

    /**
     * @var string
     */
    const ENVIRONMENT_CODE_PROD = 'prod';

    /**
     * @var string
     */
    private $username;

    /**
     * @var string
     */
    private $password;

    /**
     * @var int
     */
    private $environment;

    /**
     * Load credentials from store config and apply them on this instance.
     *
     * @param Mage_Core_Model_Store|null $store
     * @return Resursbank_Checkout_Model_Api_Credentials
     */
    public function loadFromConfig(Mage_Core_Model_Store $store = null)
    {
        $env = (string) Mage::getStoreConfig(
            'resursbank_checkout/api/environment',
            $store
        );

        $this->setEnvironment($env)
            ->setUsername(Mage::getStoreConfig(
                'resursbank_checkout/api/username_' . $env,
                $store
            ))->setPassword(Mage::getStoreConfig(
                'resursbank_checkout/api/password_' . $env,
                $store
            ));

        return $this;
    }

    /**
     * @return bool
     */
    public function hasCredentials()
    {
        return (
            $this->getUsername() !== '' &&
            $this->getPassword() !== ''
        );
    }

    /**
     * @param string $username
     * @return Resursbank_Checkout_Model_Api_Credentials
     */
    public function setUsername($username)
    {
        $this->username = (string) $username;

        return $this;
    }

    /**
     * @return string
     */
    public function getUsername()
    {
        return (string) $this->username;
    }

    /**
     * @param string $password
     * @return Resursbank_Checkout_Model_Api_Credentials
     */
    public function setPassword($password)
    {
        $this->password = (string) $password;

        return $this;
    }

    /**
     * @return string
     */
    public function getPassword()
    {
        return (string) $this->password;
    }

    /**
     * @param string|int $environment (test|production, 1|0)
     * @return Resursbank_Checkout_Model_Api_Credentials
     */
    public function setEnvironment($environment)
    {
        if (is_string($environment)) {
            $this->environment = $environment === 'test' ?
                \Resursbank\RBEcomPHP\ResursBank::ENVIRONMENT_TEST :
                \Resursbank\RBEcomPHP\ResursBank::ENVIRONMENT_PRODUCTION;
        } else {
            $this->environment = (int) $environment;
        }

        return $this;
    }

    /**
     * @return int
     */
    public function getEnvironment()
    {
        return (int) $this->environment;
    }

    /**
     * Retrieve hash value based on credentials.
     *
     * @return string
     */
    public function getHash()
    {
        return sha1(
            $this->getUsername() .
            $this->getEnvironment()
        );
    }

    /**
     * Retrieve readable environment code.
     *
     * @return string
     */
    public function getEnvironmentCode()
    {
        return $this->getEnvironment() === \Resursbank\RBEcomPHP\ResursBank::ENVIRONMENT_TEST ?
            self::ENVIRONMENT_CODE_TEST :
            self::ENVIRONMENT_CODE_PROD;
    }

    /**
     * Retrieve readable unique method code suffix.
     *
     * @return string
     */
    public function getMethodSuffix()
    {
        return strtolower(
            $this->getUsername() . '_' . $this->getEnvironmentCode()
        );
    }
}