<?php
/**
 * Copyright 2016 Resurs Bank AB
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 * API calls/actions utilizing Hosted Flow.
 *
 * Class Resursbank_Checkout_Model_Api_Adapter_Hosted
 */
class Resursbank_Checkout_Model_Api_Adapter_Hosted
    extends Resursbank_Checkout_Model_Api_Adapter_Simplified
    implements Resursbank_Checkout_Model_Api_Adapter_AdapterInterface
{
    /**
     * Create payment.
     *
     * @param Mage_Sales_Model_Order $order
     * @return mixed
     * @throws Exception
     */
    public function createPayment(Mage_Sales_Model_Order $order)
    {
        /** @var \Resursbank\RBEcomPHP\ResursBank $connection */
        $connection = Mage::helper('resursbank_checkout/ecom')->getConnection();

        // Make preparations.
        $this->preparePayment($order, $connection);

        return $connection->createPayment(
            $order->getPayment()->getMethodInstance()->getIdentifier()
        );
    }

    /**
     * Check if Hosted Flow API is enabled in config.
     *
     * @return bool
     */
    public function isEnabled()
    {
        return $this->ecom->useHostedFlow();
    }
}
