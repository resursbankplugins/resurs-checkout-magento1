<?php
/**
 * Copyright 2016 Resurs Bank AB
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 * This class is used to structure payment information returned from ECom after
 * creating a payment. Using this we can always be sure the properties we
 * require exist and have the correct type, even if they do not have a value.
 *
 * Class Resursbank_Checkout_Model_Api_Adapter_Simplified_Payment
 */
class Resursbank_Checkout_Model_Api_Adapter_Simplified_Payment extends Mage_Core_Model_Abstract
{
    /**
     * Fill data on this instance using an anonymous stdClass instance.
     *
     * @param stdClass $payment
     * @return $this
     */
    public function fill(stdClass $payment)
    {
        $this->setPaymentId(
            isset($payment->paymentId) ? (string) $payment->paymentId : ''
        )->setbookPaymentStatus(
            isset($payment->bookPaymentStatus) ? (string) $payment->bookPaymentStatus : ''
        )->setSigningUrl(
            isset($payment->signingUrl) ? (string) $payment->signingUrl : ''
        )->setApprovedAmount(
            isset($payment->approvedAmount) ? (float) $payment->approvedAmount : ''
        )->setCustomer(
            isset($payment->customer) ? $payment->customer : ''
        );

        return $this;
    }
}
