<?php
/**
 * Copyright 2016 Resurs Bank AB
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 * API calls/actions utilizing Simplified Flow.
 *
 * Class Resursbank_Checkout_Model_Api_Adapter_Simplified
 */
class Resursbank_Checkout_Model_Api_Adapter_Simplified
    implements Resursbank_Checkout_Model_Api_Adapter_AdapterInterface
{
    /**
     * Temporary storage location in session for company. We need to keep it in
     * memory, without saving it in our database, until the purchase has been
     * completed.
     *
     * @var string
     */
    const SESSION_KEY_IS_COMPANY = 'resursbank_checkout_simplified_is_company';

    /**
     * Temporary storage location in session for SSN. We need to keep it in
     * memory, without saving it in our database, until the purchase has been
     * completed.
     *
     * @var string
     */
    const SESSION_KEY_SSN = 'resursbank_checkout_simplified_ssn';

    /**
     * Temporary storage location in session for contact government id, in case of
     * using LEGAL payment methods. We need to keep it in memory, without saving
     * it in our database, until the purchase has been completed.
     *
     * @var string
     */
    const SESSION_KEY_CONTACT_GOVERNMENT_ID = 'resursbank_checkout_simplified_contact_government_id';

    /**
     * Temporary storage location in session for contact government id, in case of
     * using LEGAL payment methods. We need to keep it in memory, without saving
     * it in our database, until the purchase has been completed.
     *
     * @var string
     */
    const SESSION_KEY_COMPANY_GOVERNMENT_ID = 'resursbank_checkout_simplified_company_government_id';

    /**
     * Temporary storage location in session for card number, when payment methods is
     * based on existing Resurs Bank-cards.
     */
    const SESSION_KEY_CARD_NUMBER = 'resursbank_checkout_simplified_card_number';

    /**
     * Session key where payment id is stored between signing and booking of
     * signed payment.
     *
     * @var string
     */
    const SESSION_PAYMENT_ID = 'resursbank_checkout_simplified_payment_id';

    /**
     * @var Resursbank_Checkout_Helper_Ecom
     */
    protected $ecom;

    /**
     * @var Resursbank_Checkout_Model_Api
     */
    protected $api;

    /**
     * Resursbank_Checkout_Model_Api_Adapter_Simplified constructor.
     */
    public function __construct()
    {
        $this->ecom = Mage::helper('resursbank_checkout/ecom');
        $this->api = Mage::getModel('resursbank_checkout/api');
    }

    /**
     * Retrieve customer address using SSN.
     *
     * @param string $ssn
     * @param string $country
     * @param string $customerType
     * @return Resursbank_Checkout_Model_Api_Adapter_Simplified_Address
     * @throws Exception
     */
    public function getAddress($ssn, $country, $customerType = 'NATURAL')
    {
        /** @var Resursbank_Checkout_Model_Api_Adapter_Simplified_Address $result */
        $result = Mage::getModel(
            'resursbank_checkout/api_adapter_simplified_address'
        );

        $swedenCode =
            Resursbank_Checkout_Model_System_Config_Source_Country::SWEDEN;

        $configCountry =
            Mage::getStoreConfig('resursbank_checkout/api/country');

        // When fetching the address there could be natural errors, if for
        // example you attempt to fetch a Norwegian address when your API
        // account is configured for Sweden. For that reason we do not consider
        // problems while fetching an address to be an actual problem, we simply
        // return an empty address object.
        try {
            if ($country === $swedenCode && $configCountry === $swedenCode) {
                // Retrieve raw address data from the API.
                $raw = $this->ecom->getConnection()->getAddress((string)$ssn, $customerType);

                if (!$raw instanceof stdClass) {
                    $raw = new stdClass();
                }

                // Set address information on address object instance.
                $result->fill($raw, $this->getIsCompany());
            }
        } catch (Exception $e) {
            $result->unsetData();

            throw $e;
        }

        return $result;
    }

    /**
     * Retrieve checkout session.
     *
     * @return Mage_Checkout_Model_Session
     */
    public function getSession()
    {
        return Mage::getSingleton('checkout/session');
    }

    /**
     * Retrieve current quote instance.
     *
     * @return Mage_Sales_Model_Quote
     */
    public function getQuote()
    {
        return $this->getSession()->getQuote();
    }

    /**
     * Store SSN in session for later use.
     *
     * @param string $value
     * @return $this
     */
    public function setSsn($value)
    {
        $this->getSession()->setData(self::SESSION_KEY_SSN, (string)$value);

        return $this;
    }

    /**
     * Retrieve stored SSN.
     *
     * @return string
     */
    public function getSsn()
    {
        return (string)$this->getSession()->getData(self::SESSION_KEY_SSN);
    }

    /**
     * Removes stored SSN.
     */
    public function unsetSsn()
    {
        $this->getSession()->unsetData(self::SESSION_KEY_SSN);
    }

    /**
     * Stores the flag for if the customer is a company or not in the session
     * for later use.
     *
     * @param bool $value
     * @return $this
     */
    public function setIsCompany($value)
    {
        $this->getSession()->setData(self::SESSION_KEY_IS_COMPANY, (bool) $value);

        return $this;
    }

    /**
     * Retrieves the flag for if the customer is a company or not.
     *
     * @return bool
     */
    public function getIsCompany()
    {
        return (bool) $this->getSession()->getData(self::SESSION_KEY_IS_COMPANY);
    }

    /**
     * Removes the flag for if the customer is a company or not from the
     * session.
     */
    public function unsetIsCompany()
    {
        $this->getSession()->unsetData(self::SESSION_KEY_IS_COMPANY);
    }

    /**
     * Create payment.
     *
     * @param Mage_Sales_Model_Order $order
     * @return Resursbank_Checkout_Model_Api_Adapter_Simplified_Payment
     * @throws Exception
     */
    public function createPayment(Mage_Sales_Model_Order $order)
    {
        /** @var Resursbank_Checkout_Model_Api_Adapter_Simplified_Payment $result */
        $result = Mage::getModel(
            'resursbank_checkout/api_adapter_simplified_payment'
        );

        /** @var \Resursbank\RBEcomPHP\ResursBank $connection */
        $connection = Mage::helper('resursbank_checkout/ecom')->getConnection();

        // Make preparations.
        $this->preparePayment($order, $connection);

        // Fill data on payment object.
        $result->fill(
            $connection->createPayment(
                $order->getPayment()->getMethodInstance()->getIdentifier()
            )
        );

        // Handle payment status (like "DENIED" etc.).
        $this->handlePaymentStatus($result);

        return $result;
    }

    /**
     * Book payment after it's been signed by the client.
     *
     * @param string $paymentId
     * @return Resursbank_Checkout_Model_Api_Adapter_Simplified_Payment
     * @throws Exception
     */
    public function bookPayment($paymentId)
    {
        /** @var Resursbank_Checkout_Model_Api_Adapter_Simplified_Payment $result */
        $result = Mage::getModel('resursbank_checkout/api_adapter_simplified_payment');

        // Fill data on payment object.
        $result->fill(
            Mage::helper('resursbank_checkout/ecom')
                ->getConnection()
                ->bookSignedPayment($paymentId)
        );

        // Handle payment status (like "DENIED" etc.).
        $this->handlePaymentStatus(
            $result,
            array('DENIED', 'SIGNING')
        );

        return $result;
    }

    /**
     * Handle payment status. For example, immediately after creating a payment
     * we want to check for potential problems by examining the status the
     * payment has obtained.
     *
     * @param Resursbank_Checkout_Model_Api_Adapter_Simplified_Payment $result
     * @param array $reject Array of status codes indicating failure. Varies
     * depending on process.
     * @return $this
     * @throws Exception
     * @link https://test.resurs.com/docs/x/I4cW Reference for statuses.
     */
    protected function handlePaymentStatus(
        Resursbank_Checkout_Model_Api_Adapter_Simplified_Payment $result,
        array $reject = array('DENIED')
    ) {
        // Handle reject statuses.
        if (in_array($result->getBookPaymentStatus(), $reject)) {
            throw new Mage_Payment_Model_Info_Exception(
                Mage::helper('resursbank_checkout')->__(
                    'Your payment has been rejected, please select a ' .
                    'different payment method and try again. If the problem ' .
                    'persists please contact us for assistance.'
                ),
                400
            );
        }

        return $this;
    }

    /**
     * Prepare payment creation.
     *
     * @param Mage_Sales_Model_Order $order
     * @param \Resursbank\RBEcomPHP\ResursBank $connection
     * @return $this
     * @throws Exception
     */
    public function preparePayment(
        Mage_Sales_Model_Order $order,
        \Resursbank\RBEcomPHP\ResursBank $connection
    ) {
        // Retrieve payment lines from order.
        $paymentOrderLines = Mage::getModel('resursbank_checkout/api')
            ->getPaymentLinesFromOrder($order);

        // Set customer data.
        $this->setCustomerData(
            $order,
            $connection
        )->setCustomerBillingAddress(
            $order->getBillingAddress(),
            $connection
        )->setCustomerShippingAddress(
            $order->getShippingAddress(),
            $connection
        )->addOrderLines(
            $paymentOrderLines,
            $connection
        )->setOrderId(
            $order,
            $connection
        )->setSigningUrls(
            $connection
        )->setBookingAttributes(
            $connection
        );

        return $this;
    }

    /**
     * Update booking attributes for waitForFraudControl, annulIfFrozen and finalizeIfBooked for the simplified flow.
     * Rules of attributes are implemented on booking level, even if they are also controlled from the admin GUI.
     *
     * @param \Resursbank\RBEcomPHP\ResursBank $connection
     * @throws Mage_Core_Model_Store_Exception
     */
    public function setBookingAttributes(
        \Resursbank\RBEcomPHP\ResursBank $connection
    ) {
        $waitForFraud = (bool)$this->api->getApiSetting('wait_for_fraud_control');
        $annulIfFrozen = (bool)$this->api->getApiSetting('annul_if_frozen');

        if (!$waitForFraud && $annulIfFrozen) {
            $annulIfFrozen = false;
        }

        $connection->setFinalizeIfBooked((bool)$this->api->getApiSetting('finalize_if_booked'));
        $connection->setAnnulIfFrozen($annulIfFrozen);
        $connection->setWaitForFraudControl($waitForFraud);
    }

    /**
     * Use order increment_id as payment reference.
     *
     * @param Mage_Sales_Model_Order $order
     * @param \Resursbank\RBEcomPHP\ResursBank $connection
     * @return $this
     * @throws Exception
     */
    public function setOrderId(
        Mage_Sales_Model_Order $order,
        \Resursbank\RBEcomPHP\ResursBank $connection
    ) {
        $connection->setPreferredId($order->getIncrementId());

        return $this;
    }

    /**
     * Set payment signing URLs. This is useful for certain API:s, like
     * Simplified.
     *
     * @param \Resursbank\RBEcomPHP\ResursBank $connection
     * @return $this
     * @throws Exception
     */
    public function setSigningUrls(\Resursbank\RBEcomPHP\ResursBank $connection)
    {
        $connection->setSigning(
            $this->api->getSuccessCallbackUrl(),
            $this->api->getFailureCallbackUrl()
        );

        return $this;
    }

    /**
     * Set customer data through ECom.
     *
     * @param Mage_Sales_Model_Order $order
     * @param \Resursbank\RBEcomPHP\ResursBank $connection
     * @return $this
     * @throws Exception
     */
    public function setCustomerData(
        Mage_Sales_Model_Order $order,
        \Resursbank\RBEcomPHP\ResursBank $connection
    ) {
        $connection->setCustomer(
            (string)$this->getSsn(),
            (string)$order->getBillingAddress()->getTelephone(),
            (string)$order->getBillingAddress()->getTelephone(),
            (string)$order->getCustomerEmail(),
            (string)$this->getCustomerType($order),
            (string)$this->getSession()->getData(self::SESSION_KEY_CONTACT_GOVERNMENT_ID)
        );

        $cardNumber = (string)$this->getSession()->getData(self::SESSION_KEY_CARD_NUMBER);

        if (!empty($cardNumber)) {
            $connection->setCardData($cardNumber);
        }
        return $this;
    }

    /**
     * Set customer billing address information through ECom.
     *
     * @param Mage_Sales_Model_Order_Address $address
     * @param \Resursbank\RBEcomPHP\ResursBank $connection
     * @return $this
     * @throws Exception
     */
    public function setCustomerBillingAddress(
        Mage_Sales_Model_Order_Address $address,
        \Resursbank\RBEcomPHP\ResursBank $connection
    ) {
        $connection->setBillingAddress(
            ($address->getFirstname() . ' ' . $address->getLastname()),
            $address->getFirstname(),
            $address->getLastname(),
            $address->getStreet1(),
            $address->getStreet2(),
            $address->getCity(),
            $address->getPostcode(),
            $address->getCountry()
        );

        return $this;
    }

    /**
     * Set customer shipping address information through ECom.
     *
     * @param Mage_Sales_Model_Order_Address $address
     * @param \Resursbank\RBEcomPHP\ResursBank $connection
     * @return $this
     * @throws Exception
     */
    public function setCustomerShippingAddress(
        Mage_Sales_Model_Order_Address $address,
        \Resursbank\RBEcomPHP\ResursBank $connection
    ) {
        $connection->setDeliveryAddress(
            ($address->getFirstname() . ' ' . $address->getLastname()),
            $address->getFirstname(),
            $address->getLastname(),
            $address->getStreet1(),
            $address->getStreet2(),
            $address->getCity(),
            $address->getPostcode(),
            $address->getCountry()
        );

        return $this;
    }

    /**
     * Get customer type.
     *
     * @param Mage_Sales_Model_Order $order
     * @return string
     */
    protected function getCustomerType(Mage_Sales_Model_Order $order)
    {
        return $this->getIsCompany() ?
            Resursbank_Checkout_Model_Api::CUSTOMER_TYPE_COMPANY :
            Resursbank_Checkout_Model_Api::CUSTOMER_TYPE_PRIVATE;
    }

    /**
     * @param array $paymentLines
     * @param \Resursbank\RBEcomPHP\ResursBank $connection
     * @return Resursbank_Checkout_Model_Api_Adapter_Simplified
     * @throws Exception
     */
    public function addOrderLines(
        array $paymentLines,
        \Resursbank\RBEcomPHP\ResursBank $connection
    ) {
        foreach ($paymentLines as $paymentLine) {
            $connection->addOrderLine(
                $paymentLine['artNo'],
                $paymentLine['description'],
                $paymentLine['unitAmountWithoutVat'],
                $paymentLine['vatPct'],
                $paymentLine['unitMeasure'],
                $paymentLine['type'],
                $paymentLine['quantity']
            );
        }

        return $this;
    }

    /**
     * Check whether or not Simplified Flow API is enabled in config.
     *
     * @return bool
     * @return bool
     */
    public function isEnabled()
    {
        return $this->ecom->useSimplifiedFlow();
    }
}
