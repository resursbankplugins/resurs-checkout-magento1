<?php
/**
 * Copyright 2016 Resurs Bank AB
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 * This class is used to structure address information fetched through ECom.
 * Using this we can always be sure the properties we require exist and have the
 * correct type, even if they do not have a value.
 *
 * Class Resursbank_Checkout_Model_Api_Adapter_Simplified_Address
 */
class Resursbank_Checkout_Model_Api_Adapter_Simplified_Address extends Mage_Core_Model_Abstract
{
    /**
     * Fill data on this instance using an anonymous stdClass instance.
     *
     * @param stdClass $address
     * @param bool $isCompany
     * @return $this
     */
    public function fill(stdClass $address, $isCompany = false)
    {
        $this->setFirstName(
            isset($address->firstName) ? (string) $address->firstName : ''
        )->setLastName(
            isset($address->lastName) ? (string) $address->lastName : ''
        )->setCity(
            isset($address->postalArea) ? (string) $address->postalArea : ''
        )->setPostcode(
            isset($address->postalCode) ? (string) $address->postalCode : ''
        )->setCountry(
            isset($address->country) ? (string) $address->country : ''
        )->setCompany(
            ($isCompany && isset($address->fullName)) ?
                (string) $address->fullName :
                ''
        );

        // Resolve all street lines.
        $this->fillStreet($address);

        return $this;
    }

    /**
     * Fill street lines on this instance using an anonymous stdClass instance.
     *
     * @param stdClass $address
     * @return $this
     */
    public function fillStreet(stdClass $address)
    {
        $street = array();

        // Resolve first street line.
        if (isset($address->addressRow1)) {
            $street[] = (string) $address->addressRow1;
        }

        // Resolve second street line.
        if (isset($address->addressRow2)) {
            $street[] = (string) $address->addressRow2;
        }

        $this->setStreet(
            $street
        );

        return $this;
    }
}
