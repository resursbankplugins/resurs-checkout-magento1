<?php
/**
 * Copyright 2016 Resurs Bank AB
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 * Contains methods to compile payment information from Order object.
 *
 * Class Resursbank_Checkout_Model_Api_Items_Order
 */
class Resursbank_Checkout_Model_Api_Items_Order extends Resursbank_Checkout_Model_Api_Items_General
{
    /**
     * Retrieve order lines used when communicating the Resurs Bank.
     *
     * @param Mage_Sales_Model_Order $order
     * @return array
     * @throws Exception
     */
    public function getPaymentLines(Mage_Sales_Model_Order $order)
    {
        return parent::compilePaymentLines(
            $this->getProductLines($order),
            $this->getDiscountLine($order),
            $this->getShippingLine($order)
        );
    }

    /**
     * Retrieve order line for shipping amount.
     *
     * @param Mage_Sales_Model_Order $order
     * @return array
     * @throws Mage_Core_Model_Store_Exception
     */
    public function getShippingLine(Mage_Sales_Model_Order $order)
    {
        return parent::compileShippingLine(
            $order->getShippingAmount(),
            $this->getShippingTax($order),
            $order->getShippingMethod(),
            $order->getShippingDescription()
        );
    }

    /**
     * Retrieve order line for discount amount.
     *
     * @param Mage_Sales_Model_Order $order
     * @return array
     * @throws Mage_Core_Model_Store_Exception
     */
    public function getDiscountLine(Mage_Sales_Model_Order $order)
    {
        return parent::compileDiscountLine(
            $order->getDiscountAmount(),
            (string) $order->getCouponCode(),
            $this->getDiscountTax($order)
        );
    }

    /**
     * Retrieve array of all order lines.
     *
     * @param Mage_Sales_Model_Order $order
     * @return array
     * @throws Exception
     */
    public function getProductLines(Mage_Sales_Model_Order $order)
    {
        $result = array();

        $items = $order->getAllItems();

        if (!count($items)) {
            throw new Exception(
                $this->helper->__('No items to retrieve payment lines from.')
            );
        }

        /** @var Mage_Sales_Model_Order_Item $item */
        foreach ($items as $item) {
            if ($this->validateProductLine($item)) {
                $result[] = $this->getProductLine($item);
            }
        }

        return $result;
    }

    /**
     * Convert Mage_Sales_Model_Order_Item to an order line for the API.
     *
     * @param Mage_Sales_Model_Order_Item $item
     * @return array
     * @throws Exception
     */
    public function getProductLine(Mage_Sales_Model_Order_Item $item)
    {
        return parent::compileProductLine(
            $item->getSku(),
            $item->getName(),
            $this->getItemQty($item),
            $item->getPrice(),
            $this->getItemTaxPercent($item)
        );
    }

    /**
     * Validate product before including it in product lines sent to Resurs
     * Bank.
     *
     * @param Mage_Sales_Model_Order_Item $item
     * @return bool
     * @throws Exception
     */
    public function validateProductLine(Mage_Sales_Model_Order_Item $item)
    {
        $result = ($this->getItemQty($item) > 0 && !$item->getParentItem());

        if ($item->getProductType() === 'configurable') {
            $result = $item->getChildrenItems();
        }

        return (bool)$result;
    }

    /**
     * Retrieve shipping tax percentage.
     *
     * @param Mage_Sales_Model_Order $order
     * @return float
     */
    public function getShippingTax(Mage_Sales_Model_Order $order)
    {
        return $this->calculateShippingTax(
            $order->getShippingInclTax(),
            $order->getShippingAmount()
        );
    }

    /**
     * Retrieve discount tax percentage.
     *
     * @param Mage_Sales_Model_Order $order
     * @return float
     */
    public function getDiscountTax(Mage_Sales_Model_Order $order)
    {
        return parent::calculateDiscountTax(
            $order->getSubtotalInclTax(),
            $order->getSubtotal()
        );
    }

    /**
     * Retrieve item tax percent. Certain product types will not include the
     * tax_percent property, and in those cases we must calculate it manually
     * ((tax_amount / price) * 100).
     *
     * @param Mage_Sales_Model_Order_Item $item
     * @return float
     */
    public function getItemTaxPercent(Mage_Sales_Model_Order_Item $item)
    {
        return parent::calculateItemTaxPercent(
            $item->hasTaxPercent(),
            $item->getTaxPercent(),
            $item->getPriceInclTax(),
            $item->getPrice()
        );
    }

    /**
     * Get item quantity.
     *
     * @param Mage_Sales_Model_Order_Item $item
     * @return float
     * @throws Exception
     */
    public function getItemQty(Mage_Sales_Model_Order_Item $item)
    {
        return (float) $item->getQtyOrdered();
    }
}
