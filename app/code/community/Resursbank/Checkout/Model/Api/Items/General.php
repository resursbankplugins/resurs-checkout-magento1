<?php
/**
 * Copyright 2016 Resurs Bank AB
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 * Compile payment information for outgoing API calls.
 *
 * Class Resursbank_Checkout_Model_Api_Items_General
 */
class Resursbank_Checkout_Model_Api_Items_General
{
    /**
     * @var Resursbank_Checkout_Helper_Data
     */
    protected $helper;

    /**
     * @var Resursbank_Checkout_Model_Api
     */
    protected $api;

    /**
     * Resursbank_Checkout_Model_Api_Items_General constructor.
     */
    public function __construct()
    {
        $this->helper = Mage::helper('resursbank_checkout');
        $this->api = Mage::getSingleton('resursbank_checkout/api');
    }

    /**
     * Compile item lines in payment information.
     *
     * @param array $productLines
     * @param array $discountLine
     * @param array $shippingLine
     * @return array
     */
    protected function compilePaymentLines(
        array $productLines,
        array $discountLine,
        array $shippingLine
    ) {
        $result = $productLines;

        if (count($discountLine) > 0) {
            $result[] = $discountLine;
        }

        if (count($shippingLine) > 0) {
            $result[] = $shippingLine;
        }

        return is_array($result) ? $result : array();
    }

    /**
     * Retrieve order line for shipping amount.
     *
     * @param float $shippingAmount
     * @param float $shippingTax
     * @param string $shippingMethod
     * @param string $shippingDescription
     * @return array
     * @throws Mage_Core_Model_Store_Exception
     */
    protected function compileShippingLine(
        $shippingAmount,
        $shippingTax,
        $shippingMethod,
        $shippingDescription
    ) {
        $result = array();

        if ($shippingAmount > 0 && is_string($shippingMethod)) {
            $result = array(
                'artNo' => $this->sanitizeArtNo($shippingMethod),
                'description' => $shippingDescription,
                'quantity' => 1,
                'unitMeasure' => $this->api->getApiSetting('weight_unit'),
                'unitAmountWithoutVat' => $shippingAmount,
                'vatPct' => $shippingTax,
                'type' => 'SHIPPING_FEE'
            );
        }

        return $result;
    }

    /**
     * Retrieve order line for discount amount.
     *
     * @param float $discountAmount
     * @param string $couponCode
     * @param float $discountTax
     * @return array
     * @throws Mage_Core_Model_Store_Exception
     */
    protected function compileDiscountLine(
        $discountAmount,
        $couponCode,
        $discountTax
    ) {
        $result = array();

        if ($discountAmount < 0) {
            $name = 'Discount';
            $couponCode = (string) $couponCode;

            if (strlen($couponCode)) {
                $name.= ' (%s)';
            }

            $result = array(
                'artNo' => 'discount' . $this->sanitizeArtNo($couponCode),
                'description' => (string) $this->helper->__($name, $couponCode),
                'quantity' => 1,
                'unitMeasure' => $this->api->getApiSetting('weight_unit'),
                'unitAmountWithoutVat' => $discountAmount,
                'vatPct' => $discountTax,
                'type' => 'DISCOUNT'
            );
        }

        return $result;
    }


    /**
     * Sanitizes an article number, removing illegal characters and returning
     * the resulting string.
     *
     * @param string $artNo
     * @return string
     */
    protected function sanitizeArtNo($artNo)
    {
        return (string) preg_replace("/[^a-z0-9]/", "", strtolower($artNo));
    }

    /**
     * Convert Mage_Sales_Model_Quote_Item to an order line for the API.
     *
     * @param string $sku
     * @param string $name
     * @param float $qty
     * @param float $price
     * @param float $taxPercent
     * @return array
     * @throws Mage_Core_Model_Store_Exception
     */
    protected function compileProductLine(
        $sku,
        $name,
        $qty,
        $price,
        $taxPercent
    ) {
        return array(
            'artNo' => $sku,
            'description' => $name,
            'quantity' => $qty,
            'unitMeasure' => $this->api->getApiSetting('weight_unit'),
            'unitAmountWithoutVat' => $price,
            'vatPct' => $taxPercent,
            'type' => 'ORDER_LINE'
        );
    }

    /**
     * Retrieve discount tax percentage from quote.
     *
     * @param float $subtotalInclTax
     * @param float $subtotal
     * @return float
     */
    protected function calculateDiscountTax($subtotalInclTax, $subtotal)
    {
        $result = 0;

        if ($this->applyTaxAfterDiscount()) {
            if ($subtotalInclTax > $subtotal) {
                // We are expecting a value like 1.25 - 1 (0.25) here.
                $percent = ($subtotalInclTax / $subtotal) - 1;

                if ($percent > 0 && $percent < 1) {
                    $result = round($percent * 100, 4);
                }
            }
        }

        return (float) $result;
    }

    /**
     * Check whether or not taxes are applied after discount has been
     * subtracted from amount.
     *
     * @return bool
     */
    protected function applyTaxAfterDiscount()
    {
        /** @var Mage_Tax_Model_Config $taxConfig */
        $taxConfig = Mage::getSingleton('tax/config');

        return $taxConfig->applyTaxAfterDiscount() &&
            !$taxConfig->priceIncludesTax();
    }

    /**
     * Takes prices with both including and excluding tax, and returns the
     * calculated shipping tax.
     *
     * @param float $inclTax
     * @param float $exclTax
     * @return float
     */
    protected function calculateShippingTax($inclTax, $exclTax)
    {
        $result = 0;

        if ($inclTax > $exclTax) {
            $result = ((($inclTax / $exclTax) - 1) * 100);
        }

        return (float) $result;
    }

    /**
     * Retrieve item tax percent. Certain product types will not include the
     * tax_percent property, and in those cases we must calculate it manually
     * ((tax_amount / price) * 100).
     *
     * @param bool $hasTaxPercent
     * @param float $taxPercent
     * @param float $priceInclTax
     * @param float $price
     * @return float
     */
    protected function calculateItemTaxPercent(
        $hasTaxPercent = false,
        $taxPercent = 0.0,
        $priceInclTax = 0.0,
        $price = 0.0
    ) {
        $result = 0;

        if ($hasTaxPercent && $taxPercent > 0) {
            $result = (float) $taxPercent;
        } else {
            $taxAmount = (float) $priceInclTax - (float) $price;

            if ($taxAmount > 0) {
                $result = (($taxAmount / (float) $price) * 100);
            }
        }

        return (float) $result;
    }
}
