<?php
/**
 * Copyright 2016 Resurs Bank AB
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 * Class Resursbank_Checkout_Model_Account_Method
 */
class Resursbank_Checkout_Model_Account_Method extends Mage_Core_Model_Abstract
{
    /**
     * Define resource model.
     *
     * @return void
     */
    protected function _construct()
    {
        $this->_init('resursbank_checkout/account_method');
    }

    /**
     * Set update_at timestamp before saving entry.
     *
     * @return Mage_Core_Model_Abstract
     */
    protected function _beforeSave()
    {
        $this->setUpdatedAt(Mage::getSingleton('core/date')->gmtDate());

        return parent::_beforeSave();
    }

    /**
     * Check if payment method is active.
     *
     * @return bool
     */
    public function isActive()
    {
        return ((int) $this->_getData('active')) === 1;
    }

    /**
     * Retrieve key from raw data.
     *
     * @param string $key
     * @return mixed|null
     */
    public function getRawData($key)
    {
        $result = null;
        $key = (string) $key;
        $raw = $this->getRaw();

        if ($raw !== '') {
            $raw = @json_decode($raw, true);

            if (is_array($raw) && isset($raw[$key])) {
                $result = $raw[$key];
            }
        }

        return $result;
    }

    /**
     * @return bool
     */
    public function getActive()
    {
        return (int) $this->_getData('active') === 1;
    }

    /**
     * @return string
     */
    public function getCode()
    {
        return (string) $this->_getData('code');
    }

    /**
     * @return float
     */
    public function getMinOrderTotal()
    {
        return (float) $this->_getData('min_order_total');
    }

    /**
     * @return float
     */
    public function getMaxOrderTotal()
    {
        return (float) $this->_getData('max_order_total');
    }
}
