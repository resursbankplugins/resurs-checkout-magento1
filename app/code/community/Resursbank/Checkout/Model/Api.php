<?php
/**
 * Copyright 2016 Resurs Bank AB
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 * Handles API related tasks.
 *
 * Class Resursbank_Checkout_Model_Api
 */
class Resursbank_Checkout_Model_Api extends Mage_Core_Model_Abstract
{
    /**
     * Test API URL.
     *
     * @var string
     */
    const TEST_ENVIRONMENT_URL = 'https://omnitest.resurs.com/';

    /**
     * Production API URL.
     *
     * @var string
     */
    const PRODUCTION_ENVIRONMENT_URL = 'https://checkout.resurs.com/';

    /**
     * Key in checkout/session where we store the payment session id provided
     * by the API.
     *
     * @var string
     */
    const PAYMENT_SESSION_ID_KEY = 'resursbank_checkout_payment_session_id';

    /**
     * Key in checkout/session where we store the resulting iframe provided by
     * the API when we initialize a new session.
     *
     * @var string
     */
    const PAYMENT_SESSION_IFRAME_KEY = 'resursbank_checkout_payment_session_iframe';

    /**
     * Customer type for company.
     *
     * @var string
     */
    const CUSTOMER_TYPE_COMPANY = 'LEGAL';

    /**
     * Customer type for private citizens.
     *
     * @var string
     */
    const CUSTOMER_TYPE_PRIVATE = 'NATURAL';

    /**
     * @var Resursbank_Checkout_Helper_Data
     */
    protected $helper;

    /**
     * @var Resursbank_Checkout_Helper_Callback
     */
    protected $callbackHelper;

    /**
     * @var Resursbank_Checkout_Helper_Ecom
     */
    protected $ecom;

    /**
     * Constructor.
     *
     * Setup commonly used resources.
     */
    protected function _construct()
    {
        parent::_construct();

        $this->helper = Mage::helper('resursbank_checkout');
        $this->callbackHelper = Mage::helper('resursbank_checkout/callback');
        $this->ecom = Mage::helper('resursbank_checkout/ecom');
    }

    /**
     * Initialize payment session.
     *
     * @return stdClass
     * @throws Exception
     */
    public function initPaymentSession()
    {
        // Collect data submitted in the API request.
        $data = array(
            'orderLines' => $this->getPaymentLinesFromQuote($this->getQuote()),
            'successUrl' => $this->getSuccessCallbackUrl(),
            'backUrl' => $this->getFailureCallbackUrl(),
            'shopUrl' => $this->getShopUrl(),
            'customer' => $this->getCustomerInformation()
        );

        // Allows observers to modify the data submitted to the API.
        Mage::dispatchEvent(
            'resursbank_checkout_api_init_session_before',
            array(
                'data' => $data,
                'quote' => $this->getQuote()
            )
        );

        // Perform API request.
        $result = $this->call(
            "checkout/payments/{$this->getPaymentReference(true)}",
            'post',
            $data
        );

        // Allows observers to perform actions based on API response.
        Mage::dispatchEvent(
            'resursbank_checkout_api_init_session_after',
            array(
                'data' => $data,
                'response' => $result,
                'quote' => $this->getQuote()
            )
        );

        // Handle API response.
        $result = json_decode($result);

        if (!is_object($result) ||
            !isset($result->paymentSessionId) ||
            !isset($result->html)
        ) {
            throw new Exception($this->helper->__(
                'Failed to create payment session, unexpected return value.'
            ));
        }

        $this->helper->getCheckoutSession()->setData(
            self::PAYMENT_SESSION_ID_KEY,
            $result->paymentSessionId
        );

        $this->helper->getCheckoutSession()->setData(
            self::PAYMENT_SESSION_IFRAME_KEY,
            $result->html
        );

        return $result;
    }

    /**
     * Update existing payment session.
     *
     * @return Zend_Http_Response
     * @throws Exception
     */
    public function updatePaymentSession()
    {
        if (!$this->paymentSessionInitialized()) {
            throw new Exception(
                "Please initialize your payment session before you updating it."
            );
        }

        $data = array(
            'orderLines' => $this->getPaymentLinesFromQuote($this->getQuote())
        );

        // Allows observers to modify the data submitted to the API.
        Mage::dispatchEvent(
            'resursbank_checkout_api_update_session_before',
            array(
                'data' => $data,
                'quote' => $this->getQuote()
            )
        );

        // Perform API request.
        $result = $this->call(
            "checkout/payments/{$this->getPaymentReference()}",
            'put',
            $data
        );

        // Allows observers to perform actions based on API response.
        Mage::dispatchEvent(
            'resursbank_checkout_api_update_session_after',
            array(
                'data' => $data,
                'response' => $result,
                'quote' => $this->getQuote()
            )
        );

        return $result;
    }

    /**
     * Retrieve completed payment by quote id.
     *
     * @param string $reference
     * @return stdClass
     * @throws Zend_Http_Client_Exception
     */
    public function getPayment($reference)
    {
        // Allows observers to take actions before performing the API request.
        Mage::dispatchEvent(
            'resursbank_checkout_api_get_session_before',
            array(
                'payment_reference' => $reference
            )
        );

        $result = $this->call("checkout/payments/{$reference}", 'get');

        // Allows observers to perform actions based on API response.
        Mage::dispatchEvent(
            'resursbank_checkout_api_get_session_after',
            array(
                'response' => $result,
                'payment_reference' => $reference
            )
        );

        if (!empty($result)) {
            $result = json_decode($result);
        }

        return $result;
    }

    /**
     * Delete active payment session.
     *
     * @return Zend_Http_Response
     * @throws Zend_Http_Client_Exception
     */
    public function deletePaymentSession()
    {
        // Allows observers to take actions before performing the API request.
        Mage::dispatchEvent(
            'resursbank_checkout_api_delete_session_before',
            array(
                'quote' => $this->getQuote()
            )
        );

        $result = $this->call(
            "checkout/payments/{$this->getPaymentReference()}",
            'delete'
        );

        // Allows observers to perform actions based on API response.
        Mage::dispatchEvent(
            'resursbank_checkout_api_delete_session_after',
            array(
                'response' => $result,
                'quote' => $this->getQuote()
            )
        );

        return $result;
    }

    /**
     * Update reference (id) of existing payment.
     *
     * @param string $current
     * @param string $new
     * @return string
     * @throws Exception
     */
    public function updatePaymentReference($current, $new)
    {
        if (!is_string($current) || !is_string($new)) {
            throw new Exception('Payment reference must always be a string.');
        }

        return $this->call(
            "checkout/payments/{$current}/updatePaymentReference",
            'put',
            array(
                'paymentReference' => $new
            )
        );
    }

    /**
     * Register all callback methods.
     *
     * @return $this
     * @throws Exception
     */
    public function registerCallbacks()
    {
        // Generate SALT key.
        Mage::helper('resursbank_checkout/callback')->generateSalt();

        // List of all possible callbacks to register.
        $callbackCollection = array(
            'unfreeze',
            'automatic_fraud_control',
            'annulment',
            'finalization',
            'booked',
            'update',
            'test'
        );

        // Register all callbacks.
        foreach ($callbackCollection as $type) {
            $this->registerCallback($type);
        }

        return $this;
    }

    /**
     * Register callback configuration.
     *
     * Available callbacks:
     *
     *  UNFREEZE
     *  AUTOMATIC_FRAUD_CONTROL
     *  ANNULMENT
     *  FINALIZATION
     *  UPDATE
     *  BOOKED
     *
     * @param string $type
     * @return $this
     * @throws Exception
     */
    public function registerCallback($type)
    {
        $type = strtolower((string)$type);

        /** @var \Resursbank\RBEcomPHP\ResursBank $connection */
        $connection = $this->ecom->getConnection();

        // Workaround for strange issue in ECom callback-salt setup.
        $connection->setCallbackDigestSalt(
            $this->callbackHelper->getSalt()
        );

        // Action on CallbackController we want to be called when this
        // callback is invoked by Resurs Bank.
        $action = ($type === 'automatic_fraud_control') ?
            'automaticFraudControl' :
            $type;

        $urlParams = array(
            '_secure' => true,
            '_store' => Mage::app()->getRequest()->getParam('store')
        );

        if ($type !== 'test') {
            $urlParams['paymentId'] = '{paymentId}';
            $urlParams['digest'] = '{digest}';
        }

        if ($type === 'automatic_fraud_control') {
            $urlParams['result'] = '{result}';
        }

        if ($type === 'test') {
            $urlParams = array(
                'param1' => 'Alpha',
                'param2' => 'Bravo',
                'param3' => 'Charlie',
                'param4' => 'Delta',
                'param5' => 'Echo',
            );
        }

        $urlTemplate = Mage::getUrl(
            "resursbank_checkout/callback/{$action}",
            $urlParams
        );

        // Register callback URL.
        $params = array(
            'uriTemplate' => $urlTemplate,
            'digestConfiguration' => array()
        );

        // Add digest configuration to callback URL parameters.
        if ($type !== 'test') {
            $params['digestConfiguration'] = array(
                'digestAlgorithm' => 'SHA1',
                'digestParameters' => array('paymentId'),
                'digestSalt' => $this->callbackHelper->getSalt()
            );
        }
        
        // Register callback URL through ECom.
        $connection->setRegisterCallback(
            $connection->getCallbackTypeByString($type),
            $params['uriTemplate'],
            $params['digestConfiguration']
        );

        return $this;
    }

    /**
     * Get list of all registered callbacks.
     *
     * @param bool $assoc
     * @return array
     * @throws Zend_Http_Client_Exception|Exception
     */
    public function getCallbacks($assoc = false)
    {
        $result = $this->ecom->getConnection()->getCallBacksByRest();

        if (is_string($result) && !empty($result)) {
            $result = json_decode($result);
        }

        if (!is_array($result)) {
            $result = array();
        }

        // Convert stdClass values.
        if ($assoc && count($result)) {
            $this->convertCallbacksListToAssocArray($result);
        }

        return $result;
    }

    /**
     * Convert the result of callback retrieval from stdClass to an associative
     * array.
     *
     * @param array $callbacks
     */
    public function convertCallbacksListToAssocArray(array &$callbacks)
    {
        if (count($callbacks)) {
            foreach ($callbacks as $key => $callback) {
                if (is_object($callback) &&
                    isset($callback->eventType) &&
                    isset($callback->uriTemplate)
                ) {
                    $callbacks[$callback->eventType] = $callback->uriTemplate;
                    unset($callbacks[$key]);
                }
            }
        }
    }

    /**
     * Retrieve the URL path to a callback on Resursbank servers (the actual
     * callback name should be all uppercase).
     *
     * @param string $type
     * @return string
     */
    public function getCallbackTypePath($type)
    {
        return 'callbacks/' . strtoupper((string)$type);
    }

    /**
     * Perform API call.
     *
     * If it's ever necessary to urlencode data sent to the API please refer to
     * the urlencodeArray() method in Helper\Data.php
     *
     * @param string $action
     * @param string $method (post|put|delete|get)
     * @param string|array $data
     * @return Zend_Http_Response
     * @throws Exception
     * @throws Zend_Http_Client_Exception
     */
    public function call($action, $method, $data = null)
    {
        $result = null;

        // Validate requested method.
        $this->validateCallMethod($method);

        /** @var Resursbank_Checkout_Model_Rest_Client $client */
        $client = $this->prepareClient();

        // Debug log.
        $this->log(
            'Calling URL ' .
            $client->getUri() .
            ' action ' .
            $action .
            ' using method ' .
            $method .
            ' with data ' .
            json_encode($data)
        );

        try {
            // Perform API call.
            $response = $this->handleCall(
                $client,
                $method,
                "/{$action}",
                $data
            );
        } catch (Exception $e) {
            if (!preg_match(
                '/invalid chunk size \\"\\" unable to read chunked body/i',
                $e->getMessage()
            )) {
                // Debug log.
                $this->log($e);

                // Clear the payment session, ensuring that a new session will
                // be started once the API is reachable again.
                $this->helper->clearPaymentSession();

                // Throw the error forward.
                throw $e;
            }
        }

        if (isset($response) && is_object($response)) {
            // Debug log.
            $this->log(
                'API responded with status ' .
                $response->getStatus() .
                ' and body ' .
                json_encode($response->getBody())
            );

            // Handle potential errors.
            $this->handleErrors($response);

            // Set result to response body.
            $result = $response->getBody();
        } else {
            // Debug log.
            $this->log("API responded without any body or status.");
        }

        return $result;
    }

    /**
     * This method should never be called directly, only through call().
     *
     * @param Resursbank_Checkout_Model_Rest_Client $client
     * @param string $method
     * @param string $path
     * @param string|null $data
     * @return Zend_Http_Response
     * @throws Zend_Http_Client_Exception
     */
    private function handleCall(
        Resursbank_Checkout_Model_Rest_Client $client,
        $method,
        $path,
        $data = null
    ) {
        $result = null;

        if ($method === 'post') {
            $result = $client
                ->setCustomEncType(
                    Resursbank_Checkout_Model_Rest_Client::ENC_JSON
                )
                ->restPost($path, $data);
        } elseif ($method === 'put') {
            $result = $client
                ->setCustomEncType(
                    Resursbank_Checkout_Model_Rest_Client::ENC_JSON
                )
                ->restPut($path, $data);
        } elseif ($method === 'get') {
            $result = $client->restGet($path, $data);
        } elseif ($method === 'delete') {
            $result = $client->restDelete($path, $data);
        }

        return $result;
    }

    /**
     * Prepare API client.
     *
     * @return Resursbank_Checkout_Model_Rest_Client
     * @throws Mage_Core_Model_Store_Exception
     * @throws Zend_Http_Client_Exception
     */
    public function prepareClient()
    {
        $client = new Resursbank_Checkout_Model_Rest_Client($this->getApiUrl());
        $client->getHttpClient()
            ->setAuth($this->getUsername(), $this->getPassword())
            ->setHeaders('user-agent', $this->getUserAgent())
            ->setConfig(array(
                'timeout' => 180
            ));

        return $client;
    }

    /**
     * Returns the time in ISO 8601 format.
     *
     * @return string
     */
    public function getUserAgent()
    {
        return 'ResursBankPaymentGatewayForMagento1 ' .
            Mage::helper('resursbank_checkout')->getModuleVersion();

        // --- KEEP THE CODE BELOW UNTIL FURTHER NOTICE ---
        // We are trying to call getVersionFull() which is protected, but it can
        // probably be made public instead. We are waiting for confirmation to
        // remove this piece of code.
//            ' +EComPHP v' . Mage::helper('resursbank_checkout/ecom')
//                ->getConnection()
//                ->getVersionFull();
    }

    /**
     * Validate API request method.
     *
     * @param string $method
     * @return $this
     * @throws Exception
     */
    public function validateCallMethod($method)
    {
        if ($method !== 'get' &&
            $method !== 'put' &&
            $method !== 'post' &&
            $method !== 'delete'
        ) {
            throw new Exception(
                $this->helper->__('Invalid API method requested.')
            );
        }

        return $this;
    }

    /**
     * Retrieve iframe of current payment session from checkout/session.
     *
     * @return string
     */
    public function getSessionIframeHtml()
    {
        return (string)$this->helper->getCheckoutSession()->getData(
            self::PAYMENT_SESSION_IFRAME_KEY
        );
    }

    /**
     * Handle errors on response object.
     *
     * @param Zend_Http_Response $response
     * @return $this
     * @throws Exception
     */
    public function handleErrors(Zend_Http_Response $response)
    {
        if ($response->isError()) {
            // Log the error.
            $this->log((
                $response->getStatus() .
                ': ' .
                $response->getMessage() .
                '. Complete error: ' .
                PHP_EOL .
                $response->getBody()
            ));

            // Get readable error message.
            $error = $this->helper->__(
                'We apologize, an error occurred while communicating with the 
                payment gateway. Please contact us as soon as possible so we can
                review this problem.'
            );

            // Add error to message stack.
            Mage::getSingleton('core/session')->addError($error);

            // Stop script.
            throw new Exception($error);
        }

        return $this;
    }

    /**
     * Retrieve payment session id.
     *
     * @return string
     * @throws Exception
     */
    public function getPaymentSessionId()
    {
        return (string)$this->helper->getCheckoutSession()->getData(
            self::PAYMENT_SESSION_ID_KEY
        );
    }

    /**
     * Retrieve payment reference from active quote.
     *
     * @param bool $refresh
     * @return int
     * @throws Exception
     */
    public function getPaymentReference($refresh = false)
    {
        if (!$this->getQuote()) {
            throw new Exception($this->helper->__('Missing quote object.'));
        }

        return $this->helper->getPaymentReference($this->getQuote(), $refresh);
    }

    /**
     * Shorthand to get current quote object.
     *
     * @return Mage_Sales_Model_Quote
     */
    public function getQuote()
    {
        return $this->helper->getQuote();
    }

    /**
     * Check if a payment session has been initialized.
     *
     * @return bool
     * @throws Exception
     */
    public function paymentSessionInitialized()
    {
        return (bool)$this->getPaymentSessionId();
    }

    /**
     * Get a setting from the API configuration.
     *
     * @param string $key
     * @param bool $flag
     * @return mixed
     * @throws Mage_Core_Model_Store_Exception
     */
    public function getApiSetting($key, $flag = false)
    {
        $key = (string)$key;

        return !$flag ?
            Mage::getStoreConfig(
                "resursbank_checkout/api/{$key}",
                $this->helper->getStoreId()
            ) :
            Mage::getStoreConfigFlag(
                "resursbank_checkout/api/{$key}",
                $this->helper->getStoreId()
            );
    }

    /**
     * Check if API is in development/test mode.
     *
     * @return bool
     * @throws Mage_Core_Model_Store_Exception
     */
    public function isInTestMode()
    {
        return ($this->getApiSetting('environment') !== 'production');
    }

    /**
     * Retrieve API username.
     *
     * @return string
     * @throws Mage_Core_Model_Store_Exception
     */
    public function getUsername()
    {
        return (string)$this->getApiSetting(
            'username_' . $this->getEnvironment()
        );
    }

    /**
     * Retrieve API password.
     *
     * @return string
     * @throws Mage_Core_Model_Store_Exception
     */
    public function getPassword()
    {
        return (string)$this->getApiSetting(
            'password_' . $this->getEnvironment()
        );
    }

    /**
     * Retrieve API environment (test|production).
     *
     * @return string
     * @throws Mage_Core_Model_Store_Exception
     */
    public function getEnvironment()
    {
        return (string)$this->getApiSetting('environment');
    }

    /**
     * Retrieve API URL.
     *
     * @param string $action
     * @return string
     * @throws Mage_Core_Model_Store_Exception
     */
    public function getApiUrl($action = '')
    {
        return $this->isInTestMode() ?
            (self::TEST_ENVIRONMENT_URL . $action) :
            (self::PRODUCTION_ENVIRONMENT_URL . $action);
    }

    /**
     * Retrieve order lines used when communicating with Resurs Bank.
     *
     * @param $order Mage_Sales_Model_Order
     * @return array
     * @throws Exception
     */
    public function getPaymentLinesFromOrder(Mage_Sales_Model_Order $order)
    {
        return Mage::getModel('resursbank_checkout/api_items_order')
            ->getPaymentLines($order);
    }

    /**
     * Retrieve quote lines used when communicating with Resurs Bank.
     *
     * @param $quote Mage_Sales_Model_Quote
     * @return array
     * @throws Exception
     */
    public function getPaymentLinesFromQuote(Mage_Sales_Model_Quote $quote)
    {
        return Mage::getModel('resursbank_checkout/api_items_quote')
            ->getPaymentLines($quote);
    }

    /**
     * Retrieve creditmemo lines used when communicating with Resurs Bank.
     *
     * @param $creditmemo Mage_Sales_Model_Order_Creditmemo
     * @return array
     * @throws Exception
     */
    public function getPaymentLinesFromCreditmemo(
        Mage_Sales_Model_Order_Creditmemo $creditmemo
    ) {
        return Mage::getModel('resursbank_checkout/api_items_creditmemo')
            ->getPaymentLines($creditmemo);
    }


    /**
     * Retrieve customer information used when initializing a payment session.
     *
     * @return array
     */
    public function getCustomerInformation()
    {
        return array(
            'mobile' => (
            $this->getCustomer()->getPrimaryBillingAddress() ?
                $this->getCustomer()->getPrimaryBillingAddress()
                    ->getData('telephone') :
                null
            ),
            'email' => $this->getCustomer()->getData('email')
        );
    }

    /**
     * Get the currently logged in customer.
     *
     * @return Mage_Customer_Model_Customer
     */
    public function getCustomer()
    {
        return Mage::getSingleton('customer/session')->getCustomer();
    }

    /**
     * Retrieve shop url (used for iframe communication, the return value will
     * be the target origin). Basically this is your Magento websites
     * protocol:domain without any trailing slashes. For example
     * http://www.testing.com
     *
     * @return string
     */
    public function getShopUrl()
    {
        return rtrim(
            Mage::getBaseUrl(
                Mage_Core_Model_Store::URL_TYPE_LINK,
                array('_secure' => true)
            ),
            '/'
        );
    }

    /**
     * Retrieve iframe protocol:domain. This is used for iframe communication
     * (from JavaScript). This follows the same rules as getShopUrl() above, so
     * no trailing slashes (e.g https://resursbank.com)
     *
     * @return string
     * @throws Mage_Core_Model_Store_Exception
     */
    public function getIframeUrl()
    {
        return rtrim($this->getApiUrl(), '/');
    }

    /**
     * Retrieve URL for order success callback from API.
     *
     * @return string
     */
    public function getSuccessCallbackUrl() {
        return Mage::getUrl(
            'checkout/onepage/success',
            $this->getCallbackParams()
        );
    }

    /**
     * Retrieve URL for order failure callback from API.
     *
     * @return string
     */
    public function getFailureCallbackUrl() {
        return Mage::getUrl(
            'checkout/onepage/failure',
            $this->getCallbackParams()
        );
    }

    /**
     * Check if we have credentials for the API.
     *
     * @return bool
     * @throws Mage_Core_Model_Store_Exception
     */
    public function hasCredentials()
    {
        $username = $this->getUsername();
        $password = $this->getPassword();

        return (!empty($username) && !empty($password));
    }

    /**
     * Log messages.
     *
     * @param string|Exception $message
     */
    public function log($message)
    {
        $this->helper->debugLog($message, Resursbank_Checkout_Helper_Log::API);
    }

    /**
     * Please see Resursbank_Checkout_Model_Observer_FixSessionData.
     *
     * @return array
     */
    private function getCallbackParams() {
        $params = array('_secure' => true);

        $params['quote_id'] = (int) $this->helper->getCheckoutSession()
            ->getQuoteId();

        return $params;
    }
}
