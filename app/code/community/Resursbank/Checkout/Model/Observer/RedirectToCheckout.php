<?php
/**
 * Copyright 2016 Resurs Bank AB
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 * Handles redirect to Checkout.
 *
 * Class Resursbank_Checkout_Model_Observer_RedirectToCheckout
 */
class Resursbank_Checkout_Model_Observer_RedirectToCheckout
{
    /**
     * @var Resursbank_Checkout_Helper_Data
     */
    protected $helper;

    /**
     * Resursbank_Checkout_Model_Observer_PartPayment constructor.
     *
     * Setup commonly used resources.
     */
    public function __construct()
    {
        $this->helper = Mage::helper('resursbank_checkout');
    }

    /**
     * Redirect incoming calls to Resursbank_Checkout (if necessary).
     *
     * @param Varien_Event_Observer $observer
     * @throws Mage_Core_Model_Store_Exception
     */
    public function execute(Varien_Event_Observer $observer)
    {
        if ($this->helper->isEnabled() && $this->helper->useCheckoutFlow()) {
            // URL to redirect to, if any.
            $redirect = $this->getRedirectUrl(
                $observer->getControllerAction()
            );

            if ($redirect !== '') {
                $this->helper->redirect($redirect);
            }
        }
    }

    /**
     * Get URL to redirect incoming request. Essentially we want to redirect all
     * incoming request aimed against the vanilla cart/checkout to our custom
     * checkout page.
     *
     * @param Mage_Core_Controller_Varien_Action $controller
     * @return string
     */
    public function getRedirectUrl(
        Mage_Core_Controller_Varien_Action $controller
    ) {
        $result = '';

        $module = $controller->getRequest()->getModuleName();

        if ($module === 'checkout') {
            $result = (
                $this->isRedirectAbleCheckoutAction($controller) ||
                $this->isRedirectAbleCartAction($controller)
            ) ? 'resursbank_checkout' :
                '';
        } elseif ($module === 'resursbank_checkout') {
            if ($controller->getRequest()->getControllerName() === 'index' ||
                $controller->getRequest()->getControllerName() === 'cart'
            ) {
                $result = $this->helper->quoteIsUnusable() ?
                    'checkout/cart' :
                    '';
            }
        }

        return $result;
    }

    /**
     * Check if give action is a redirect-able call to the vanilla cart
     * controller.
     *
     * @param Mage_Core_Controller_Varien_Action $controller
     * @return bool
     */
    public function isRedirectAbleCartAction(
        Mage_Core_Controller_Varien_Action $controller
    ) {
        return (
            $controller->getRequest()->getControllerName() === 'cart' &&
            $controller->getRequest()->getActionName() === 'index' &&
            $this->helper->quoteHasItems() &&
            Mage::getStoreConfigFlag(
                'resursbank_checkout/general/redirect_cart_to_checkout'
            )
        );
    }

    /**
     * Check if give action is a redirect-able call to the vanilla
     * checkout/onepage controllers.
     *
     * @param Mage_Core_Controller_Varien_Action $controller
     * @return bool
     */
    public function isRedirectAbleCheckoutAction(
        Mage_Core_Controller_Varien_Action $controller
    ) {
        $controllerName = $controller->getRequest()->getControllerName();

        return (
            $controllerName === 'checkout' ||
            (
                $controllerName === 'onepage' &&
                !in_array(
                    $controller->getRequest()->getActionName(),
                    array('success', 'failure')
                )
            )
        );
    }
}
