<?php
/**
 * Copyright 2016 Resurs Bank AB
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 * When utilising Simplified Flow the following actions will transpire at
 * order placement:
 *
 * 1. Order is submitted.
 * 2. Payment session is created (bookPayment) at Resurs Bank.
 * 3. Client is redirected to Resurs Bank to sign their payment (normally
 * by using Bank ID).
 * 4. Client is redirected back to the success URL (we register the success
 * / failure URLs during step 2).
 * 5. API call bookSignedPayment is submitted to convert payment session to
 * an actual payment at Resurs Bank.
 * 6. The order is completed in Magento.
 *
 * Step 5 / 6 requires some data to be available in the client session, in
 * order to locate the quote and order corresponding with their purchase.
 *
 * This information will always be in the client session, unless they have
 * utilised a cell phone to perform their purchase. Consider a scenario
 * where the client opens their custom web browser (Chrome for example)
 * to assemble a shopping cart and place their order. Upon being asked to
 * sign for their purchase "Using Bank ID on this device" the client clicks
 * the popup to open their Bank ID application. After signing the Bank ID
 * app will open a link to the success page. This link will be opened in the
 * default browser (for example Safari). This means your session data is not
 * available since you performed your purchase using Chrome, and thus step 5 and
 * 6 fail.
 *
 * To circumvent this eventuality we have include the clients quote_id in
 * the success / failure URLs registered in the payment session (see
 * Resursbank_Checkout_Model_Api :: getFailureCallbackUrl() /
 * getSuccessCallbackUrl()). We then add the information back to the
 * client session at pre-dispatch of the success / failure controller
 * actions.
 */
class Resursbank_Checkout_Model_Observer_FixSessionData
{
    /**
     * @param Varien_Event_Observer $observer
     * @throws Exception
     */
    public function execute(Varien_Event_Observer $observer)
    {
        /** @var Resursbank_Checkout_Helper_Data $helper */
        $helper = Mage::helper('resursbank_checkout');

        try {
            $session = $helper->getCheckoutSession();
            $quoteId = (int) Mage::app()->getRequest()->getParam('quote_id');

            if ($quoteId !== 0) {
                /** @var Mage_Sales_Model_Order $order */
                $order = Mage::getModel('sales/order')
                    ->load($quoteId, 'quote_id');

                if ((int) $order->getId() !== 0) {
                    $session->setLastQuoteId($quoteId)
                        ->setLastSuccessQuoteId($quoteId)
                        ->setLastOrderId($order->getId())
                        ->setLastRealOrderId($order->getIncrementId());

                    $session->setData(
                        Resursbank_Checkout_Model_Api_Adapter_Simplified::SESSION_PAYMENT_ID,
                        $order->getIncrementId()
                    );
                }
            }
        } catch (Exception $e) {
            // Log entry.
            $helper->debugLog($e);
        }
    }
}
