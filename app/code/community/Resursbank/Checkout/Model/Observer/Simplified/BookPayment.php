<?php
/**
 * Copyright 2016 Resurs Bank AB
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 * Book a signed payment using ECom.
 *
 * Class Resursbank_Checkout_Model_Observer_Simplified_BookPayment
 */
class Resursbank_Checkout_Model_Observer_Simplified_BookPayment
{
    /**
     * Book signed payment (complete a payment which has already been created
     * at Resurs Bank).
     *
     * @param Varien_Event_Observer $observer
     * @throws Exception
     */
    public function execute(Varien_Event_Observer $observer)
    {
        try {
            /** @var Resursbank_Checkout_Helper_Data $helper */
            $helper = Mage::helper('resursbank_checkout');

            if (Mage::getStoreConfig('resursbank_checkout/api/flow') === Resursbank_Checkout_Model_System_Config_Source_Flow::FLOW_SIMPLIFIED) {
                // Order increment id.
                $paymentId = $this->getPaymentId();

                if ($paymentId !== '') {
                    // Book previously signed payment.
                    Mage::getModel('resursbank_checkout/api_adapter_simplified')
                        ->bookPayment($paymentId);
                }
            }

            // Clear the payment session.
            $helper->clearPaymentSession();
        } catch (Exception $e) {
            // This will execute the RebuildCart observer, which will cancel the
            // order and redirect the client to a new cart based on the content
            // of the failed order.
            $helper->redirect('checkout/onepage/failure');

            // Log entry.
            $helper->debugLog($e);
        }
    }

    /**
     * Retrieve payment id.
     *
     * @return string
     */
    private function getPaymentId()
    {
        return (string) Mage::getSingleton('checkout/session')->getData(
            Resursbank_Checkout_Model_Api_Adapter_Simplified::SESSION_PAYMENT_ID
        );
    }
}
