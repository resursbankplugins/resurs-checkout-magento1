<?php
/**
 * Copyright 2016 Resurs Bank AB
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 * Class Resursbank_Checkout_Model_Observer_Adminhtml_Order_Cancel
 */
class Resursbank_Checkout_Model_Observer_Adminhtml_Order_Cancel
{
    /**
     * @var Resursbank_Checkout_Helper_Ecom
     */
    protected $helper;

    /**
     * Resursbank_Checkout_Model_Observer_Adminhtml_Order_Cancel constructor.
     *
     * Setup commonly used resources.
     */
    public function __construct()
    {
        $this->helper = Mage::helper('resursbank_checkout/ecom');
    }

    /**
     * Annul Resursbank payment after an order has been annulled in Magento.
     *
     * @param Varien_Event_Observer $observer
     * @throws Exception
     */
    public function execute(Varien_Event_Observer $observer)
    {
        /** @var Mage_Sales_Model_Order $order */
        $order = $observer->getOrder();

        if (!($order instanceof Mage_Sales_Model_Order)) {
            throw new Exception('No order object provided.');
        }

        if ($this->helper->isEnabled() &&
            !$this->isOrderEditAction() &&
            (Mage::registry(
                Resursbank_Checkout_Helper_Data::FLAG_AUTOMATIC_ORDER_ANNULMENT
            ) !== true) &&
            $this->helper->validatePayment($order)
        ) {
            try {
                /** @var string $reference */
                $reference = $this->helper
                    ->getPaymentReferenceFromOrder($order);

                /** @var \Resursbank\RBEcomPHP\ResursBank $connection */
                $connection = $this->helper->getConnection();

                // ECom requires this.
                $connection->setPreferredId($reference);

                // This cannot be removed, otherwise annulPayment won't work as
                // getPayment stores necessary information on the helper.

                /** @var \Resursbank\RBEcomPHP\resurs_payment $payment */
                $payment = $connection->getPayment($reference);

                if (!is_object($payment) || !isset($payment->id)) {
                    throw new Exception(
                        'Failed to obtain payment object for reference ' .
                        $reference
                    );
                }

                $this->log("Cancelling payment {$reference}");

                // Annul payment.
                if (!$connection->annulPayment($reference)) {
                    throw new Exception(
                        'Something went wrong while communicating with the ' .
                        'RBECom API.'
                    );
                }

                // Log entry.
                $this->log("Successfully cancelled payment {$reference}");

                // Order history comment.
                $order->addStatusHistoryComment(
                    $this->helper->__("Resursbank: payment annulled.")
                );
            } catch (Exception $e) {
                Mage::getSingleton('core/session')->addError(
                    $this->helper->__('Failed to cancel Resursbank payment.')
                );

                $this->log($e);

                throw $e;
            }
        }
    }

    /**
     * Check whether or not we are currently editing an order.
     *
     * @return bool
     */
    public function isOrderEditAction()
    {
        return (
            Mage::app()->getRequest()->getModuleName() === 'admin' &&
            Mage::app()->getRequest()->getControllerName() === 'sales_order_edit'
        );
    }

    /**
     * Log messages.
     *
     * @param string|Exception $message
     */
    public function log($message)
    {
        $this->helper->debugLog($message, Resursbank_Checkout_Helper_Log::ECOM);
    }
}
