<?php
/**
 * Copyright 2016 Resurs Bank AB
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 * Class Resursbank_Checkout_Model_Observer_Adminhtml_Order_Creditmemo_Save
 */
class Resursbank_Checkout_Model_Observer_Adminhtml_Order_Creditmemo_Save
{
    /**
     * @var Resursbank_Checkout_Helper_Ecom
     */
    protected $helper;

    /**
     * Resursbank_Checkout_Model_Observer_Adminhtml_Order_Creditmemo_Save
     * constructor.
     *
     * Setup commonly used resources.
     */
    public function __construct()
    {
        $this->helper = Mage::helper('resursbank_checkout/ecom');
    }

    /**
     * Credit the Resursbank payment when a creditmemo has been created.
     *
     * @param Varien_Event_Observer $observer
     * @throws Exception
     */
    public function execute(Varien_Event_Observer $observer)
    {
        /** @var Mage_Sales_Model_Order_Creditmemo $creditmemo */
        $creditmemo = $observer->getDataObject();

        if (!($creditmemo instanceof Mage_Sales_Model_Order_Creditmemo)) {
            throw new Exception('No creditmemo object provided.');
        }

        if ($this->helper->isEnabled() &&
            $this->helper->validatePayment($creditmemo->getOrder())
        ) {
            /** @var Resursbank_Checkout_Model_Api $api */
            $api = Mage::getSingleton('resursbank_checkout/api');

            try {
                /** @var string $reference */
                $reference = $this->helper->getPaymentReferenceFromOrder(
                    $creditmemo->getOrder()
                );

                $this->log("Refunding payment {$reference}");

                /** @var \Resursbank\RBEcomPHP\ResursBank $connection */
                $connection = $this->helper->getConnection();

                // ECom requires this.
                $connection->setPreferredId($reference);

                // This cannot be removed, otherwise creditPayment won't work as
                // getPayment stores necessary information on the helper.

                /** @var \Resursbank\RBEcomPHP\resurs_payment $payment */
                $payment = $connection->getPayment($reference);

                if (!is_object($payment) || !isset($payment->id)) {
                    throw new Exception(
                        'Failed to obtain payment object for reference ' .
                        $reference
                    );
                }

                if (!$connection->creditPayment(
                    $reference,
                    $api->getPaymentLinesFromCreditmemo($creditmemo),
                    array(),
                    false,
                    true
                )) {
                    throw new Exception(
                        'Something went wrong while communicating with the ' .
                        'RBECom API.'
                    );
                }

                // Log entry.
                $this->log("Successfully refunded payment {$reference}");

                // Order history comment.
                $creditmemo->getOrder()->addStatusHistoryComment(
                    $this->helper->__("Resursbank: payment refunded.")
                );
            } catch (Exception $e) {
                Mage::getSingleton('core/session')->addError(
                    $this->helper->__('Failed to credit Resursbank payment.')
                );

                $this->log($e);

                throw $e;
            }
        }
    }

    /**
     * Log messages.
     *
     * @param string|Exception $message
     */
    public function log($message)
    {
        $this->helper->debugLog($message, Resursbank_Checkout_Helper_Log::ECOM);
    }
}
