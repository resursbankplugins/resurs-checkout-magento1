<?php
/**
 * Copyright 2016 Resurs Bank AB
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 * Prepend Resurs Bank payment information to order and invoice view.
 *
 * Class Resursbank_Checkout_Model_Observer_Adminhtml_Order_Info
 */
class Resursbank_Checkout_Model_Observer_Adminhtml_Order_Info
{
    /**
     * Prepend Resurs Bank payment information to order and invoice view.
     *
     * @param Varien_Event_Observer $observer
     */
    public function execute(Varien_Event_Observer $observer)
    {
        try {
            /* @var $block Mage_Core_Block_Abstract */
            $block = $observer->getEvent()->getBlock();

            if ($block instanceof Mage_Adminhtml_Block_Sales_Order_View_Info) {
                if ($this->canDisplay()) {
                    $original = $observer->getTransport()->getHtml();
                    $resursbank = Mage::getBlockSingleton('resursbank_checkout/adminhtml_sales_order_view_info')
                        ->setOrder($this->getOrder())
                        ->toHtml();

                    // We have to setup the values in independent variables
                    // before submitting them to "setHtml", otherwise it
                    // distorts the rendering process, because of the toHtml()
                    // call.
                    $observer->getTransport()->setHtml($resursbank . $original);
                }
            }
        } catch (Exception $e) {
            Mage::helper('resursbank_checkout')->debugLog($e->getMessage());
        }
    }

    /**
     * Check whether or not the block containing payment information can be
     * displayed.
     *
     * @return bool
     */
    protected function canDisplay()
    {
        $controller = Mage::app()->getRequest()->getControllerName();

        return (
            (
                $controller === 'sales_order' ||
                $controller === 'sales_order_invoice'
            ) &&
            Mage::app()->getRequest()->getActionName() == 'view'
        );
    }

    /**
     * Retrieve active order object.
     *
     * @return Mage_Sales_Model_Order
     * @throws Exception
     */
    public function getOrder()
    {
        $order = null;

        $controller = Mage::app()->getRequest()->getControllerName();

        if ($controller === 'sales_order') {
            $order = Mage::registry('current_order') ?
                Mage::registry('current_order') :
                Mage::registry('order');
        } elseif ($controller === 'sales_order_invoice') {
            $invoice = Mage::registry('current_invoice') ?
                Mage::registry('current_invoice') :
                Mage::registry('invoice');

            if ($invoice instanceof Mage_Sales_Model_Order_Invoice) {
                $order = $invoice->getOrder();
            }
        }

        if (!($order instanceof Mage_Sales_Model_Order)) {
            throw new Exception('Unable to locate order object.');
        }

        return $order;
    }
}
