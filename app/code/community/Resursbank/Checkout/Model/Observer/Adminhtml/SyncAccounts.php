<?php
/**
 * Copyright 2016 Resurs Bank AB
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 * Class Resursbank_Checkout_Model_Observer_Adminhtml_SyncAccounts
 */
class Resursbank_Checkout_Model_Observer_Adminhtml_SyncAccounts
{
    /**
     * @var Resursbank_Checkout_Helper_Data
     */
    protected $helper;

    /**
     * Setup commonly used resources.
     */
    public function __construct()
    {
        $this->helper = Mage::helper('resursbank_checkout/payment_method');
    }

    /**
     * Synchronize API accounts in our database.
     *
     * @param Varien_Event_Observer $observer
     * @return void
     */
    public function execute(Varien_Event_Observer $observer)
    {
        try {
            /** @var array $data */
            foreach ($this->helper->getCredentialsCollection() as $data) {
                /** @var Resursbank_Checkout_Model_Api_Credentials $credentials */
                $credentials = $data['credentials'];

                /** @var Resursbank_Checkout_Model_Account $account */
                $account = Mage::getModel('resursbank_checkout/account')
                    ->loadByCredentials($credentials);

                if (!$account->getId()) {
                    $account->setUsername(
                        $credentials->getUsername()
                    )->setEnvironment(
                        $credentials->getEnvironmentCode()
                    )->save();
                }
            }
        } catch (Exception $e) {
            $this->helper->debugLog($e);

            Mage::getSingleton('core/session')->addError(
                $this->helper->__('Failed to synchronize account information.')
            );
        }
    }
}
