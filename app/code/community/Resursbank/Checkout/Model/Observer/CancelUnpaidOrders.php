<?php
/**
 * Copyright 2016 Resurs Bank AB
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 * Class Resursbank_Checkout_Model_Observer_CancelUnpaidOrders
 *
 * Cancel all orders that ...
 *
 * 1) ... are new.
 * 2) ... have no recorded payments.
 * 3) ... have no Resurs Bank payment attached to them.
 * 4) ... are more than x number of seconds old (specified by ORDER_TTL).
 */
class Resursbank_Checkout_Model_Observer_CancelUnpaidOrders
{
    /**
     * Order time to live in seconds. The number of seconds a new order without
     * a payment may be kept alive.
     */
    const ORDER_TTL = 3600;

    /**
     * Process orders in accordance with class description.
     *
     * @throws Exception
     */
    public function execute()
    {
        try {
            // Debug log entry.
            Mage::helper('resursbank_checkout/log')
                ->entry('Starting job to cancel unpaid orders.');

            /** @var Resursbank_Checkout_Helper_Ecom $ecom */
            $ecom = Mage::helper('resursbank_checkout/ecom');

            // Check if API is online.
            if (!$ecom->isApiOnline()) {
                throw new Exception('API is offline.');
            }

            /** @var Mage_Sales_Model_Order $order */
            foreach ($this->getOrderCollection() as $order) {
                try {
                    // Set active store id. This is to ensure config values will be
                    // accurately collected (to get the correct API credentials).
                    Mage::helper('resursbank_checkout')->setStoreId(
                        $order->getStoreId()
                    );

                    // Check if the order was created using the same API
                    // credentials we will be using when attempting to collect
                    // the payment. If the user changed their API credentials
                    // between the moment of order placement and this action
                    // the result would otherwise be us not being able to find
                    // the payment, and cancelling an order which probably
                    // should not be cancelled.
                    $accountHash = $order->getData('resursbank_api_account');

                    if ($accountHash !== $this->getApiAccountHash()) {
                        Mage::helper('resursbank_checkout/log')
                            ->entry(
                                'Skipping order ' .
                                $order->getIncrementId() .
                                '. API account has changed.'
                            );

                        continue;
                    }

                    // the validation method will check if there is a payment
                    // matching the order increment_id available at Resurs Bank.
                    // If there is no payment matching our order we will cancel
                    // the order.
                    if (!$ecom->validatePayment($order)) {
                        // Debug log entry (payment didn't exist).
                        Mage::helper('resursbank_checkout/log')
                            ->entry(
                                'Cancelling order ' .
                                $order->getIncrementId()
                            );

                        // Cancel order.
                        $this->cancelOrder($order);
                    } else {
                        // Debug log entry (payment exists).
                        Mage::helper('resursbank_checkout/log')
                            ->entry(
                                'Skipping order ' .
                                $order->getIncrementId() .
                                '. Matching payment found at Resurs Bank.'
                            );
                    }
                } catch (Exception $e) {
                    // Debug log entry, something went wrong.
                    Mage::helper('resursbank_checkout/log')
                        ->entry(
                            'Failed to process order ' .
                            $order->getIncrementId()
                        );
                }
            }
        } catch (Exception $e) {
            // Debug log entry.
            Mage::helper('resursbank_checkout/log')->entry($e);
        }
    }

    /**
     * Retrieve order collection.
     *
     * @return Mage_Sales_Model_Entity_Order_Collection
     * @throws Mage_Core_Model_Store_Exception
     */
    public function getOrderCollection()
    {
        return Mage::getModel('sales/order')
            ->getCollection()
            ->addAttributeToFilter(
                Resursbank_Checkout_Helper_Callback::CALLBACK_RECEIVED_COLUMN,
                array(
                    'eq' => 0
                )
            )->addAttributeToFilter(
                'state',
                'new'
            )->addAttributeToFilter(
                'total_paid',
                array(
                    'null' => true
                )
            )->addAttributeToFilter(
                'grand_total',
                array(
                    'gt' => 0
                )
            )->addAttributeToFilter(
                'resursbank_api_account',
                array(
                    'notnull' => true
                )
            )->addAttributeToFilter(
                'created_at',
                array(
                    'to' => date('Y-m-d H:i:s', time() - self::ORDER_TTL),
                    'date' => true
                )
            );
    }

    /**
     * Retrieve API account based hash.
     *
     * @return string
     * @throws Mage_Core_Model_Store_Exception
     */
    public function getApiAccountHash()
    {
        return Mage::helper('resursbank_checkout')
            ->getApiAccountHash();
    }

    /**
     * Cancel order.
     *
     * @param Mage_Sales_Model_Order $order
     * @throws Exception
     */
    public function cancelOrder(Mage_Sales_Model_Order $order)
    {
        // Add comment explaining why the order was cancelled.
        $order->addStatusHistoryComment(
            Mage::helper('resursbank_checkout')->__(
                'Resurs Bank: order automatically cancelled because no ' .
                'payment exists.'
            )
        );

        // Cancel order.
        $order->cancel();

        // Save changes.
        $order->save();
    }
}
