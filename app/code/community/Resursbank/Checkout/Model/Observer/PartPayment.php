<?php
/**
 * Copyright 2016 Resurs Bank AB
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 * Class Resursbank_Checkout_Model_Observer_PartPayment
 */
class Resursbank_Checkout_Model_Observer_PartPayment
{
    /**
     * On for example the bundled product view the price blocks are included
     * multiple times (for whatever reason). To avoid us injecting the part
     * payment price suggestion widget multiple times we utilise the registry,
     * using this key.
     */
    const UNIQUE_WIDGET_REGISTRY_KEY = 'resursbank_checkout_partpayment_widget';

    /**
     * @var Resursbank_Checkout_Helper_PartPayment
     */
    protected $helper;

    /**
     * Resursbank_Checkout_Model_Observer_PartPayment constructor.
     *
     * Setup commonly used resources.
     */
    public function __construct()
    {
        $this->helper = Mage::helper('resursbank_checkout/partPayment');
    }

    /**
     * This will append the part payment information widget to the original
     * price block displayed on the product view.
     *
     * @param Varien_Event_Observer $observer
     */
    public function execute(Varien_Event_Observer $observer)
    {
        try {
            if ($this->helper->isEnabled()) {
                /* @var Mage_Core_Block_Abstract $block */
                $block = $observer->getEvent()->getBlock();

                if ((
                        (
                        ($block instanceof Mage_Catalog_Block_Product_Price)) ||
                        ($block instanceof Mage_Bundle_Block_Catalog_Product_Price)
                    ) &&
                    (
                        $this->helper->isActivePath('catalog/product/view') ||
                        $this->helper->isActivePath('checkout/cart/configure')
                    )
                ) {
                    /** @var Resursbank_Checkout_Block_Catalog_Product_View_PartPayment $widget */
                    $widget = Mage::app()->getLayout()->createBlock(
                        'resursbank_checkout/catalog_product_view_partPayment'
                    );

                    if ((Mage::registry(self::UNIQUE_WIDGET_REGISTRY_KEY) !== true) &&
                        ($widget instanceof Resursbank_Checkout_Block_Catalog_Product_View_PartPayment) &&
                        in_array(
                            $block->getTemplate(),
                            array(
                                'catalog/product/price.phtml',
                                'bundle/catalog/product/view/price.phtml'
                            )
                        )
                    ) {
                        // Render these outside of the transporters setHtml
                        // function to avoid render distortions.
                        $origHtml = $observer->getTransport()->getHtml();
                        $widgetHtml = $widget->toHtml();

                        $observer->getTransport()->setHtml(
                            $origHtml .
                            $widgetHtml
                        );

                        Mage::register(self::UNIQUE_WIDGET_REGISTRY_KEY, true);
                    }
                }
            }
        } catch (Exception $e) {
            $this->helper->debugLog($e->getMessage());
        }
    }
}
