<?php
/**
 * Copyright 2016 Resurs Bank AB
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 * Update payment session whenever the quote is updated.
 *
 * Class Resursbank_Checkout_Model_Observer_Cart
 */
class Resursbank_Checkout_Model_Observer_Quote_UpdatePaymentSession
{
    /**
     * Update the payment session at Resursbank when the quote is updated,
     * or delete the payment session and clear the checkout/session information
     * if the cart is empty.
     *
     * @param Varien_Event_Observer $observer
     * @throws Mage_Core_Model_Store_Exception
     */
    public function execute(Varien_Event_Observer $observer)
    {
        /** @var Resursbank_Checkout_Helper_Data $helper */
        $helper = Mage::helper('resursbank_checkout');

        if ($helper->isEnabled()) {
            if ($helper->cartIsEmpty()) {
                $helper->deletePaymentSession();
            } else {
                $helper->updatePaymentSession();
                $this->collectSimplifiedFlowValues();
            }
        }
    }

    /**
     * Retrieve checkout session.
     *
     * @return Mage_Checkout_Model_Session
     */
    public function getSession()
    {
        return Mage::getSingleton('checkout/session');
    }

    /**
     * Collect values from Resurs Bank payment method form fields and store them
     * temporarily within our session for later use. We need this information
     * when we create the payment at Resurs Bank after the order has been
     * created.
     *
     * @return $this
     */
    public function collectSimplifiedFlowValues()
    {
        // Pick up post data for our special form fields.
        $data = Mage::app()->getRequest()->getParam('payment');

        if (is_array($data)) {
            foreach ($data as $key => $value) {
                // Convert input name ($key) to session key name.
                $sessionKey = $this->getSessionKey($key);

                if ($sessionKey !== '') {
                    // We require this check because in the event where you've
                    // entered a different SSN already to fetch your address and
                    // manipulated your request to include it within the payment[]
                    // form even though the field hasn't rendered.
                    if ((string)$this->getSession()->getData($sessionKey) === '') {
                        $this->getSession()->setData($sessionKey, (string)$value);
                    }
                }
            }
        }

        return $this;
    }

    /**
     * Basically this converts input name to session key name.
     *
     * @param string $input
     * @return string
     */
    private function getSessionKey($input)
    {
        $result = '';

        switch ((string) $input) {
            case 'contact_government_id':
                $result = Resursbank_Checkout_Model_Api_Adapter_Simplified::SESSION_KEY_CONTACT_GOVERNMENT_ID;
                break;
            case 'company_government_id':
                $result = Resursbank_Checkout_Model_Api_Adapter_Simplified::SESSION_KEY_COMPANY_GOVERNMENT_ID;
                break;
            case 'card_number':
                $result = Resursbank_Checkout_Model_Api_Adapter_Simplified::SESSION_KEY_CARD_NUMBER;
                break;
            case 'government_id':
                $result = Resursbank_Checkout_Model_Api_Adapter_Simplified::SESSION_KEY_SSN;
                break;
        }

        return $result;
    }
}
