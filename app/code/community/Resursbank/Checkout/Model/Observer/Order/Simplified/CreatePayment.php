<?php
/**
 * Copyright 2016 Resurs Bank AB
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 * Create payment at Resurs Bank through Simplified Flow right before the order
 * is created. The payment will later be signed during redirection to the
 * success page.
 *
 * Class Resursbank_Checkout_Model_Observer_Order_Simplified_CreatePayment
 */
class Resursbank_Checkout_Model_Observer_Order_Simplified_CreatePayment
{
    /**
     * Unique flag to indicate that payment creation for an order has already
     * executed.
     *
     * @var string
     */
    const PAYMENT_CREATED = 'resursbank_checkout_order_payment_reference_flag';

    /**
     * @var Resursbank_Checkout_Helper_Data
     */
    protected $helper;

    /**
     * @var Resursbank_Checkout_Helper_Ecom
     */
    protected $ecom;

    /**
     * Resursbank_Checkout_Model_Observer_Order_Simplified_CreatePayment constructor.
     */
    public function __construct()
    {
        $this->helper = Mage::helper('resursbank_checkout');
        $this->ecom = Mage::helper('resursbank_checkout/ecom');
    }

    /**
     * Create payment.
     *
     * @param Varien_Event_Observer $observer
     * @throws Exception
     */
    public function execute(Varien_Event_Observer $observer)
    {
        try {
            /** @var Mage_Sales_Model_Order $order */
            $order = $this->getOrder($observer);

            // Check whether or not this method should execute.
            if ($this->isEnabled($order) && $this->getApiAdapter()->isEnabled()) {
                // Apply payment redirect URL to order.
                $this->prepareSigning($order);

                // Remember what account was used when creating the payment
                // associated with this order.
                $order->setResursbankApiAccount(
                    $this->helper->getApiAccountHash()
                );
            }
        } catch (Exception $e) {
            // Log error.
            $this->helper->debugLog($e, Resursbank_Checkout_Helper_Log::API);
            throw $e;
        }
    }

    /**
     * @return Resursbank_Checkout_Model_Api_Adapter_Simplified
     */
    protected function getApiAdapter()
    {
        return Mage::getModel('resursbank_checkout/api_adapter_simplified');
    }

    /**
     * Retrieve order from observer.
     *
     * @param Varien_Event_Observer $observer
     * @return Mage_Sales_Model_Order
     * @throws Exception
     */
    protected function getOrder(Varien_Event_Observer $observer)
    {
        /** @var Mage_Sales_Model_Order $order */
        $order = $observer->getOrder();

        // Make sure we received an order in our event context.
        if (!$order instanceof Mage_Sales_Model_Order) {
            throw new Exception('Missing order.');
        }

        return $order;
    }

    /**
     * Prepare signing a payment. Basically, we need to tell the checkout to
     * redirect us to the payment gateway where the payment is signed (the
     * client may need to sign with the BankID or similar) and we need to store
     * the resulting paymentId after creating the payment in our checkout
     * session so we can book (complete) the payment when finally redirected
     * back to our success page.
     *
     * @param Mage_Sales_Model_Order $order
     * @throws Exception
     */
    protected function prepareSigning(Mage_Sales_Model_Order $order)
    {
        /** @var Resursbank_Checkout_Model_Api_Adapter_Simplified_Payment $payment */
        $payment = $this->getApiAdapter()->createPayment($order);

        if ($payment->getSigningUrl() !== '') {
            // Redirect us to the payment gateway after order placement.
            $this->getQuote()
                ->getPayment()
                ->getMethodInstance()
                ->setOrderPlaceRedirectUrl($payment->getSigningUrl());

            // Keep the resulting paymentId from creating the payment in mind.
            $this->getCheckoutSession()->setData(
                Resursbank_Checkout_Model_Api_Adapter_Simplified::SESSION_PAYMENT_ID,
                $payment->getPaymentId()
            );
        }
    }

    /**
     * Retrieve quote from checkout session.
     *
     * @return Mage_Sales_Model_Quote
     * @throws Exception
     */
    protected function getQuote()
    {
        $result = $this->getCheckoutSession()->getQuote();

        if (!$result instanceof Mage_Sales_Model_Quote) {
            throw new Exception('Failed to obtain quote from checkout session.');
        }

        return $result;
    }

    /**
     * Retrieve checkout session handler.
     *
     * @return Mage_Checkout_Model_Session
     */
    protected function getCheckoutSession()
    {
        return Mage::getSingleton('checkout/session');
    }

    /**
     * Check whether or not the execute method should process.
     *
     * @param Mage_Sales_Model_Order $order
     * @return bool
     * @throws Mage_Core_Model_Store_Exception
     */
    protected function isEnabled(Mage_Sales_Model_Order $order)
    {
        return (
            $this->helper->isEnabled() &&
            Mage::registry(self::PAYMENT_CREATED) !== true &&
            $order->isObjectNew() &&
            !$order->getOriginalIncrementId() &&
            $this->ecom->validatePaymentMethod($order)
        );
    }
}
