<?php
/**
 * Copyright 2016 Resurs Bank AB
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 * Create payment at Resurs Bank through Hosted Flow right before the order
 * is created. The client will later be redirected to the gateway to complete
 * their payment.
 *
 * Class Resursbank_Checkout_Model_Observer_Order_Hosted_CreatePayment
 */
class Resursbank_Checkout_Model_Observer_Order_Hosted_CreatePayment extends Resursbank_Checkout_Model_Observer_Order_Simplified_CreatePayment
{
    /**
     * @return Resursbank_Checkout_Model_Api_Adapter_Hosted
     */
    protected function getApiAdapter()
    {
        return Mage::getModel('resursbank_checkout/api_adapter_hosted');
    }

    /**
     * Prepare signing a payment. Basically, we need to tell the checkout to
     * redirect us to the gateway where the payment will be completed.
     *
     * @param Mage_Sales_Model_Order $order
     * @throws Exception
     */
    protected function prepareSigning(Mage_Sales_Model_Order $order)
    {
        $url = $this->getApiAdapter()->createPayment($order);

        if (!is_string($url)) {
            throw new Exception('Unable to generate gateway redirect URL.');
        }

        // Redirect us to the payment gateway after order placement.
        $this->getQuote()
            ->getPayment()
            ->getMethodInstance()
            ->setOrderPlaceRedirectUrl($url);
    }
}
