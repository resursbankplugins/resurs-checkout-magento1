<?php
/**
 * Copyright 2016 Resurs Bank AB
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 * Update Resursbank payment reference to be order increment_id and clear
 * payment session after order has been saved.
 *
 * Class Resursbank_Checkout_Model_Observer_Order_UpdatePaymentReference
 */
class Resursbank_Checkout_Model_Observer_Order_UpdatePaymentReference
{
    /**
     * @var string
     */
    const PAYMENT_REFERENCE_UPDATE_FLAG = 'resursbank_checkout_order_payment_reference_flag';
    
    /**
     * Update payment reference (id) and clear payment session after order
     * placement.
     *
     * @param Varien_Event_Observer $observer
     * @throws Exception
     */
    public function execute(Varien_Event_Observer $observer)
    {
        /** @var Resursbank_Checkout_Helper_Data $helper */
        $helper = Mage::helper('resursbank_checkout');
        
        if ($helper->isEnabled() &&
            Mage::registry(self::PAYMENT_REFERENCE_UPDATE_FLAG) !== true &&
            Mage::getStoreConfig('resursbank_checkout/api/flow') === Resursbank_Checkout_Model_System_Config_Source_Flow::FLOW_CHECKOUT
        ) {
            /** @var Mage_Sales_Model_Order $order */
            $order = $observer->getOrder();

            if (($order instanceof Mage_Sales_Model_Order) &&
                $order->isObjectNew()
                && !$order->getOriginalIncrementId()
            ) {
                try {
                    // Ensure this doesn't run multiple times.
                    Mage::register(self::PAYMENT_REFERENCE_UPDATE_FLAG, true);

                    // Change payment reference from a random token string to
                    // the actual order id.
                    $helper->getApiModel()->updatePaymentReference(
                        $helper->getQuote()->getData(
                            Resursbank_Checkout_Helper_Data::QUOTE_TOKEN_FIELD
                        ),
                        $order->getIncrementId()
                    );

                    // Remember what account was used when creating the payment
                    // associated with this order.
                    $order->setResursbankApiAccount(
                        $helper->getApiAccountHash()
                    );

                    // Clear payment session.
                    if ($helper->getApiModel()->paymentSessionInitialized()) {
                        $helper->clearPaymentSession();
                    }
                } catch (Exception $e) {
                    // Log error.
                    $helper->debugLog($e, Resursbank_Checkout_Helper_Log::API);

                    throw $e;
                }
            }
        }
    }
}
