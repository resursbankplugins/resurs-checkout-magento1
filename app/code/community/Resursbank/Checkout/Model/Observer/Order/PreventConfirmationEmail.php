<?php
/**
 * Copyright 2016 Resurs Bank AB
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 * Prevents order confirmation email from being submitted during order
 * placement. It will be sent after we have received the "booked" callback
 * from Resursbank, letting us know payment can be debited.
 *
 * Class Resursbank_Checkout_Model_Observer_Order
 */
class Resursbank_Checkout_Model_Observer_Order_PreventConfirmationEmail
{
    /**
     * @param Varien_Event_Observer $observer
     * @throws Mage_Core_Model_Store_Exception
     */
    public function execute(Varien_Event_Observer $observer)
    {
        if (Mage::helper('resursbank_checkout')->isEnabled()) {
            /** @var Mage_Sales_Model_Order $order */
            $order = $observer->getOrder();

            if (($order instanceof Mage_Sales_Model_Order) &&
                $order->isObjectNew()
            ) {
                // Avoid sending an order confirmation email until the payment
                // has actually gone through.
                $order->setCanSendNewEmailFlag(false);
            }
        }
    }
}
