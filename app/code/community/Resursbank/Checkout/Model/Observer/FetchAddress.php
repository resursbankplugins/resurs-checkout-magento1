<?php
/**
 * Copyright 2016 Resurs Bank AB
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 * Append fetch address widget HTML to address section in Magento vanilla
 * checkout.
 *
 * Class Resursbank_Checkout_Model_Observer_FetchAddress
 */
class Resursbank_Checkout_Model_Observer_FetchAddress
{
    /**
     * Simply to ensure this block will only render once (regardless of what
     * checkout module is being utilized) we use the registry to keep track of
     * when the widget has been rendered.
     *
     * @var string
     */
    const UNIQUE_WIDGET_REGISTRY_KEY = 'resursbank_checkout_fetch_address_widget';

    /**
     * @var Resursbank_Checkout_Helper_Data
     */
    protected $helper;

    /**
     * Resursbank_Checkout_Model_Observer_FetchAddress constructor.
     *
     * Setup commonly used resources.
     */
    public function __construct()
    {
        $this->helper = Mage::helper('resursbank_checkout');
    }

    /**
     * This will append a widget to fetch address information to the address
     * section on the vanilla checkout page.
     *
     * @param Varien_Event_Observer $observer
     */
    public function execute(Varien_Event_Observer $observer)
    {
        try {
            if ($this->helper->isEnabled() &&
                Mage::registry(self::UNIQUE_WIDGET_REGISTRY_KEY) !== true
            ) {
                /* @var Mage_Core_Block_Abstract $block */
                $block = $observer->getEvent()->getBlock();

                if ($block instanceof Mage_Checkout_Block_Onepage_Billing) {
                    /** @var Resursbank_Checkout_Block_Address_Fetch $widget */
                    $widget = Mage::app()->getLayout()->createBlock(
                        'resursbank_checkout/address_fetch'
                    );

                    $observer->getTransport()->setHtml(
                        $observer->getTransport()->getHtml() .
                        $widget->toHtml()
                    );

                    Mage::register(self::UNIQUE_WIDGET_REGISTRY_KEY, true);
                }
            }
        } catch (Exception $e) {
            $this->helper->debugLog($e->getMessage());
        }
    }
}
