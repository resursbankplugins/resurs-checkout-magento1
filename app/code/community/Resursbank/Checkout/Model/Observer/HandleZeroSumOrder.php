<?php
/**
 * Copyright 2016 Resurs Bank AB
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 * Handle zero sum orders.
 *
 * Class Resursbank_Checkout_Model_Observer_HandleZeroSumOrder
 */
class Resursbank_Checkout_Model_Observer_HandleZeroSumOrder
{
    /**
     * This method will simulate the event executed when we receive the "booked"
     * callback. We need to do this for 0-sum orders because no payment will
     * have been created at Resurs Bank, and therefore no callbacks will be
     * submitted to the store, which means potentially critical sub-processes
     * (like exporting orders to external systems automatically) may fail.
     *
     * @param Varien_Event_Observer $observer
     */
    public function execute(Varien_Event_Observer $observer)
    {
        /** @var Resursbank_Checkout_Helper_Callback $helper */
        $helper = Mage::helper('resursbank_checkout/callback');

        try {
            /** @var Mage_Sales_Model_Order $order */
            $order = $this->getOrder();

            if ((float) $order->getGrandTotal() === 0.0) {
                /** @var Resursbank_Checkout_Model_Callback $callbackModel */
                $callbackModel = Mage::getModel('resursbank_checkout/callback');

                // Run same action as "booked" callback.
                $callbackModel->booked(
                    $order->getIncrementId(),
                    strtoupper(
                        sha1($order->getIncrementId() . $helper->getSalt())
                    )
                );

                // Set order status to "processing".
                $callbackModel->setOrderStatus(
                    $order->getIncrementId(),
                    'confirmed'
                );
            }
        } catch (Exception $e) {
            $helper->debugLog($e);
        }
    }

    /**
     * Retrieve order instance from last order id.
     *
     * @return Mage_Sales_Model_Order
     * @throws Exception
     */
    private function getOrder()
    {
        /** @var int $orderId */
        $orderId = (int) Mage::getModel('checkout/session')
            ->getLastOrderId();

        if (!$orderId) {
            throw new Exception(
                'Could not find order.'
            );
        }

        /** @var Mage_Sales_Model_Order $order */
        $order = Mage::getModel('sales/order')->load($orderId);

        if (!($order instanceof Mage_Sales_Model_Order) || !$order->getId()) {
            throw new Exception("Failed to load order with id {$orderId}");
        }

        return $order;
    }
}
