<?php
/**
 * Copyright 2016 Resurs Bank AB
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 * Rebuild cart upon payment failure.
 *
 * Class Resursbank_Checkout_Model_Observer_RebuildCart
 */
class Resursbank_Checkout_Model_Observer_RebuildCart
{
    /**
     * @param Varien_Event_Observer $observer
     * @throws Exception
     */
    public function execute(Varien_Event_Observer $observer)
    {
        try {
            /** @var Resursbank_Checkout_Helper_Data $helper */
            $helper = Mage::helper('resursbank_checkout');

            /** @var int $orderId */
            $orderId = (int) Mage::getModel('checkout/session')
                ->getLastOrderId();

            if (!$orderId) {
                throw new Exception(
                    'Could not find previous order to rebuild cart contents from.'
                );
            }

            /** @var Mage_Sales_Model_Order $order */
            $order = Mage::getModel('sales/order')->load($orderId);

            if (!($order instanceof Mage_Sales_Model_Order) || !$order->getId()) {
                throw new Exception("Failed to load order with id {$orderId}");
            }

            // Rebuild cart form order.
            $helper->rebuildCart($order);

            /** @var Mage_Checkout_Model_Type_Onepage $onepage */
            $onepage = Mage::getSingleton('checkout/type_onepage');

            // Set previous shipping method and coupon code.
            $onepage->saveShippingMethod($order->getShippingMethod());
            $onepage->getQuote()->setCouponCode($order->getCouponCode());
            $onepage->getQuote()->collectTotals()->save();

            // Cancel previous order.
            $order->cancel()->save();

            // Add error message explaining the payment failed but they may try
            // a different payment method.
            Mage::getSingleton('checkout/session')->addError(
                $helper->__(
                    'The payment failed. Please confirm the cart content and ' .
                    'try a different payment method.'
                )
            );
        } catch (Exception $e) {
            Mage::getSingleton('checkout/session')->addError(
                $helper->__(
                    'The payment failed and the cart could not be rebuilt. ' .
                    'Please add the items back to your cart manually and try ' .
                    'a different payment alternative. We sincerely apologies ' .
                    'for this inconvenience.'
                )
            );

            $helper->debugLog($e);
        }

        // Redirect to cart page.
        $helper->redirect('checkout/cart');
    }
}
