<?php
/**
 * Copyright 2016 Resurs Bank AB
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 * Implements custom config collector for dynamic payment methods (see the method description of getConfig() for more
 * information).
 *
 * Class Resursbank_Checkout_Model_Rewrite_Core_Store
 */
class Resursbank_Checkout_Model_Rewrite_Core_Store extends Mage_Core_Model_Store
{
    /**
     * This function overrides the default implementation of getConfig() method.
     * The reason we are overriding this is because we cannot override methods
     * in Mage_Core_Model_Config, and thus inject XML code for our dynamic
     * payment methods while the information from config.xml files is being
     * collected.
     *
     * Ideally we want to inject the XML for our dynamic payment methods during
     * the collection sequence, and thus follow Magento's native behaviour
     * without needing to modify our config.xml file.
     *
     * This should be safe, since we are using call_user_func_array() to contact
     * the parent function after applying our custom logic. By doing that we
     * ensure that we pass the same arguments forward, so if our logic does not
     * apply the method should execute just as it normally would.
     *
     * While the overwrite should be safe we should find a better way of doing
     * this in the future.
     *
     * @param string $path
     * @return mixed
     */
    public function getConfig($path)
    {
        $result = null;

        if (preg_match('/payment\/(resursbank_)(?!default).*/', $path)) {
            $result = $this->getConfigValue($path);
        } else {
            $result = call_user_func_array('parent::getConfig', func_get_args());

            if ($path === 'payment') {
                try {
                    $result = array_merge(
                        $result,
                        $this->methodHelper()->getConfigData()
                    );
                } catch (Exception $e) {
                    Mage::helper('resursbank_checkout')->debugLog($e);
                }
            }
        }

        return $result;
    }

    /**
     * Retrieve custom config value for generated payment method.
     *
     * @param string $path
     * @return null
     */
    public function getConfigValue($path)
    {
        $result = null;

        $methods = $this->methodHelper()->getConfigData();
        $matches = array();

        if (preg_match('/payment\/(resursbank\_.*)\/(.*)/', $path, $matches)) {
            if (count($matches) === 3) {
                if (isset($methods[$matches[1]][$matches[2]])) {
                    $result = $methods[$matches[1]][$matches[2]];
                }
            }
        }

        return $result;
    }

    /**
     * @return Resursbank_Checkout_Helper_Payment_Method
     */
    private function methodHelper()
    {
        return Mage::helper('resursbank_checkout/payment_method');
    }
}
