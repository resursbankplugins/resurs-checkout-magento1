<?php
/**
 * Copyright 2016 Resurs Bank AB
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 * List of available countries for the API.
 *
 * Class Resursbank_Checkout_Model_System_Config_Source_Country
 */
class Resursbank_Checkout_Model_System_Config_Source_Country
{
    /**
     * @var string
     */
    const SWEDEN = 'SE';

    /**
     * @var string
     */
    const NORWAY = 'NO';

    /**
     * @var string
     */
    const FINLAND = 'FI';

    /**
     * @var string
     */
    const DENMARK = 'DK';

    /**
     * @var Resursbank_Checkout_Helper_Data
     */
    protected $helper;

    /**
     * Resursbank_Checkout_Model_System_Config_Source_Flow constructor.
     */
    public function __construct()
    {
        $this->helper = Mage::helper('resursbank_checkout');
    }

    /**
     * Options getter.
     *
     * @return array
     */
    public function toOptionArray()
    {
        return array(
            array(
                'value' => self::SWEDEN,
                'label' => $this->helper->__('Sweden')
            ),
            array(
                'value' => self::NORWAY,
                'label' => $this->helper->__('Norway')
            ),
            array(
                'value' => self::FINLAND,
                'label' => $this->helper->__('Finland')
            ),
            array(
                'value' => self::DENMARK,
                'label' => $this->helper->__('Denmark')
            )
        );
    }

    /**
     * Get options in "key-value" format.
     *
     * @return array
     */
    public function toArray()
    {
        return array(
            self::SWEDEN => $this->helper->__('Sweden'),
            self::NORWAY => $this->helper->__('Norway'),
            self::FINLAND => $this->helper->__('Finland'),
            self::DENMARK => $this->helper->__('Denmark')
        );
    }
}
