<?php
/**
 * Copyright 2016 Resurs Bank AB
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 * Class Resursbank_Checkout_Model_System_Config_Source_Environment
 */
class Resursbank_Checkout_Model_System_Config_Source_Environment
{
    /**
     * @var Resursbank_Checkout_Helper_Data
     */
    protected $helper;

    /**
     * Resursbank_Checkout_Model_System_Config_Source_Shipping_Availability
     * constructor.
     *
     * Setup commonly used resources.
     */
    public function __construct()
    {
        $this->helper = Mage::helper('resursbank_checkout');
    }

    /**
     * Options getter.
     *
     * @return array
     */
    public function toOptionArray()
    {
        return array(
            array(
                'value' => 'test',
                'label' => $this->helper->__('Test')
            ),
            array(
                'value' => 'production',
                'label' => $this->helper->__('Production')
            ),
        );
    }

    /**
     * Get options in "key-value" format.
     *
     * @return array
     */
    public function toArray()
    {
        return array(
            'test' => $this->helper->__('Test'),
            'production' => $this->helper->__('Production'),
        );
    }
}
