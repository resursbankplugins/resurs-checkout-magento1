<?php
/**
 * Copyright 2016 Resurs Bank AB
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 * Class Resursbank_Checkout_Model_System_Config_Source_Flow
 */
class Resursbank_Checkout_Model_System_Config_Source_Flow
{
    /**
     * Checkout Flow identifier.
     */
    const FLOW_CHECKOUT = 'checkout';

    /**
     * Simplified Flow identifier.
     */
    const FLOW_SIMPLIFIED = 'simplified';

    /**
     * Hosted Flow identifier.
     */
    const FLOW_HOSTED = 'hosted';

    /**
     * @var Resursbank_Checkout_Helper_Data
     */
    protected $helper;

    /**
     * Resursbank_Checkout_Model_System_Config_Source_Flow constructor.
     *
     * Setup shopflows
     */
    public function __construct()
    {
        $this->helper = Mage::helper('resursbank_checkout');
    }

    /**
     * Options getter.
     *
     * @return array
     */
    public function toOptionArray()
    {
        return array(
            array(
                'value' => 'checkout',
                'label' => 'Checkout'
            ),
            array(
                'value' => 'simplified',
                'label' => 'Simplified'
            ),
            array(
                'value' => 'hosted',
                'label' => 'Hosted'
            )
        );
    }

    /**
     * Get options in "key-value" format.
     *
     * @return array
     */
    public function toArray()
    {
        return array(
            'checkout' => 'Checkout',
            'simplified' => 'Simplified',
            'hosted' => 'Hosted'
        );
    }
}
