<?php
/**
 * Copyright 2016 Resurs Bank AB
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 * Class Resursbank_Checkout_Model_System_Config_Source_PartPayment_Duration
 */
class Resursbank_Checkout_Model_System_Config_Source_PartPayment_Duration
{
    /**
     * Cache id for annuity factor collection used for this setting.
     *
     * @var string
     */
    const CACHE_ID = 'resursbank_checkout_setting_partpayment_methods_annuities';

    /**
     * Options getter.
     *
     * @param int $methodId
     * @return array
     */
    public function toOptionArray($methodId = 0)
    {
        $result = array();

        foreach ($this->toArray($methodId) as $value => $label) {
            $result[] = array(
                'value' => $value,
                'label' => $label
            );
        }

        return $result;
    }

    /**
     * Get options in "key-value" format.
     *
     * @param int $methodId
     * @return array
     */
    public function toArray($methodId = 0)
    {
        $result = array();

        try {
            // Resolve method id from configuration if none is supplied.
            if (!is_int($methodId) || $methodId === 0) {
                $methodId = $this->helper()->getMethodId();
            }

            // Load from cache.
            $result = $this->cache()->load($this->getCacheKey($methodId));

            if (!is_array($result)) {
                $collection = Mage::getModel('resursbank_checkout/account_method_annuity')
                    ->getCollection()
                    ->addFieldToFilter('method_id', $methodId);

                /** @var Resursbank_Checkout_Model_Account_Method_Annuity $annuity */
                foreach ($collection as $annuity) {
                    $result[$annuity->getId()] = $annuity->getTitle();
                }

                $this->cache()->save($result, $this->getCacheKey($methodId));
            }
        } catch (Exception $e) {
            $this->helper()->debugLog($e);
        }

        return $result;
    }

    /**
     * Retrieve instance of part payment helper.
     *
     * @return Resursbank_Checkout_Helper_PartPayment
     */
    private function helper()
    {
        return Mage::helper('resursbank_checkout/partPayment');
    }

    /**
     * Retrieve cache helper instance.
     *
     * @return Resursbank_Checkout_Helper_Cache
     */
    private function cache()
    {
        return Mage::helper('resursbank_checkout/cache');
    }

    /**
     * Retrieve cache key based on method id.
     *
     * @param int $methodId
     * @return string
     */
    private function getCacheKey($methodId)
    {
        return self::CACHE_ID . '_' . (int) $methodId;
    }
}
