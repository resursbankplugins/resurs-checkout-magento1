<?php
/**
 * Copyright 2016 Resurs Bank AB
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 * Class Resursbank_Checkout_Model_System_Config_Source_PartPayment_Methods
 */
class Resursbank_Checkout_Model_System_Config_Source_PartPayment_Methods
{
    /**
     * Cache id for payment method collection used for this setting.
     *
     * @var string
     */
    const CACHE_ID = 'resursbank_checkout_setting_partpayment_methods';

    /**
     * Options getter.
     *
     * @return array
     */
    public function toOptionArray()
    {
        $result = array();

        foreach ($this->toArray() as $value => $label) {
            $result[] = array(
                'value' => $value,
                'label' => $label
            );
        }

        return $result;
    }

    /**
     * Get options in "key-value" format.
     *
     * @return array
     */
    public function toArray()
    {
        $result = array();

        try {
            $accountId = $this->accountHelper()->getCurrentAccount()->getId();
            $result = $this->loadCache($accountId);

            if (empty($result)) {
                /** @var Resursbank_Checkout_Model_Account_Method $method */
                foreach ($this->getMethodsByAccountId($accountId) as $method) {
                    $result[$method->getId()] = $method->getTitle();
                }

                $this->saveCache($accountId, $result);
            }
        } catch (Exception $e) {
            $this->helper()->debugLog($e);
        }

        return is_array($result) ? $result : array();
    }

    /**
     * Retrieve instance of part payment helper.
     *
     * @return Resursbank_Checkout_Helper_PartPayment
     */
    private function helper()
    {
        return Mage::helper('resursbank_checkout/partPayment');
    }

    /**
     * Retrieve instance of account helper.
     *
     * @return Resursbank_Checkout_Helper_Account
     */
    private function accountHelper()
    {
        return Mage::helper('resursbank_checkout/account');
    }

    /**
     * Retrieve cache helper instance.
     *
     * @return Resursbank_Checkout_Helper_Cache
     */
    private function cache()
    {
        return Mage::helper('resursbank_checkout/cache');
    }

    /**
     * Returns a list of payment methods, filtered by account id.
     *
     * @param int $id
     * @return Mage_Eav_Model_Entity_Collection_Abstract
     */
    private function getMethodsByAccountId($id)
    {
        return $this->helper()
            ->getMethodsCollection()
            ->addFieldToFilter('account_id', $id);
    }

    /**
     * Loads cached payment method results based on an account id.
     *
     * @param int $accountId
     * @return array
     */
    private function loadCache($accountId)
    {
        $result = $this->cache()->load($this->getCacheKey($accountId));

        return is_array($result) ? $result : array();
    }

    /**
     * Cache payment methods result by account id. Payment methods has be
     * categorised by an account id, since the available methods can differ
     * between accounts.
     *
     * @param int $accountId
     * @param array $methods
     * @return $this
     */
    private function saveCache($accountId, $methods)
    {
        $this->cache()->save($methods, $this->getCacheKey($accountId));

        return $this;
    }

    /**
     * Retrieve cache key unique for each API account.
     *
     * @param int $accountId
     * @return string
     */
    private function getCacheKey($accountId)
    {
        return self::CACHE_ID . '_' . $accountId;
    }
}
