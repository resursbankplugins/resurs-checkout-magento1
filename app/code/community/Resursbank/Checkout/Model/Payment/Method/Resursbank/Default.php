<?php
/**
 * Copyright 2016 Resurs Bank AB
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 * Common methods used by all our payment methods.
 *
 * Class Resursbank_Checkout_Model_Payment_Method_Resursbank_Default
 */
class Resursbank_Checkout_Model_Payment_Method_Resursbank_Default extends Mage_Payment_Model_Method_Abstract
{
    /**
     * Default order status.
     *
     * @var string
     */
    const DEFAULT_ORDER_STATUS = 'pending';

    /**
     * Payment method code prefix.
     *
     * @var string
     */
    const CODE_PREFIX = 'resursbank_';

    /**
     * Default title.
     *
     * @var string
     */
    const TITLE = 'Resurs Bank';

    /**
     * @var string|null
     */
    protected $_type;

    /**
     * Payment method code.
     *
     * @var string
     */
    protected $_code = 'resursbank_default';

    /**
     * Can capture payment.
     *
     * @var bool
     */
    protected $_canCapture = true;

    /**
     * Can be refunded.
     *
     * @var bool
     */
    protected $_canRefund = true;

    /**
     * Can refund partial invoice amount.
     *
     * @var bool
     */
    protected $_canRefundInvoicePartial = true;

    /**
     * Can be used during checkout.
     *
     * @var bool
     */
    protected $_canUseCheckout = true;

    /**
     * Cannot be used from the administration panel.
     *
     * @var bool
     */
    protected $_canUseInternal = true;

    /**
     * @var Resursbank_Checkout_Helper_Data
     */
    protected $helper;

    /**
     * @var Resursbank_Checkout_Helper_Ecom
     */
    protected $ecom;

    /**
     * @var Resursbank_Checkout_Model_Api
     */
    protected $api;

    /**
     * Constructor.
     *
     * Setup commonly used resources.
     */
    public function __construct()
    {
        $this->helper = Mage::helper('resursbank_checkout');
        $this->ecom = Mage::helper('resursbank_checkout/ecom');
        $this->api = Mage::getSingleton('resursbank_checkout/api');

        if ($this->helper->useSimplifiedFlow()) {
            $this->_formBlockType = 'resursbank_checkout/payment_method_form';
        }

        parent::__construct();
    }

    /**
     * Retrieve Resurs Bank Method instance. This contains information about the
     * payment method fetched from the API.
     *
     * @return Resursbank_Checkout_Model_Account_Method
     */
    public function getResursBankMethod()
    {
        return Mage::getModel('resursbank_checkout/account_method')->load(
            $this->getCode(),
            'code'
        );
    }

    /**
     * Set code on this model instance to provided value.
     *
     * @param string $code
     * @return Resursbank_Checkout_Model_Payment_Method_Resursbank_Default
     */
    public function setCode($code)
    {
        $this->_code = (string) $code;

        return $this;
    }

    /**
     * Retrieve "raw" data value from Resurs Bank Method instance.
     *
     * @param string $key
     * @return mixed
     */
    public function getRawData($key = '')
    {
        return $this->getResursBankMethod()->getRawData($key);
    }

    /**
     * Check whether or not the method is available.
     *
     * If we are using Simplified Flow a couple of additional rules dictates
     * whether or not a payment method is available:
     *
     * 1) The default payment method is never available since it's a fallback
     * for Checkout Flow.
     *
     * 2) If the country defined in the customers billing address doesn't match
     * the country configured for our module no payment methods will work at
     * all.
     *
     * @param Mage_Sales_Model_Quote|null $quote
     * @return bool
     * @throws Mage_Core_Model_Store_Exception
     */
    public function isAvailable($quote = null)
    {
        $result = (
                $this->helper->isEnabled() &&
                $this->isAvailableResursBankMethod()
            ) ?
            parent::isAvailable($quote) :
            false;

        if ($result) {
            if ($this->helper->useSimplifiedFlow()) {
                $result = $this->isAvailableSimplifiedFlow($quote);
            } elseif ($this->helper->useHostedFlow()) {
                $result = $this->isAvailableHostedFlow($quote);
            }
        }

        return $result;
    }

    /**
     * Check if this payment method is available in checkout based on
     * information from Resurs Bank.
     *
     * @return bool
     */
    public function isAvailableResursBankMethod()
    {
        return (
            $this->getCode() === (self::CODE_PREFIX . 'default') ||
            $this->getResursBankMethod()->getActive()
        );
    }

    /**
     * Availability checks specifically used for Simplified Flow API.
     *
     * @param Mage_Sales_Model_Quote|null $quote
     * @return bool
     * @throws Mage_Core_Model_Store_Exception
     */
    protected function isAvailableSimplifiedFlow($quote = null)
    {
        return (
            $this->getCode() !== (self::CODE_PREFIX . 'default') &&
            $this->validateSimplifiedFlowCustomerType() &&
            $this->validateQuoteTotalLimit($quote)
        );
    }

    /**
     * Availability checks specifically used for Hosted Flow API.
     *
     * @param Mage_Sales_Model_Quote|null $quote
     * @return bool
     * @throws Mage_Core_Model_Store_Exception
     */
    protected function isAvailableHostedFlow($quote = null)
    {
        return $this->isAvailableSimplifiedFlow($quote);
    }

    /**
     * Validate that quote grand total is between the min/max limit values
     * specified for the payment method.
     *
     * @param null|Mage_Sales_Model_Quote $quote
     * @return bool
     */
    protected function validateQuoteTotalLimit($quote = null)
    {
        $total = (float) $quote->getGrandTotal();

        return (
            ($quote instanceof Mage_Sales_Model_Quote) &&
            $total > 0 &&
            $total >= $this->getResursBankMethod()->getMinOrderTotal() &&
            $total <= $this->getResursBankMethod()->getMaxOrderTotal()
        );
    }

    /**
     * Is available check specifically used for Simplified Flow API. This checks
     * whether or not the customer type previously selected by the customer
     * (custom input added by this module, above the form where you enter
     * your billing address) is available for this payment method.
     *
     * @return bool
     */
    protected function validateSimplifiedFlowCustomerType()
    {
        return in_array(
            Mage::helper('resursbank_checkout/simplified')
                ->getCustomerTypeFromSession(),
            (array) $this->getRawData('customerType')
        );
    }

    /**
     * Capture payment.
     *
     * @param Varien_Object $payment
     * @param float $amount
     * @return $this
     * @throws Exception
     */
    public function capture(Varien_Object $payment, $amount)
    {
        parent::capture($payment, $amount);

        try {
            /** @var Mage_Sales_Model_Order $order */
            $order = $payment->getOrder();

            if ($this->ecom->isEnabled() &&
                ($order instanceof Mage_Sales_Model_Order) &&
                $this->ecom->validatePayment($order)
            ) {
                /** @var string $reference */
                $reference = $this->helper
                    ->getPaymentReferenceFromOrder($order);

                // Log capturing event.
                $this->log(
                    'Capturing payment of order ' .
                    $reference .
                    ' with amount ' .
                    $amount .
                    ', using method ' .
                    $this->getCode()
                );

                // Finalize the payment.
                $this->finalizePayment($reference);

                // Set order increment_id as transaction identifier.
                $payment->setTransactionId($reference);

                // Log capturing event.
                $this->log(
                    "Successfully captured payment of order {$reference}."
                );

                // Add history entry.
                $order->addStatusHistoryComment(
                    $this->helper->__('Resursbank: payment debited.')
                );

                // Close transaction to complete process.
                $payment->setIsTransactionClosed(true);
            }
        } catch (Exception $e) {
            Mage::getSingleton('core/session')->addError(
                $this->helper->__('Failed to debit Resursbank payment.')
            );

            $this->log($e);

            throw $e;
        }

        return $this;
    }

    /**
     * Log messages.
     *
     * @param string|Exception $message
     */
    public function log($message)
    {
        $this->helper->debugLog($message, Resursbank_Checkout_Helper_Log::ECOM);
    }

    /**
     * Finalize Resursbank payment.
     *
     * @param string $reference
     * @return $this
     * @throws Exception
     */
    public function finalizePayment($reference)
    {
        $reference = (string)$reference;

        /** @var \Resursbank\RBEcomPHP\ResursBank $connection */
        $connection = $this->ecom->getConnection();

        // ECom requires this.
        $connection->setPreferredId($reference);

        /** @var \Resursbank\RBEcomPHP\resurs_payment $paymentSession */
        $paymentSession = $connection->getPayment($reference);

        if (!is_object($paymentSession) || !isset($paymentSession->id)) {
            throw new Exception(
                "Resursbank payment {$reference} could not be found."
            );
        }

        if (!isset($paymentSession->finalized)) {
            $paymentSession->finalized = false;
        }

        // Finalize Resursbank payment.
        if (!$paymentSession->finalized &&
            !$this->ecom->paymentSessionHasStatus(
                $paymentSession,
                'IS_DEBITED'
            )
        ) {
            if (isset($paymentSession->frozen) && $paymentSession->frozen) {
                throw new Exception(
                    "Resursbank payment {$reference} is still frozen."
                );
            }

            if (!$this->ecom->paymentSessionHasStatus(
                $paymentSession,
                'DEBITABLE'
            )) {
                throw new Exception(
                    "Resursbank payment {$reference} cannot be debited yet."
                );
            }

            // Finalize payment session.
            if (!$connection->finalizePayment($reference)) {
                throw new Exception(
                    "Failed to finalize Resursbank payment {$reference}"
                );
            }
        }

        return $this;
    }

    /**
     * Retrieve method identifier.
     *
     * @return string
     */
    public function getIdentifier()
    {
        $result = '';

        $method = Mage::getModel('resursbank_checkout/account_method')->load(
            $this->getCode(),
            'code'
        );

        if ($method->getId()) {
            $result = $method->getIdentifier();
        }

        return $result;
    }
}
