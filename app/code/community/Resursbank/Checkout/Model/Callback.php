<?php
/**
 * Copyright 2016 Resurs Bank AB
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 * Callback business logic.
 *
 * Class Resursbank_Checkout_Model_Callback
 */
class Resursbank_Checkout_Model_Callback extends Mage_Core_Model_Abstract
{
    /**
     * @var Resursbank_Checkout_Helper_Callback
     */
    protected $helper;

    /**
     * Constructor.
     *
     * Setup commonly used resources.
     */
    protected function _construct()
    {
        parent::_construct();

        $this->helper = Mage::helper('resursbank_checkout/callback');
    }

    /**
     * Payment is unfrozen, which means it can be captured.
     *
     * @param string $paymentId
     * @param string $digest
     * @return bool
     * @throws Exception
     */
    public function unfreeze($paymentId, $digest)
    {
        $this->initCallback('unfreeze', $paymentId)
            ->validate($paymentId, $digest)
            ->log($paymentId, 'unfreeze')
            ->addOrderComment($paymentId, 'Resursbank: payment unfrozen.')
            ->setOrderCallbackFlag($paymentId, true)
            ->finalizeCallback('unfreeze', $paymentId);

        return true;
    }

    /**
     * Payment has been booked by Resursbank. This means the payment has been
     * unfrozen and is preparing to be finalized.
     *
     * @param string $paymentId
     * @param string $digest
     * @return bool
     * @throws Exception
     */
    public function booked($paymentId, $digest)
    {
        $this->initCallback('booked', $paymentId)
            ->validate($paymentId, $digest)
            ->log($paymentId, 'booked')
            ->addOrderComment($paymentId, 'Resursbank: payment booked.')
            ->sendOrderEmail($paymentId)
            ->setOrderCallbackFlag($paymentId, true)
            ->finalizeCallback('booked', $paymentId);

        return true;
    }

    /**
     * Payment has been finalized by Resursbank. This means the client has been
     * debited by Resursbank.
     *
     * @param string $paymentId
     * @param string $digest
     * @return bool
     * @throws Exception
     */
    public function finalization($paymentId, $digest)
    {
        $this->initCallback('finalization', $paymentId)
            ->validate($paymentId, $digest)
            ->log($paymentId, 'finalization')
            ->addOrderComment($paymentId, 'Resursbank: payment finalized.')
            ->setOrderCallbackFlag($paymentId, true)
            ->finalizeCallback('finalization', $paymentId);

        return true;
    }

    /**
     * Payment passed/failed automatic fraud screening from Resursbank.
     *
     * @param string $paymentId
     * @param string $digest
     * @param string $result (FROZEN = failed, THAWED = passed)
     * @return bool
     * @throws Exception
     */
    public function automaticFraudControl($paymentId, $digest, $result)
    {
        // Resolve screening result.
        $passed = (strtolower($result) === 'thawed');

        // Order comment.
        $comment = ($passed ?
            'Resursbank: payment passed automatic fraud screening.' :
            'Resursbank: payment failed automatic fraud screening.'
        );

        // Order status.
        $status = (!$passed ? 'suspected_fraud' : 'confirmed');

        $this->initCallback('automatic_fraud_control', $paymentId)
            ->validate($paymentId, $digest, $result)
            ->log($paymentId, "automatic fraud control ({$passed}).")
            ->addOrderComment($paymentId, $comment)
            ->setOrderStatus($paymentId, $status)
            ->setOrderCallbackFlag($paymentId, true)
            ->finalizeCallback('automatic_fraud_control', $paymentId);

        return true;
    }

    /**
     * Payment has been fully annulled by Resursbank. This can for example occur
     * if a fraud screening fails.
     *
     * @param string $paymentId
     * @param string $digest
     * @return bool
     * @throws Exception
     */
    public function annulment($paymentId, $digest)
    {
        // Avoid cancelling the payment when the order is cancelled.
        Mage::register(
            Resursbank_Checkout_Helper_Data::FLAG_AUTOMATIC_ORDER_ANNULMENT,
            true
        );

        $this->initCallback('annulment', $paymentId)
            ->validate($paymentId, $digest)
            ->log($paymentId, 'annulment')
            ->annulOrder($paymentId)
            ->addOrderComment($paymentId, 'Resursbank: payment annulled.')
            ->setOrderCallbackFlag($paymentId, true)
            ->finalizeCallback('annulment', $paymentId);

        return true;
    }

    /**
     * Payment has been updated at Resursbank.
     *
     * @param string $paymentId
     * @param string $digest
     * @return bool
     * @throws Exception
     */
    public function update($paymentId, $digest)
    {
        $this->initCallback('update', $paymentId)
            ->validate($paymentId, $digest)
            ->log($paymentId, 'update')
            ->addOrderComment($paymentId, 'Resursbank: payment updated.')
            ->setOrderCallbackFlag($paymentId, true)
            ->finalizeCallback('update', $paymentId);

        return true;
    }

    /**
     * Retrieve request order.
     *
     * @param string $paymentId
     * @return Mage_Sales_Model_Order
     * @throws Exception
     */
    protected function getOrder($paymentId)
    {
        return $this->helper->getOrderFromRequest($paymentId);
    }

    /**
     * Log that we received a callback.
     *
     * @param string $paymentId
     * @param $text
     * @return $this
     */
    public function log($paymentId, $text)
    {
        $this->helper->debugLog(
            "Received callback {$text} for {$paymentId}",
            Resursbank_Checkout_Helper_Log::CALLBACK
        );

        return $this;
    }

    /**
     * Annul order.
     *
     * @param string $paymentId
     * @return $this
     * @throws Exception
     */
    public function annulOrder($paymentId)
    {
        $this->helper->annulOrder($this->getOrder($paymentId));

        return $this;
    }

    /**
     * Send order email.
     *
     * @param string $paymentId
     * @return $this
     * @throws Exception
     */
    public function sendOrderEmail($paymentId)
    {
        $this->helper->sendOrderEmail($this->getOrder($paymentId));

        return $this;
    }

    /**
     * Add history comment to requested order.
     *
     * @param string $paymentId
     * @param string $comment
     * @return $this
     * @throws Exception
     */
    protected function addOrderComment($paymentId, $comment)
    {
        $this->helper->addOrderComment(
            $this->getOrder($paymentId),
            $this->helper->__($comment)
        );

        return $this;
    }

    /**
     * Set order status.
     *
     * @param string $paymentId
     * @param string $type
     * @return $this
     * @throws Exception
     */
    public function setOrderStatus($paymentId, $type)
    {
        $status = Mage::getStoreConfig(
            "resursbank_checkout/callback/order_status_{$type}"
        );

        if ($status) {
            $this->helper->setOrderStatus($this->getOrder($paymentId), $status);
        }

        return $this;
    }

    /**
     * Set callback received flag on order.
     *
     * @param string $paymentId
     * @param bool $value
     * @return $this
     * @throws Exception
     */
    public function setOrderCallbackFlag($paymentId, $value)
    {
        $this->helper->setOrderCallbackFlag(
            $this->getOrder($paymentId),
            (bool) $value
        );

        return $this;
    }

    /**
     * Validate incoming callback using provied digest value.
     *
     * @param string $paymentId
     * @param string $digest
     * @param string $additional (optional)
     * @return $this
     * @throws Exception
     */
    public function validate($paymentId, $digest, $additional = '')
    {
        $additional = (string) $additional;

        if (!is_string($paymentId) ||
            !is_string($digest) ||
            empty($paymentId) ||
            empty($digest)
        ) {
            throw new Exception('Missing required parameters.');
        }

        $raw = $paymentId . $additional . $this->helper->getSalt();

        if (strtoupper(sha1($raw)) !== $digest) {
            throw new Exception('Invalid digest.');
        }

        return $this;
    }

    /**
     * Initialize callback.
     *
     * @param string $type
     * @param string $paymentId
     * @return $this
     * @throws Exception
     */
    public function initCallback($type, $paymentId)
    {
        $this->helper->dispatchBeforeCallbackEvent($type, $paymentId);

        return $this;
    }

    /**
     * Finalize callback.
     *
     * @param string $type
     * @param string $paymentId
     * @return $this
     * @throws Exception
     */
    public function finalizeCallback($type, $paymentId)
    {
        $this->helper->dispatchAfterCallbackEvent($type, $paymentId);

        return $this;
    }
}
