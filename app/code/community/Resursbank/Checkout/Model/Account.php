<?php
/**
 * Copyright 2016 Resurs Bank AB
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 * Class Resursbank_Checkout_Model_Account
 */
class Resursbank_Checkout_Model_Account extends Mage_Core_Model_Abstract
{
    /**
     * Define resource model.
     *
     * @return void
     */
    protected function _construct()
    {
        $this->_init('resursbank_checkout/account');
    }

    /**
     * Set update_at timestamp before saving entry.
     *
     * @return Mage_Core_Model_Abstract
     */
    protected function _beforeSave()
    {
        $this->setUpdatedAt(Mage::getSingleton('core/date')->gmtDate());

        return parent::_beforeSave();
    }

    /**
     * Load by Credentials instance.
     *
     * @param Resursbank_Checkout_Model_Api_Credentials $credentials
     * @return $this
     */
    public function loadByCredentials(
        Resursbank_Checkout_Model_Api_Credentials $credentials
    ) {
        $collection = $this->getCollection()
            ->addFieldToFilter('username', $credentials->getUsername())
            ->addFieldToFilter('environment', $credentials->getEnvironmentCode());

        if (count($collection) > 0) {
            $this->load($collection->getLastItem()->getId());
        }

        return $this;
    }

    /**
     * @return string
     */
    public function getSalt()
    {
        return (string) $this->_getData('salt');
    }
}
