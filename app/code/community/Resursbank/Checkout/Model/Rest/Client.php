<?php
/**
 * Copyright 2016 Resurs Bank AB
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 * The original
 *
 * Class Resursbank_Checkout_Model_Rest_Client
 */
class Resursbank_Checkout_Model_Rest_Client extends Zend_Rest_Client
{
    /**
     * @var string
     */
    const ENC_JSON = 'application/json';

    /**
     * Custom request enc type.
     *
     * @var string
     */
    protected $customEncType;

    /**
     * Perform a POST or PUT
     *
     * Performs a POST or PUT request. Any data provided is set in the HTTP
     * client. String data is pushed in as raw POST data; array or object data
     * is pushed in as POST parameters.
     *
     * This extends the original function from Zend_Rest_Client to allow custom
     * enc types, like application/json.
     *
     * @param mixed $method
     * @param mixed $data
     * @return Zend_Http_Response
     * @throws Zend_Http_Client_Exception
     */
    protected function _performPost($method, $data = null)
    {
        $client = self::getHttpClient();

        if (is_null($this->customEncType)) {
            if (is_string($data)) {
                $client->setRawData($data);
            } elseif (is_array($data) || is_object($data)) {
                $client->setParameterPost((array)$data);
            }
        } else {
            $enctype = (string) $this->customEncType;

            if ($enctype === self::ENC_JSON) {
                $client->setRawData(Mage::helper('core')->jsonEncode($data));
                $client->setEncType(self::ENC_JSON);
            } else {
                throw new Zend_Http_Client_Exception(
                    "Invalid enctype {$enctype}."
                );
            }
        }

        return $client->request($method);
    }

    /**
     * Set custom enc type.
     *
     * @param string $val
     * @return $this
     */
    public function setCustomEncType($val)
    {
        $this->customEncType = $val;

        return $this;
    }
}
