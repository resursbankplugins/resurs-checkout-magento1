<?php
/**
 * Copyright 2016 Resurs Bank AB
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/* @var $installer Mage_Core_Model_Resource_Setup */
$installer = $this;

$installer->startSetup();

/** @var string $table */
$table = $installer->getTable('resursbank_checkout/method');

// Type column.
$installer->getConnection()->addColumn($table, 'type', array(
    'type' => Varien_Db_Ddl_Table::TYPE_TEXT,
    'length' => 255,
    'nullable' => false,
    'comment' => 'Payment method type'
));

// Specific type column.
$installer->getConnection()->addColumn($table, 'specific_type', array(
    'type' => Varien_Db_Ddl_Table::TYPE_TEXT,
    'length' => 255,
    'nullable' => false,
    'comment' => 'Payment method specific type'
));

// Min limit column.
$installer->getConnection()->addColumn($table, 'min_limit', array(
    'type' => Varien_Db_Ddl_Table::TYPE_DECIMAL,
    'scale' => 4,
    'precision' => 12,
    'unsigned' => true,
    'nullable' => false,
    'default' => 0,
    'comment' => 'Minimum order cost to utilize payment method.'
));

// Max limit column.
$installer->getConnection()->addColumn($table, 'max_limit', array(
    'type' => Varien_Db_Ddl_Table::TYPE_DECIMAL,
    'scale' => 4,
    'precision' => 12,
    'unsigned' => true,
    'nullable' => false,
    'default' => 0,
    'comment' => 'Maximum order cost to utilize payment method.'
));

// Raw column.
$installer->getConnection()->addColumn($table, 'raw', array(
    'type' => Varien_Db_Ddl_Table::TYPE_TEXT,
    'length' => 0,
    'nullable' => false,
    'comment' => 'Complete payment method information, JSON encoded.'
));

$installer->endSetup();
