<?php
/**
 * Copyright 2016 Resurs Bank AB
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/* @var $installer Mage_Core_Model_Resource_Setup */
$installer = $this;
$installer->startSetup();

/**
 * Create table 'resursbank_checkout/account'
 */
$accountTable = $installer->getConnection()
    ->newTable($installer->getTable('resursbank_checkout/account'))
    ->addColumn(
        'account_id',
        Varien_Db_Ddl_Table::TYPE_SMALLINT,
        null,
        [
            'unsigned' => true,
            'identity' => true,
            'nullable' => false,
            'primary' => true
        ],
        'Prim Key'
    )->addColumn(
        'username',
        Varien_Db_Ddl_Table::TYPE_TEXT,
        255,
        ['nullable' => false],
        'API Account Username'
    )->addColumn(
        'environment',
        Varien_Db_Ddl_Table::TYPE_TEXT,
        255,
        ['nullable' => false],
        'API Account Environment'
    )->addColumn(
        'salt',
        Varien_Db_Ddl_Table::TYPE_TEXT,
        255,
        ['nullable' => true],
        'Callback SALT'
    )->addColumn(
        'created_at',
        Varien_Db_Ddl_Table::TYPE_TIMESTAMP,
        null,
        ['nullable' => false, 'default' => Varien_Db_Ddl_Table::TIMESTAMP_INIT],
        'Created At'
    )->addColumn(
        'updated_at',
        Varien_Db_Ddl_Table::TYPE_TIMESTAMP,
        null,
        ['nullable' => false],
        'Updated At'
    )->addIndex(
        $installer->getIdxName(
            'resursbank_checkout/account',
            ['username', 'environment'],
            Varien_Db_Adapter_Interface::INDEX_TYPE_UNIQUE
        ),
        ['username', 'environment'],
        ['type' => Varien_Db_Adapter_Interface::INDEX_TYPE_UNIQUE]
    )->setComment(
        'Resurs Bank API Account Table'
    );

$installer->getConnection()->createTable($accountTable);

/**
 * Create table 'resursbank_checkout/account_method'
 */
$methodTable = $installer->getConnection()
    ->newTable(
        $installer->getTable('resursbank_checkout/account_method')
    )->addColumn(
        'method_id',
        Varien_Db_Ddl_Table::TYPE_SMALLINT,
        null,
        [
            'unsigned' => true,
            'identity' => true,
            'nullable' => false,
            'primary' => true
        ],
        'Prim Key'
    )->addColumn(
        'account_id',
        Varien_Db_Ddl_Table::TYPE_SMALLINT,
        null,
        ['unsigned' => true, 'nullable' => true],
        'Account ID'
    )->addColumn(
        'identifier',
        Varien_Db_Ddl_Table::TYPE_TEXT,
        255,
        ['nullable' => false],
        'Raw Method ID'
    )->addColumn(
        'code',
        Varien_Db_Ddl_Table::TYPE_TEXT,
        255,
        ['nullable' => false],
        'Method Code'
    )->addColumn(
        'active',
        Varien_Db_Ddl_Table::TYPE_BOOLEAN,
        null,
        ['nullable' => false, 'default' => 0],
        'Active'
    )->addColumn(
        'title',
        Varien_Db_Ddl_Table::TYPE_TEXT,
        255,
        ['nullable' => false],
        'Method Title'
    )->addColumn(
        'min_order_total',
        Varien_Db_Ddl_Table::TYPE_DECIMAL,
        '15,5',
        ['unsigned' => true, 'nullable' => false, 'default' => '0.00000'],
        'Minimum Order Total'
    )->addColumn(
        'max_order_total',
        Varien_Db_Ddl_Table::TYPE_DECIMAL,
        '15,5',
        ['unsigned' => true, 'nullable' => false, 'default' => '0.00000'],
        'Maximum Order Total'
    )->addColumn(
        'order_status',
        Varien_Db_Ddl_Table::TYPE_TEXT,
        255,
        ['nullable' => false, 'default' => 'pending'],
        'Order Status'
    )->addColumn(
        'specificcountry',
        Varien_Db_Ddl_Table::TYPE_TEXT,
        255,
        ['nullable' => false],
        'Which country the method can be used with'
    )->addColumn(
        'raw',
        Varien_Db_Ddl_Table::TYPE_TEXT,
        0,
        ['nullable' => true],
        'Raw API Data'
    )->addColumn(
        'created_at',
        Varien_Db_Ddl_Table::TYPE_TIMESTAMP,
        null,
        ['nullable' => false, 'default' => Varien_Db_Ddl_Table::TIMESTAMP_INIT],
        'Created At'
    )->addColumn(
        'updated_at',
        Varien_Db_Ddl_Table::TYPE_TIMESTAMP,
        null,
        ['nullable' => false],
        'Updated At'
    )->addIndex(
        $installer->getIdxName(
            'resursbank_checkout/account_method',
            ['code'],
            Varien_Db_Adapter_Interface::INDEX_TYPE_UNIQUE
        ),
        ['code'],
        ['type' => Varien_Db_Adapter_Interface::INDEX_TYPE_UNIQUE]
    )->addIndex(
        $installer->getIdxName(
            'resursbank_checkout/account_method',
            ['account_id']
        ),
        ['account_id']
    )->addForeignKey(
        $installer->getFkName(
            'resursbank_checkout/account_method',
            'account_id',
            'resursbank_checkout/account',
            'account_id'
        ),
        'account_id',
        $installer->getTable('resursbank_checkout/account'),
        'account_id',
        Varien_Db_Ddl_Table::ACTION_CASCADE
    )->setComment(
        'Resurs Bank Payment Method Table'
    );

$installer->getConnection()->createTable($methodTable);

/**
 * Create table 'resursbank_checkout/account_method_annuity'
 */
$annuityTable = $installer->getConnection()
    ->newTable(
        $installer->getTable('resursbank_checkout/account_method_annuity')
    )->addColumn(
        'annuity_id',
        Varien_Db_Ddl_Table::TYPE_SMALLINT,
        null,
        [
            'unsigned' => true,
            'identity' => true,
            'nullable' => false,
            'primary' => true
        ],
        'Prim Key'
    )->addColumn(
        'method_id',
        Varien_Db_Ddl_Table::TYPE_SMALLINT,
        null,
        ['unsigned' => true, 'nullable' => true],
        'Method ID'
    )->addColumn(
        'title',
        Varien_Db_Ddl_Table::TYPE_TEXT,
        255,
        ['nullable' => false],
        'Annuity Title'
    )->addColumn(
        'raw',
        Varien_Db_Ddl_Table::TYPE_TEXT,
        0,
        ['nullable' => true],
        'Raw API Data'
    )->addColumn(
        'duration',
        Varien_Db_Ddl_Table::TYPE_SMALLINT,
        null,
        ['unsigned' => true, 'nullable' => true],
        'Annuity Duration'
    )->addColumn(
        'factor',
        Varien_Db_Ddl_Table::TYPE_FLOAT,
        null,
        ['unsigned' => true, 'nullable' => true],
        'Annuity Factor'
    )->addColumn(
        'created_at',
        Varien_Db_Ddl_Table::TYPE_TIMESTAMP,
        null,
        ['nullable' => false, 'default' => Varien_Db_Ddl_Table::TIMESTAMP_INIT],
        'Created At'
    )->addColumn(
        'updated_at',
        Varien_Db_Ddl_Table::TYPE_TIMESTAMP,
        null,
        ['nullable' => false],
        'Updated At'
    )->addIndex(
        $installer->getIdxName(
            'resursbank_checkout/account_method_annuity',
            ['method_id']
        ),
        ['method_id']
    )->addForeignKey(
        $installer->getFkName(
            'resursbank_checkout/account_method_annuity',
            'method_id',
            'resursbank_checkout/account_method',
            'method_id'
        ),
        'method_id',
        $installer->getTable('resursbank_checkout/account_method'),
        'method_id',
        Varien_Db_Ddl_Table::ACTION_CASCADE
    )->setComment(
        'Resurs Bank Payment Method Annuity Table'
    );

$installer->getConnection()->createTable($annuityTable);

$installer->endSetup();
