<?php
/**
 * Copyright 2016 Resurs Bank AB
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 * Business logic to handle payment method data retrieved from the API.
 *
 * Class Resursbank_Checkout_Helper_Payment_Method_Api
 */
class Resursbank_Checkout_Helper_Payment_Method_Api extends Resursbank_Checkout_Helper_Payment_Method
{
    /**
     * Retrieve all available payment methods from the API.
     *
     * @param Resursbank_Checkout_Model_Api_Credentials $credentials
     * @return array
     * @throws Exception
     */
    public function fetch(
        Resursbank_Checkout_Model_Api_Credentials $credentials
    ) {
        $result = $this->ecom($credentials)->getPaymentMethods();

        if (is_array($result)) {
            // The raw result from ECom will be an array of stdClass instances,
            // we want a nestled array. To achieve this we can simply
            // json_encode the raw value, and the decode it again.
            $result = json_decode(json_encode($result), true);
        }

        return is_array($result) ? $result : [];
    }

    /**
     * Sync a payment method to our database.
     *
     * @param array $method
     * @param Resursbank_Checkout_Model_Account $account
     * @param Resursbank_Checkout_Model_Api_Credentials $credentials
     * @param string $country
     * @return Resursbank_Checkout_Model_Account_Method
     * @throws Exception
     */
    public function sync(
        array $method,
        Resursbank_Checkout_Model_Account $account,
        Resursbank_Checkout_Model_Api_Credentials $credentials,
        $country
    ) {
        // Validate raw API data.
        $this->validate($method);

        // Resolve payment method code.
        $code = $this->getCode($method['id'], $credentials);

        /** @var Resursbank_Checkout_Model_Account_Method $model */
        $model = Mage::getModel('resursbank_checkout/account_method')
            ->load($code, 'code');

        // Set 'code' if this is a new method currently not in the db.
        if (!$model->getId()) {
            $model->setCode($code);
        }

        // Update values of method record (existing or not). Please note we
        // cannot use setData as that would overwrite 'code' and 'id', thus
        // causing a unique constraint violation when saving.
        $model->setIdentifier($method['id'])
            ->setAccountId($account->getId())
            ->setActive(1)
            ->setTitle($method['description'])
            ->setMinOrderTotal($method['minLimit'])
            ->setMaxOrderTotal($method['maxLimit'])
            ->setOrderStatus(Resursbank_Checkout_Model_Payment_Method_Resursbank_Default::DEFAULT_ORDER_STATUS)
            ->setSpecificcountry($country)
            ->setRaw(json_encode($method))
            ->setSpecificcountry((string) $country);

        // Save updates.
        $model->save();

        return $model;
    }

    /**
     * Validate keys in raw payment method data from API.
     *
     * @param array $method
     * @throws Exception
     */
    protected function validate(array $method)
    {
        $keys = ['id', 'description', 'minLimit', 'maxLimit'];

        foreach ($keys as $key) {
            if (!isset($method[$key])) {
                throw new Exception(
                    'Missing key in raw payment method data ' . $key
                );
            }
        }
    }

    /**
     * Retrieve formatted method code unique to API account.
     *
     * @param string $identifier
     * @param Resursbank_Checkout_Model_Api_Credentials $credentials
     * @return string
     */
    protected function getCode(
        $identifier,
        Resursbank_Checkout_Model_Api_Credentials $credentials
    ) {
        return Resursbank_Checkout_Model_Payment_Method_Resursbank_Default::CODE_PREFIX .
            strtolower((string) $identifier) . '_' .
            $credentials->getMethodSuffix();
    }
}
