<?php
/**
 * Copyright 2016 Resurs Bank AB
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 * Business logic to handle payment method annuity data.
 *
 * Class Resursbank_Checkout_Helper_Payment_Method_Api_Annuity
 */
class Resursbank_Checkout_Helper_Payment_Method_Api_Annuity extends Resursbank_Checkout_Helper_Payment_Method
{
    /**
     * Sync payment method annuity factors from API to our local DB.
     *
     * @param Resursbank_Checkout_Model_Api_Credentials $credentials
     * @param Resursbank_Checkout_Model_Account_Method $method
     * @throws Exception
     */
    public function sync(
        Resursbank_Checkout_Model_Api_Credentials $credentials,
        Resursbank_Checkout_Model_Account_Method $method
    ) {
        foreach ($this->fetch($credentials, $method) as $annuity) {
            // Validate raw annuity data.
            $this->validate($annuity);

            // Create new annuity entry in our database.
            Mage::getModel('resursbank_checkout/account_method_annuity')
                ->setMethodId($method->getId())
                ->setTitle($annuity['paymentPlanName'])
                ->setDuration($annuity['duration'])
                ->setFactor($annuity['factor'])
                ->setRaw(json_encode($annuity))
                ->save();
        }
    }

    /**
     * Fetch annuity factors from API.
     *
     * @param Resursbank_Checkout_Model_Api_Credentials $credentials
     * @param Resursbank_Checkout_Model_Account_Method $method
     * @return array
     * @throws Exception
     */
    private function fetch(
        Resursbank_Checkout_Model_Api_Credentials $credentials,
        Resursbank_Checkout_Model_Account_Method $method
    ) {
        $result = $this->ecom($credentials)
            ->getAnnuityFactors($method->getIdentifier());

        if (is_array($result)) {
            // The raw result from ECom will be an array of stdClass instances,
            // we want a nestled array. To achieve this we can simply
            // json_encode the raw value, and the decode it again.
            $result = json_decode(json_encode($result), true);
        }

        return is_array($result) ? $result : [];
    }

    /**
     * Truncate annuities table.
     */
    public function truncateTable()
    {
        /** @var $resource Mage_Core_Model_Resource $resource */
        $resource = Mage::getSingleton('core/resource');

        /** @var Varien_Db_Adapter_Interface $connection */
        $connection = $resource->getConnection('core_write');

        $connection->truncateTable(
            $resource->getTableName(
                'resursbank_checkout/account_method_annuity'
            )
        );
    }

    /**
     * Validate keys in raw payment method annuity factor data from API.
     *
     * @param array $annuity
     * @throws Exception
     */
    private function validate(array $annuity)
    {
        $keys = ['factor', 'duration', 'paymentPlanName'];

        foreach ($keys as $key) {
            if (!isset($annuity[$key])) {
                throw new Exception(
                    'Missing key in raw payment method annuity data ' . $key
                );
            }
        }
    }
}
