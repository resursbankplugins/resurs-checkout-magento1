<?php
/**
 * Copyright 2016 Resurs Bank AB
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 * Business logic to handle payment method data retrieved from the
 * sales/order_payment table.
 *
 * Class Resursbank_Checkout_Helper_Payment_Method_Legacy
 */
class Resursbank_Checkout_Helper_Payment_Method_Legacy extends Resursbank_Checkout_Helper_Payment_Method
{
    /**
     * This method will create payment method records for all methods found in
     * the sales/order_payment table. These records won't be activated.
     *
     * This lets us create config sections for old payment methods no longer
     * available in the API.
     *
     * Without this orders which used an old, no longer available, method would
     * cease to function. You would not be able to for example open them in the
     * admin panel, since their referenced payment method would not exist in the
     * config.
     */
    public function sync()
    {
        foreach ($this->fetch() as $method) {
            $model = Mage::getModel('resursbank_checkout/account_method')
                ->load($method['method'], 'code');

            // Default payment method code.
            $defaultMethod = Resursbank_Checkout_Model_Payment_Method_Resursbank_Default::CODE_PREFIX .
                'default';

            // Only create new records. All methods are deactivated at the
            // beginning of the synchronization process.
            if (!$model->getId() && $method['method'] !== $defaultMethod) {
                $model->setCode($method['method'])
                    ->setActive(0)
                    ->setTitle($this->getTitle($method))
                    ->save();
            }
        }
    }

    /**
     * Collect Resurs Bank payment methods from sales/order_payment table.
     *
     * @return array
     */
    private function fetch()
    {
        /** @var $resource Mage_Core_Model_Resource $resource */
        $resource = Mage::getSingleton('core/resource');

        /** @var Varien_Db_Adapter_Interface $connection */
        $connection = $resource->getConnection('core_read');

        // Retrieve a distinct collection of payment method codes and titles
        // used for existing orders.
        $result = $connection->fetchAll(
            "select distinct method, additional_information from " .
            $resource->getTableName('sales/order_payment') .
            " where method like '" .
            Resursbank_Checkout_Model_Payment_Method_Resursbank_Default::CODE_PREFIX .
            "%'"
        );

        return is_array($result) ? $result : [];
    }

    /**
     * Get payment method title from payment information extracted from order.
     *
     * @param array $method
     * @return string
     */
    private function getTitle(array $method)
    {
        $result = Resursbank_Checkout_Model_Payment_Method_Resursbank_Default::TITLE;
        $info = isset($method['additional_information']) ?
            @json_decode($method['additional_information'], true) :
            [];

        if (is_array($info) && isset($info['method_title'])) {
            $result = $info['method_title'];
        }

        return (string) $result;
    }
}
