<?php
/**
 * Copyright 2016 Resurs Bank AB
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 * Business logic to handle payment methods. Mor specifically this class handles
 * collecting methods and annuity factors from the API and local resources.
 *
 * Class Resursbank_Checkout_Helper_Payment_Method
 */
class Resursbank_Checkout_Helper_Payment_Method extends Resursbank_Checkout_Helper_Data
{
    /**
     * Cache key for generated config data.
     *
     * @var string
     */
    const CONFIG_DATA_CACHE_KEY = 'resursbank_checkout_payment_methods_config';

    /**
     * Payment model.
     *
     * @var string
     */
    const PAYMENT_MODEL = 'resursbank_checkout/payment_method_resursbank_default';

    /**
     * @var float
     */
    const DEFAULT_MIN_ORDER_TOTAL = 150.0;

    /**
     * @var float
     */
    const DEFAULT_MAX_ORDER_TOTAL = 0.0;

    /**
     * Sync all payment methods from the API and sales/order_payment table to
     * our local payment method table. This also syncs all available annuity
     * factors from the API.
     *
     * This data will later be used to construct configuration sections to
     * support our payment methods.
     *
     * @return void
     * @throws Exception
     */
    public function syncAll()
    {
        // Do not truncate methods table! We need to retain legacy methods
        // removed from the API account. Otherwise old orders won't work.
        $this->deactivateMethods();

        // Delete all annuity factors stored locally, to avoid dangling data
        // no longer available in the API account.
        $this->annuity()->truncateTable();

        /** @var array $credentials */
        foreach ($this->getCredentialsCollection() as $credentials) {
            /** @var Resursbank_Checkout_Model_Account $account */
            $account = $this->getAccount($credentials['credentials']);

            /** @var array $methods */
            $methods = $this->api()->fetch($credentials['credentials']);

            /** @var array $method */
            foreach ($methods as $method) {
                $this->annuity()->sync(
                    $credentials['credentials'],
                    $this->api()->sync(
                        $method,
                        $account,
                        $credentials['credentials'],
                        $credentials['country']
                    )
                );
            }
        }

        $this->legacy()->sync();

        // Clean our own cache data and the config cache (since our updated
        // methods list will need to be re-applied in the config cache for our
        // methods to function).
        $this->cache()->clean([Mage_Core_Model_Config::CACHE_TAG]);
    }

    /**
     * Get config data representing all our locally stored payment methods. This
     * data will be injected into Magento's configuration to enable our dynamic
     * payment methods.
     *
     * @return array
     */
    public function getConfigData()
    {
        $result = array();

        try {
            // Load previously collected data from cache.
            $result = $this->cache()->load(self::CONFIG_DATA_CACHE_KEY);

            if (!is_array($result)) {
                $result = array();

                /** @var Resursbank_Checkout_Model_Account_Method $method */
                foreach ($this->getCollection() as $method) {
                    $result[$method->getCode()] = array(
                        'identifier' => $method->getIdentifier(),
                        'code' => $method->getCode(),
                        'active' => $method->getActive(),
                        'model' => self::PAYMENT_MODEL,
                        'order_status' => $method->getOrderStatus(),
                        'title' => $method->getTitle(),
                        'group' => 'offline',
                        'allowspecific' => '1',
                        'specificcountry' => $method->getSpecificcountry(),
                        'min_order_total' => $method->getMinOrderTotal(),
                        'max_order_total' => $method->getMaxOrderTotal()
                    );
                }

                // Save collected data in cache.
                $this->cache()->save($result, self::CONFIG_DATA_CACHE_KEY);
            }
        } catch (Exception $e) {
            $this->debugLog($e);
        }

        return is_array($result) ? $result : array();
    }

    /**
     * Fetch collection of payment methods from local DB.
     *
     * @param bool $active Ignore inactive methods.
     * @return object
     */
    public function getCollection($active = false)
    {
        $collection = Mage::getModel('resursbank_checkout/account_method')
            ->getCollection();

        if ($active) {
            $collection->addFieldToFilter('active', true);
        }

        return $collection;
    }

    /**
     * Retrieve collection of all API credentials.
     *
     * @return array
     */
    public function getCredentialsCollection()
    {
        $list = [];

        /** @var Mage_Core_Model_Store $store */
        foreach (Mage::app()->getStores() as $store) {
            /** @var $credentials Resursbank_Checkout_Model_Api_Credentials */
            $credentials = Mage::getModel('resursbank_checkout/api_credentials')
                ->loadFromConfig($store);

            if ($credentials->hasCredentials()) {
                $list[$credentials->getHash()] = [
                    'credentials' => $credentials,
                    'country' => $this->getCountry($store)
                ];
            }
        }

        return $list;
    }

    /**
     * Retrieve instance of helper to handle payment methods from the API.
     *
     * @return Resursbank_Checkout_Helper_Payment_Method_Api
     */
    protected function api()
    {
        return Mage::helper('resursbank_checkout/payment_method_api');
    }

    /**
     * Retrieve instance of helper to handle legacy payment methods.
     *
     * @return Resursbank_Checkout_Helper_Payment_Method_Legacy
     */
    protected function legacy()
    {
        return Mage::helper('resursbank_checkout/payment_method_legacy');
    }

    /**
     * Retrieve instance of helper to handle annuities.
     *
     * @return Resursbank_Checkout_Helper_Payment_Method_Api_Annuity
     */
    protected function annuity()
    {
        return Mage::helper('resursbank_checkout/payment_method_api_annuity');
    }

    /**
     * Retrieve instance of cache helper.
     *
     * @return Resursbank_Checkout_Helper_Cache
     */
    protected function cache()
    {
        return Mage::helper('resursbank_checkout/cache');
    }

    /**
     * Get ECom API connection.
     *
     * @return \Resursbank\RBEcomPHP\ResursBank
     */
    protected function ecom(
        Resursbank_Checkout_Model_Api_Credentials $credentials
    ) {
        return Mage::helper('resursbank_checkout/ecom')
            ->getConnection($credentials);
    }

    /**
     * Retrieve configured country.
     *
     * @param Mage_Core_Model_Store $store
     * @return string
     */
    private function getCountry(Mage_Core_Model_Store $store)
    {
        return (string) Mage::getStoreConfig(
            'resursbank_checkout/api/country',
            $store
        );
    }

    /**
     * Reset 'active' to '0' for all records.
     *
     * @return void
     */
    private function deactivateMethods()
    {
        /** @var Resursbank_Checkout_Model_Account_Method $method */
        foreach ($this->getCollection(true) as $method) {
            $method->setActive(0)->save();
        }
    }

    /**
     * Retrieve API account from database based on supplied credentials.
     *
     * @param Resursbank_Checkout_Model_Api_Credentials $credentials
     * @return Resursbank_Checkout_Model_Account
     * @throws Exception
     */
    private function getAccount(
        Resursbank_Checkout_Model_Api_Credentials $credentials
    ) {
        $account = Mage::getModel('resursbank_checkout/account')
            ->loadByCredentials($credentials);

        if (!$account instanceof Resursbank_Checkout_Model_Account) {
            throw new Exception(
                'Failed to locate account associated with API credentials ' .
                $credentials->getUsername() . ' :: ' .
                $credentials->getEnvironmentCode()
            );
        }

        return $account;
    }
}
