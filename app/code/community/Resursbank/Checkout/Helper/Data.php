<?php
/**
 * Copyright 2016 Resurs Bank AB
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 * General functionality used here and there.
 *
 * Class Resursbank_Checkout_Helper_Data
 */
class Resursbank_Checkout_Helper_Data extends Mage_Core_Helper_Abstract
{
    /**
     * This flag will be used to avoid the after shop implementation from
     * making an API request to cancel the payment on an order being
     * automatically annulled, for example as a result of payment failure.
     *
     * @var string
     */
    const FLAG_AUTOMATIC_ORDER_ANNULMENT = 'resursbank_checkout_automatic_annulment';

    /**
     * Name of the quote table column containing the temporary Resursbank token.
     *
     * @var string
     */
    const QUOTE_TOKEN_FIELD = 'resursbank_token';

    /**
     * Name of error log file.
     *
     * @var string
     */
    const ERROR_LOG = 'resurs_error.log';

    /**
     * Working store id.
     *
     * @var int
     */
    protected $storeId;

    /**
     * Returns the version number for the module.
     *
     * @return string
     */
    public function getModuleVersion()
    {
        return Mage::getResourceModel('core/resource')
            ->getDbVersion('resursbank_checkout_setup');
    }

    /**
     * Check whether or not we are in the admin panel.
     *
     * @return boolean
     * @throws Mage_Core_Model_Store_Exception
     */
    public function isAdmin()
    {
        return (
            Mage::app()->getStore()->isAdmin() ||
            Mage::getDesign()->getArea() == 'adminhtml'
        );
    }

    /**
     * Get working store id, works for frontend and backend.
     *
     * @return int
     * @throws Mage_Core_Model_Store_Exception
     */
    public function getStoreId()
    {
        if (is_null($this->storeId)) {
            if ($this->isAdmin()) {
                $store = Mage::app()->getRequest()->getParam('store');

                if ($store === null) {
                    $orderId = (int) Mage::app()->getRequest()->getParam('order_id');

                    if ($orderId > 0) {
                        /** @var Mage_Sales_Model_Order $order */
                        $order = Mage::getModel('sales/order')->load($orderId);
                        $store = $order->getStore()->getId();
                    }
                }

                if (!is_numeric($store)) {
                    $storeModel = Mage::getModel('core/store')->load($store);

                    if ($storeModel && $storeModel->getId()) {
                        $this->storeId = (int) $storeModel->getId();
                    }
                } else {
                    $this->storeId = (int) $store;
                }
            } else {
                $this->storeId = (int) Mage::app()->getStore()->getId();
            }
        }

        return (int) $this->storeId;
    }

    /**
     * Set active store id. Useful when  we need getStoreId() to function
     * properly in context to for example orders, where store id is not
     * specified through the request.
     *
     * @param int $id
     * @return $this
     */
    public function setStoreId($id)
    {
        $this->storeId = (int) $id;

        return $this;
    }

    /**
     * Check if extension is enabled.
     *
     * @param int $storeId
     * @return boolean
     * @throws Mage_Core_Model_Store_Exception
     */
    public function isEnabled($storeId = null)
    {
        return (Mage::getStoreConfigFlag(
            'resursbank_checkout/general/enabled',
            (
                is_null($storeId) ?
                    $this->getStoreId() :
                    $storeId
            )
        ) &&
            $this->getApiModel()->hasCredentials()
        );
    }

    /**
     * Log debug message.
     *
     * @param string|array|Exception $message
     * @param string|null $file
     * @param bool $force
     * @return $this
     */
    public function debugLog($message, $file = null, $force = false)
    {
        Mage::helper('resursbank_checkout/log')->entry($message, $file, $force);

        return $this;
    }

    /**
     * Redirect to specified url.
     *
     * @param string $url
     * @param boolean $hard
     * @return $this
     * @throws Exception
     */
    public function redirect($url, $hard = false)
    {
        if (is_string($url) && !empty($url)) {
            if ($hard) {
                $this->hardRedirect($url);
            } else {
                $this->softRedirect($url);
            }
        }

        return $this;
    }

    /**
     * Redirect back to previous URL.
     */
    public function redirectBack()
    {
        header('Location: ' . $_SERVER['HTTP_REFERER']);
    }

    /**
     * Perform a hard redirect to $url
     *
     * @param $url
     * @throws Exception
     */
    public function hardRedirect($url)
    {
        $url = (string) $url;

        if (empty($url)) {
            throw new Exception("Cannot redirect to empty URL.");
        }

        header('Location: ' . (string) $url) ;
    }

    /**
     * Perform a soft redirect to $url
     *
     * @param $url
     */
    public function softRedirect($url)
    {
        Mage::app()
            ->getResponse()
            ->setRedirect(Mage::getUrl($url, array('_secure' => true)))
            ->sendResponse();
    }

    /**
     * Retrieve completely random string.
     *
     * @param int $length
     * @param string $charset
     * @return string
     */
    public function strRand($length, $charset = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789')
    {
        $result = '';

        $length = (int) $length;

        if ($length > 0) {
            $max = strlen($charset)-1;

            for ($i = 0; $i < $length; $i++) {
                $result.= $charset[mt_rand(0, $max)];
            }
        }

        return $result;
    }

    /**
     * Check if quote object have any items.
     *
     * @return bool
     */
    public function quoteHasItems()
    {
        return $this->getQuote()->hasItems();
    }

    /**
     * Retrieve quote object.
     *
     * @return Mage_Sales_Model_Quote
     */
    public function getQuote()
    {
        return Mage::getSingleton('checkout/cart')
            ->getQuote();
    }

    /**
     * Check if the quote object is unusable (ie. cannot be checked out).
     *
     * @return bool
     */
    public function quoteIsUnusable()
    {
        return (
            !$this->getQuote()->hasItems() ||
            $this->getQuote()->getHasError()
        );
    }

    /**
     * Assign default address information to quote object (in order to collect
     * available shipping methods).
     *
     * @param bool $shipping
     * @param bool $billing
     * @return $this
     * @throws Exception
     */
    public function quoteAssignDefaultAddress($shipping = true, $billing = true)
    {
        if ($shipping) {
            $this->quoteAssignAddress(array(
                'country_id' => Mage::helper('core')->getDefaultCountry(),
                'collect_shipping_rates' => true
            ), 'shipping');
        }

        if ($billing) {
            $this->quoteAssignAddress(array(
                'country_id' => Mage::helper('core')->getDefaultCountry(),
                'collect_shipping_rates' => true
            ), 'billing');
        }

        return $this;
    }

    /**
     * Assign address information to quote object.
     *
     * @param array $data
     * @param string $type
     * @return $this
     * @throws Exception
     */
    public function quoteAssignAddress(array $data, $type)
    {
        $this->quoteValidateAddressType($type);

        // Create empty address objects if they are missing.
        $this->quoteCreateMissingAddressObject($type);

        if ($type === 'billing') {
            $this->getQuote()->getBillingAddress()->addData($data);
        } else {
            $this->getQuote()->getShippingAddress()->addData($data);
        }

        return $this;
    }

    /**
     * Assign an empty Mage_Sales_Model_Quote_Address instance to quote
     * billing/shipping address if needed.
     *
     * @param string $type (billing|shipping)
     * @return $this
     * @throws Exception
     */
    public function quoteCreateMissingAddressObject($type)
    {
        $this->quoteValidateAddressType($type);

        if ($type === 'billing') {
            if (!$this->getQuote()->getBillingAddress()) {
                $this->getQuote()->setBillingAddress(
                    Mage::getModel('sales/quote_address')
                );
            }
        } else {
            if (!$this->getQuote()->getShippingAddress()) {
                $this->getQuote()->setShippingAddress(
                    Mage::getModel('sales/quote_address')
                );
            }
        }

        return $this;
    }

    /**
     * Validate address type, should be either billing or shipping.
     *
     * @param string $type
     * @return bool
     * @throws Exception
     */
    public function quoteValidateAddressType(&$type)
    {
        $type = (string) $type;

        if ($type !== 'billing' && $type !== 'shipping') {
            throw new Exception("Invalid address type provided.");
        }

        return true;
    }

    /**
     * Check if any address information is missing on the quote object.
     *
     * @return bool
     */
    public function quoteIsMissingAddress()
    {
        return (
            !$this->getQuote()->getShippingAddress()->getData('country_id') ||
            !$this->getQuote()->getBillingAddress()->getData('country_id')
        );
    }

    /**
     * Retrieve checkout session.
     *
     * @return Mage_Checkout_Model_Session
     */
    public function getCheckoutSession()
    {
        return Mage::getSingleton('checkout/session');
    }

    /**
     * Check if some value has changed based on a hash stored in the checkout
     * session (useful when checking if certain blocks have been updated as
     * values are changed in checkout, otherwise we do not need to transport the
     * HTML back to the client and ultimately we conserve resources that way).
     *
     * @param string $value
     * @param string $key
     * @return bool
     */
    public function blockHasChanged($value, $key)
    {
        $result = false;

        $value = (string) $value;
        $key = preg_replace('/[^a-z0-9\-]/', '', strtolower((string) $key));

        $key = 'resursbank-checkout-hash-' . $key;

        $current = $this->getCheckoutSession()->getData($key);
        $new = md5($value);

        if ($current !== $new) {
            $this->getCheckoutSession()->setData($key, $new);
            $result = true;
        }

        return $result;
    }

    /**
     * Reset checkout elements.
     *
     * @return $this
     */
    public function resetCheckoutElements()
    {
        if (!$this->legacySetup()) {
            $this->getCheckoutSession()->setData(
                'resursbank-checkout-hash-header-cart',
                null
            );
        }

        $this->getCheckoutSession()->setData(
            'resursbank-checkout-hash-resursbank-checkout-shipping-methods-list',
            null
        );

        $this->getCheckoutSession()->setData(
            'resursbank-checkout-hash-current-coupon-code',
            null
        );

        return $this;
    }

    /**
     * Retrieve API singleton.
     *
     * @return Resursbank_Checkout_Model_Api
     */
    public function getApiModel()
    {
        return Mage::getSingleton('resursbank_checkout/api');
    }

    /**
     * Initialize payment session, if one isn't already available.
     *
     * @return mixed|Zend_Http_Response
     * @throws Exception
     */
    public function initPaymentSession()
    {
        if (!$this->getApiModel()->paymentSessionInitialized()) {
            $this->getApiModel()->initPaymentSession();
        }

        return $this;
    }

    /**
     * Update active payment session if any.
     *
     * @return $this
     * @throws Exception
     */
    public function updatePaymentSession()
    {
        if ($this->getApiModel()->paymentSessionInitialized()) {
            try {
                $this->getApiModel()->updatePaymentSession();
            } catch (Exception $e) {
                // Debug log.
                $this->debugLog($e, Resursbank_Checkout_Helper_Log::API);
            }
        }

        return $this;
    }

    /**
     * Delete active payment session.
     *
     * @return mixed|Zend_Http_Response
     * @throws Exception
     */
    public function deletePaymentSession()
    {
        if ($this->getApiModel()->paymentSessionInitialized()) {
            try {
                // Delete session through API.
                $this->getApiModel()->deletePaymentSession();
            } catch (Exception $e) {
                // Debug log.
                $this->debugLog($e, Resursbank_Checkout_Helper_Log::API);
            }
        }

        // Unset session data.
        $this->clearPaymentSession();

        return $this;
    }

    /**
     * Clear payment session information from checkout session.
     *
     * @return $this
     */
    public function clearPaymentSession()
    {
        // Unset session data.
        $this->getCheckoutSession()
            ->unsetData(
                Resursbank_Checkout_Model_Api::PAYMENT_SESSION_ID_KEY
            )->unsetData(
                Resursbank_Checkout_Model_Api::PAYMENT_SESSION_IFRAME_KEY
            )->unsetData(
                Resursbank_Checkout_Model_Api_Adapter_Simplified::SESSION_KEY_CARD_NUMBER
            )->unsetData(
                Resursbank_Checkout_Model_Api_Adapter_Simplified::SESSION_KEY_COMPANY_GOVERNMENT_ID
            )->unsetData(
                Resursbank_Checkout_Model_Api_Adapter_Simplified::SESSION_KEY_CONTACT_GOVERNMENT_ID
            )->unsetData(
                Resursbank_Checkout_Model_Api_Adapter_Simplified::SESSION_KEY_IS_COMPANY
            )->unsetData(
                Resursbank_Checkout_Model_Api_Adapter_Simplified::SESSION_KEY_SSN
            )->unsetData(
                Resursbank_Checkout_Model_Api_Adapter_Simplified::SESSION_PAYMENT_ID
            );

        // Reset rendered checkout blocks.
        $this->resetCheckoutElements();

        return $this;
    }

    /**
     * Register API callbacks.
     *
     * @return $this
     * @throws Zend_Http_Client_Exception
     */
    public function registerApiCallbacks()
    {
        $this->getApiModel()->registerCallbacks();

        return $this;
    }

    /**
     * Retrieve/generate unique token used to identify quote/order object when
     * communicating with Resursbank servers. The token is a completely unique
     * string to ensure that callbacks made from Resursbank will identify the
     * correct quote/order.
     *
     * @param Mage_Sales_Model_Quote $quote
     * @param bool $refresh
     * @return string
     * @throws Exception
     */
    public function getPaymentReference(
        Mage_Sales_Model_Quote $quote,
        $refresh = false
    ) {
        if (!$quote->getId()) {
            throw new Exception('Uninitialized quote object.');
        }

        /** @var Mage_Sales_Model_Order $order */
        $order = Mage::getModel('sales/order')
            ->load(
                $quote->getId(),
                'quote_id'
            );

        if (($order instanceof Mage_Sales_Model_Order) && $order->getId()) {
            $result = $order->getIncrementId();
        } else {
            // Retrieve existing token form quote, if any.
            $result = $quote->getData(self::QUOTE_TOKEN_FIELD);

            if ($refresh || !is_string($result) || empty($result)) {
                // Generate unique token.
                $result = $this->strRand(32);

                // Store token on quote for later usage.
                $quote->setData(self::QUOTE_TOKEN_FIELD, $result)->save();
            }
        }

        if (empty($result)) {
            throw new Exception(
                'Something went wrong while obtaining order payment reference.'
            );
        }

        return (string) $result;
    }

    /**
     * Retrieve Resursbank payment reference from order object (resursbank_token
     * for backwards compatibility).
     *
     * @param Mage_Sales_Model_Order $order
     * @return string
     */
    public function getPaymentReferenceFromOrder(Mage_Sales_Model_Order $order)
    {
        $result = $order->getData('resursbank_token') ?
            $order->getData('resursbank_token') :
            '';

        if (empty($result)) {
            $result = $order->getOriginalIncrementId() ?
                $order->getOriginalIncrementId() : $order->getIncrementId();
        }

        return (string) $result;
    }

    /**
     * URL encode array recursively.
     *
     * @param array $data
     * @return array
     */
    public function urlencodeArray(array $data)
    {
        if (count($data)) {
            foreach ($data as $key => $val) {
                $data[$key] = is_array($val) ?
                    $this->urlencodeArray($val) :
                    urlencode($val);
            }
        }

        return $data;
    }

    /**
     * Get a completed payment from Resursbank.
     *
     * @param Mage_Sales_Model_Order $order
     * @return null|stdClass
     * @throws Zend_Http_Client_Exception
     * @throws Exception
     */
    public function getPayment(Mage_Sales_Model_Order $order)
    {
        // Set correct store id to use in context with API calls.
        $this->setStoreId($order->getStoreId());

        return $this->getApiModel()->getPayment(
            $this->getPaymentReferenceFromOrder($order)
        );
    }

    /**
     * Render checkout elements (useful for return values from AJAX calls).
     *
     * @param Mage_Core_Model_Layout $layout
     * @param bool $onlyUpdated
     * @return array
     */
    public function renderCheckoutElements(
        Mage_Core_Model_Layout $layout,
        $onlyUpdated = true
    ) {
        $result = array();

        if (!$this->legacySetup()) {
            $result['header-cart'] = $this->renderCheckoutElementMiniCart(
                $layout,
                $onlyUpdated
            );
        }
        
        $result['resursbank-checkout-shipping-methods-list'] = $this->renderCheckoutElementShippingMethods(
            $layout,
            $onlyUpdated
        );

        $result['current-coupon-code'] = $this->renderCheckoutElementCurrentCoupon(
            $layout,
            $onlyUpdated
        );

        foreach ($result as $id => $el) {
            if (is_null($el)) {
                unset($result[$id]);
            }
        }

        return $result;
    }

    /**
     * Render mini-cart element.
     *
     * @param Mage_Core_Model_Layout $layout
     * @param bool $onlyUpdated
     * @return null|string
     */
    public function renderCheckoutElementMiniCart(
        Mage_Core_Model_Layout $layout,
        $onlyUpdated = true
    ) {
        $result = null;

        /** @var Mage_Checkout_Block_Cart_Sidebar $block */
        $block = $layout->getBlock('minicart_content');

        if ($block) {
            // Render mini-cart element.
            $result = $block->toHtml();

            if ($onlyUpdated &&
                !$this->blockHasChanged($result, 'header-cart')
            ) {
                $result = null;
            }
        }

        return $result;
    }

    /**
     * Render element displaying currently applied coupon code.
     *
     * @param Mage_Core_Model_Layout $layout
     * @param bool $onlyUpdated
     * @return null|string
     */
    public function renderCheckoutElementCurrentCoupon(
        Mage_Core_Model_Layout $layout,
        $onlyUpdated = true
    ) {
        $result = null;

        /** @var Resursbank_Checkout_Block_Coupon $block */
        $block = Mage::getBlockSingleton('resursbank_checkout/coupon');

        if ($block) {
            // Render element.
            $result = $block->toHtml();

            if ($onlyUpdated &&
                !$this->blockHasChanged($result, 'current-coupon-code')
            ) {
                $result = null;
            }
        }

        return $result;
    }

    /**
     * Render shipping methods block displayed at checkout.
     *
     * @param Mage_Core_Model_Layout $layout
     * @param bool $onlyUpdated
     * @return null|string
     */
    public function renderCheckoutElementShippingMethods(
        Mage_Core_Model_Layout $layout,
        $onlyUpdated = true
    ) {
        $result = null;

        if ($this->shippingMethodsAvailable()) {
            /** @var Mage_Checkout_Block_Onepage_Shipping_Method_Available $block */
            $block = $layout->getBlock(
                'checkout.onepage.shipping_method.available'
            );

            if ($block) {
                // Set template.
                // Render element.
                $result = $block->toHtml();

                if ($onlyUpdated &&
                    !$this->blockHasChanged(
                        $result,
                        'resursbank-checkout-shipping-methods-list'
                    )
                ) {
                    $result = null;
                }
            }
        } else {
            // Reset the block with shipping methods, to avoid problems where it
            // would be rendered, then the placeholder is rendered, and then the
            // same shipping methods tries to render again (this could for
            // instance happen if you set your address, clear it from the quote
            // and the re-apply it).
            $this->getCheckoutSession()->setData(
                'resursbank-checkout-hash-resursbank-checkout-shipping-methods-list',
                null
            );

            // Display placeholder.
            $result = Mage::getStoreConfig(
                'resursbank_checkout/shipping/placeholder'
            );
        }

        return $result;
    }

    /**
     * Get one page checkout model.
     *
     * Copied from app/code/core/Mage/Checkout/controllers/OnepageController.php
     * (CE 1.9.2.4)
     *
     * @return Mage_Checkout_Model_Type_Onepage
     */
    public function getOnepage()
    {
        return Mage::getSingleton('checkout/type_onepage');
    }

    /**
     * Basically this will return true if you are running a version of Magento
     * below 1.9 (hence missing the RWD theme and a bunch of other stuff).
     *
     * @return bool
     */
    public function legacySetup()
    {
        return (bool) (version_compare(Mage::getVersion(), '1.9', '<'));
    }

    /**
     * Check if shopping cart is empty.
     *
     * @return bool
     */
    public function cartIsEmpty()
    {
        return ((float) Mage::helper('checkout/cart')->getItemsCount() < 1);
    }

    /**
     * Validate product quantity.
     *
     * @param Mage_Sales_Model_Quote_Item $item
     * @param float $qty
     * @return boolean
     * @throws Exception
     */
    public function validateQty(Mage_Sales_Model_Quote_Item $item, $qty)
    {
        if (!is_numeric($qty)) {
            throw new Exception($this->__('Invalid quantity'));
        }

        $filter = new Zend_Filter_LocalizedToNormalized(
            array('locale' => Mage::app()->getLocale()->getLocaleCode())
        );

        $qty = $filter->filter($qty);

        if (!$item->getProduct() instanceof Mage_Catalog_Model_Product) {
            throw new Exception($this->__('Failed to find the product.'));
        }

        if (count($item->getChildren())) {
            /** @var Mage_Sales_Model_Quote_Item $child */
            foreach ($item->getChildren() as $child) {
                $this->validateItemQty($child, $qty);
            }
        } else {
            $this->validateItemQty($item, $qty);
        }

        return true;
    }

    /**
     * Validate quote item quantity.
     *
     * @param Mage_Sales_Model_Quote_Item $item
     * @param float $qty
     * @return $this
     * @throws Exception
     */
    public function validateItemQty(Mage_Sales_Model_Quote_Item $item, $qty)
    {
        /** @var Mage_CatalogInventory_Model_Stock_Item $inventory */
        $inventory = Mage::getModel('cataloginventory/stock_item')
            ->loadByProduct($item->getProduct());

        if (!$inventory instanceof Mage_CatalogInventory_Model_Stock_Item) {
            throw new Exception(
                'Failed to find the stock item associated with the product.'
            );
        }

        $test = $inventory->checkQuoteItemQty($qty, $qty, $qty);

        if (isset($test['has_error']) && $test['has_error'] === true) {
            throw new Exception(
                (isset($test['message']) && !empty($test['message'])) ?
                    (string) $test['message'] :
                    $this->__('An unknown error occurred.')
            );
        }

        return $this;
    }

    /**
     * Rebuild cart contents from existing order.
     *
     * @param Mage_Sales_Model_Order $order
     * @return $this
     */
    public function rebuildCart(Mage_Sales_Model_Order $order)
    {
        /* @var $cart Mage_Checkout_Model_Cart */
        $cart = Mage::getSingleton('checkout/cart');

        /** @var Mage_Sales_Model_Resource_Order_Item_Collection $items */
        $items = $order->getItemsCollection();

        if (count($items)) {
            foreach ($items as $item) {
                $cart->addOrderItem($item);
            }
        }

        $cart->save();

        return $this;
    }

    /**
     * Check if shipping methods are available.
     *
     * @return bool
     */
    public function shippingMethodsAvailable()
    {
        /** @var Mage_Sales_Model_Quote $quote */
        $quote = null;
        $result = true;

        if (Mage::getStoreConfig('resursbank_checkout/shipping/availability') === 'after_shipping_address') {
            $quote = $this->getQuote();
            $result = (
                $quote->getShippingAddress()->getData('street') &&
                $quote->getShippingAddress()->getData('city') &&
                $quote->getShippingAddress()->getData('postcode') &&
                $quote->getShippingAddress()->getData('firstname') &&
                $quote->getShippingAddress()->getData('lastname')
            );
        }

        return (bool) $result;
    }

    /**
     * Checks if the provided $path matches the current module/controller/action
     * path.
     *
     * @param string $path (eg. catalog/product/view)
     * @return bool
     */
    public function isActivePath($path)
    {
        $current = (
            Mage::app()->getRequest()->getModuleName() .
            '/' .
            Mage::app()->getRequest()->getControllerName() .
            '/' .
            Mage::app()->getRequest()->getActionName()
        );

        return ($current === $path);
    }

    /**
     * Whether or not to use the Simplified API.
     *
     * @return bool
     */
    public function useSimplifiedFlow()
    {
        return Mage::getStoreConfig('resursbank_checkout/api/flow') ===
            Resursbank_Checkout_Model_System_Config_Source_Flow::FLOW_SIMPLIFIED;
    }

    /**
     * Whether or not to use the Hosted Flow API.
     *
     * @return bool
     */
    public function useHostedFlow()
    {
        return Mage::getStoreConfig('resursbank_checkout/api/flow') ===
            Resursbank_Checkout_Model_System_Config_Source_Flow::FLOW_HOSTED;
    }

    /**
     * Whether or not to use the Checkout API.
     *
     * @return bool
     */
    public function useCheckoutFlow()
    {
        return Mage::getStoreConfig('resursbank_checkout/api/flow') ===
            Resursbank_Checkout_Model_System_Config_Source_Flow::FLOW_CHECKOUT;
    }

    /**
     * Retrieve hash value based on currently configured API environment and
     * username. This is used to identify what environment/account payments are
     * created from. Useful when performing after-shop actions or automatic
     * order annulment.
     *
     * @return string
     * @throws Mage_Core_Model_Store_Exception
     */
    public function getApiAccountHash()
    {
        return sha1(
            $this->getApiModel()->getEnvironment() .
            '_' .
            $this->getApiModel()->getUsername()
        );
    }

    /**
     * Retrieve backend URL parameters.
     *
     * @return array
     */
    public function getBackendUrl($url)
    {
        return Mage::getModel('adminhtml/url')->getUrl(
            $url,
            $this->getBackendUrlParams()
        );
    }

    /**
     * Retrieve backend URL parameters.
     *
     * @return array
     */
    public function getBackendUrlParams()
    {
        $params = array(
            '_secure' => true
        );

        $store = Mage::app()->getRequest()->getParam('store');

        if ($store !== null) {
            $params['store'] = (string) $store;
        }

        $website = Mage::app()->getRequest()->getParam('website');

        if ($website !== null) {
            $params['website'] = (string) $website;
        }

        return $params;
    }
}
