<?php
/**
 * Copyright 2016 Resurs Bank AB
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 * Business logic to handle part payment related actions.
 *
 * Class Resursbank_Checkout_Helper_PartPayment
 */
class Resursbank_Checkout_Helper_PartPayment extends Resursbank_Checkout_Helper_Data
{
    /**
     * Retrieve configured method ID.
     *
     * @return int
     */
    public function getMethodId()
    {
        return (int) Mage::getStoreConfig(
            'resursbank_checkout/part_payment/payment_method'
        );
    }

    /**
     * Retrieve model instance based on configured method ID.
     *
     * @return Resursbank_Checkout_Model_Account_Method
     */
    public function getMethod()
    {
        $result = Mage::getModel('resursbank_checkout/account_method');

        if ($this->getMethodId() !== 0) {
            $result->load($this->getMethodId());
        }

        return $result;
    }

    /**
     * Retrieve configured method identifier.
     *
     * @return string
     */
    public function getMethodIdentifier()
    {
        return $this->getMethod()->getIdentifier();
    }

    /**
     * Retrieve configured annuity factor ID.
     *
     * @return int
     */
    public function getAnnuityId()
    {
        return (int) Mage::getStoreConfig(
            'resursbank_checkout/part_payment/duration'
        );
    }

    /**
     * Retrieve model instance based on configured method annuity ID.
     *
     * @return Resursbank_Checkout_Model_Account_Method_Annuity
     */
    public function getAnnuity()
    {
        $result = Mage::getModel('resursbank_checkout/account_method_annuity');

        if ($this->getAnnuityId() !== 0) {
            $result->load($this->getAnnuityId());
        }

        return $result;
    }

    /**
     * Retrieve a collection of all payment methods which have annuity factors
     * attached to them. In other words, retrieve all payment methods available
     * as part payment alternatives.
     *
     * @return Resursbank_Checkout_Model_Resource_Account_Method_Collection
     */
    public function getMethodsCollection()
    {
        $factors = Mage::getModel('resursbank_checkout/account_method_annuity')
            ->getCollection()
            ->addFieldToSelect('method_id');

        $factors->getSelect()->group('method_id');

        $methodIds = [];

        /** @var $method Resursbank_Checkout_Model_Account_Method_Annuity */
        foreach ($factors as $factor) {
            $methodIds[] = $factor->getMethodId();
        }

        return Mage::getModel('resursbank_checkout/account_method')
            ->getCollection()
            ->addFieldToFilter('method_id', $methodIds);
    }

    /**
     * Retrieve calculated part payment price.
     *
     * @param float $price
     * @param float $factor
     * @return float
     */
    public function calculate($price, $factor)
    {
        $result = 0.0;

        $price = (float) $price;
        $factor = (float) $factor;

        try {
            if ($factor > 0 && $this->canCalculate($price)) {
                $result = (float) round(($price * $factor) * 100) / 100;
            }
        } catch (Exception $e) {
            $this->debugLog($e);
        }

        return round($result, 2);
    }

    /**
     * Check if part payment widget is enabled.
     *
     * For the part payment widget to count as enabled you must have:
     *
     * 1) Enabled the extension itself.
     * 2) Enabled the part payment widget.
     * 3) Assigned a payment method to base part payment calculations on.
     * 4) Assigned a duration to base part payment calculations on.
     *
     * @param int $storeId
     * @return boolean
     * @throws Mage_Core_Model_Store_Exception
     */
    public function isEnabled($storeId = null)
    {
        $storeId = is_null($storeId) ? $this->getStoreId() : $storeId;

        $result = parent::isEnabled($storeId);
        $enabledWidget = Mage::getStoreConfigFlag(
            'resursbank_checkout/part_payment/enabled',
            $storeId
        );

        return (
            $result &&
            $enabledWidget &&
            $this->getMethodId() &&
            $this->getAnnuityId()
        );
    }

    /**
     * Check if price is eligible for part payment plans.
     *
     * @param float $price
     * @return bool
     */
    private function canCalculate($price)
    {
        $price = (float) $price;

        $minPrice = $this->getMethod()->getMinOrderTotal();
        $maxPrice = $this->getMethod()->getMaxOrderTotal();

        return ($price >= $minPrice && $price <= $maxPrice) ||
            ($minPrice === 0.0 && $maxPrice === 0.0);
    }
}
