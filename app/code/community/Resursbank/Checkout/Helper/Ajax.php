<?php
/**
 * Copyright 2016 Resurs Bank AB
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 * General AJAX functionality, mostly used to handle AJAX call to the cart.
 *
 * Class Resursbank_Checkout_Helper_Ajax
 */
class Resursbank_Checkout_Helper_Ajax extends Resursbank_Checkout_Helper_Data
{
    /**
     * Clear redirect on controller.
     *
     * @param Mage_Core_Controller_Front_Action|Mage_Adminhtml_Controller_Action $controller
     * @return $this
     * @throws Zend_Controller_Response_Exception
     */
    public function correctResponseHeaders(&$controller)
    {
        // avoid redirects
        $controller->getResponse()
            ->clearHeader('Location')
            ->setHttpResponseCode(200);

        // avoid forwards
        $controller->getRequest()->setDispatched(true);

        return $this;
    }

    /**
     * Transfer messages from sessions to referenced array.
     *
     * @param array $data
     * @param array $strict (only include specified message types).
     * @param boolean $clear (remove messages from sessions after collecting them).
     * @param array $sessions
     * @return $this
     */
    public function collectMessages(
        array &$data,
        $strict = array(),
        $clear = true,
        array $sessions = array()
    ) {
        $this->fillSessionsArray($sessions);

        foreach ($sessions as $session) {
            /** @var Mage_Core_Model_Session_Abstract $model */
            $model = Mage::getSingleton($session);

            if ($model) {
                /** @var Mage_Core_Model_Message_Collection $messageCollection */
                $messageCollection = $model->getData('messages');

                if ($messageCollection &&
                    count($messageCollection->getItems())
                ) {
                    /** @var Mage_Core_Model_Message $message */
                    foreach ($messageCollection->getItems() as $message) {
                        $type = (string) $message->getType();

                        if (!empty($type) &&
                            (!count($strict) || in_array($type, $strict))
                        ) {
                            if (!isset($data[$type])) {
                                $data[$type] = array();
                            }

                            if (isset($data[$type]) &&
                                is_array($data[$type]) &&
                                !in_array($message->getCode(), $data[$type])
                            ) {
                                $data[$type][] = $message->getCode();
                            }
                        }
                    }
                }
            }
        }

        if ($clear) {
            $this->clearSessionMessages($sessions);
        }

        return $this;
    }

    /**
     * Clear out all session messages.
     *
     * @param array $sessions
     * @return $this
     */
    public function clearSessionMessages(array $sessions = array())
    {
        $this->fillSessionsArray($sessions);

        foreach ($sessions as $session) {
            /** @var Mage_Core_Model_Session_Abstract $model */
            $model = Mage::getSingleton($session);

            if ($model) {
                $model->getData('messages')->clear();
            }
        }

        return $this;
    }

    /**
     * Fill passed sessions array.
     *
     * @param array $sessions
     * @return $this
     */
    public function fillSessionsArray(array &$sessions = array())
    {
        if (!count($sessions)) {
            if ($this->isAdmin()) {
                $sessions = array(
                    'core/session',
                    'adminhtml/session'
                );
            } else {
                $sessions = array(
                    'core/session',
                    'customer/session',
                    'checkout/session'
                );
            }
        }

        return $this;
    }

    /**
     * Retrieve action result shell array.
     *
     * @return array
     */
    public function getActionResultShell()
    {
        return array(
            'message' => array(
                'success'   => array(),
                'error'     => array(),
                'notice'    => array()
            ),
            'elements' => array()
        );
    }
}
