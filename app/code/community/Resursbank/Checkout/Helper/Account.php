<?php
/**
 * Copyright 2016 Resurs Bank AB
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 * Account related business logic.
 *
 * Class Resursbank_Checkout_Helper_Account
 */
class Resursbank_Checkout_Helper_Account extends Resursbank_Checkout_Helper_Data
{
    /**
     * Loads the current account model by using the entered API credentials
     * from the configuration.
     *
     * @return Resursbank_Checkout_Model_Account
     * @throws Mage_Core_Model_Store_Exception
     */
    public function getCurrentAccount()
    {
        return $this->account()->loadByCredentials($this->getCredentials());
    }

    /**
     * Get API settings credentials that has been set in the configuration.
     *
     * @return Resursbank_Checkout_Model_Api_Credentials
     * @throws Mage_Core_Model_Store_Exception
     */
    private function getCredentials()
    {
        return $this->credentials()
            ->loadFromConfig($this->getStore());
    }

    /**
     * Retrieve account model.
     *
     * @return Resursbank_Checkout_Model_Account
     */
    private function account()
    {
        return Mage::getModel('resursbank_checkout/account');
    }

    /**
     * Retrieve API credentials model.
     *
     * @return Resursbank_Checkout_Model_Api_Credentials
     */
    private function credentials()
    {
        return Mage::getModel('resursbank_checkout/api_credentials');
    }

    /**
     * Retrieve instance of store model for current store view.
     *
     * @return Mage_Core_Model_Store
     * @throws Mage_Core_Model_Store_Exception
     */
    private function getStore()
    {
        return Mage::getModel('core/store')->load(
            $this->getStoreId()
        );
    }
}
