<?php
/**
 * Copyright 2016 Resurs Bank AB
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 * Class Resursbank_Checkout_Helper_Rewrite_Payment_Data
 */
class Resursbank_Checkout_Helper_Rewrite_Payment_Data extends Mage_Payment_Helper_Data
{
    /**
     * Cache key for method list.
     *
     * @var string
     */
    const CACHE_KEY_METHOD_LIST = 'resursbank_checkout_methods_list_';

    /**
     * Retrieve method model object.
     *
     * NOTE: we modify the created instance if it belongs to Resurs Bank. We
     * apply the methods appropriate code on the instance. This way we do not
     * require a separate physical class for each of our payment methods.
     *
     * @param string $code
     * @return Mage_Payment_Model_Method_Abstract|false
     */
    public function getMethodInstance($code)
    {
        $instance = parent::getMethodInstance($code);

        if (Mage::helper('resursbank_checkout')->isEnabled() &&
            $instance instanceof Resursbank_Checkout_Model_Payment_Method_Resursbank_Default
        ) {
            $instance->setCode($code);
        }

        return $instance;
    }

    /**
     * Append our dynamic payment methods to the list of methods assembled by
     * Magento. This is essentially a copy of the original method, with the
     * addition of the setCode() statement to correct the method code on the
     * model instances (occurs in the getResursBankMethodInstance() method).
     *
     * @param mixed $store
     * @param Mage_Sales_Model_Quote $quote
     * @return array
     */
    public function getStoreMethods($store = null, $quote = null)
    {
        $result = parent::getStoreMethods($store, $quote);

        try {
            // Append our dynamic methods to result.
            $result = array_merge(
                $result,
                $this->getResursBankMethods($store, $quote)
            );

            // Sort everything again to support our appended methods.
            usort($result, array($this, '_sortMethods'));
        } catch (Exception $e) {
            Mage::helper('resursbank_checkout')->debugLog($e);
        }

        return $result;
    }

    /**
     * Retrieve array of Resurs Bank payment method instances.
     *
     * @param null $store
     * @param null $quote
     * @return mixed
     * @throws Mage_Core_Model_Store_Exception
     */
    private function getResursBankMethods($store = null, $quote = null)
    {
        $result = $this->cache()->load($this->getCacheKey($store));

        if (!is_array($result)) {
            $result = [];

            /** @var Resursbank_Checkout_Model_Account_Method $method */
            foreach ($this->getResursBankMethodCollection($store) as $method) {
                // Add Resurs Bank payment method instance to result.
                $result[] = $this->getResursBankMethodInstance(
                    $method->getCode(),
                    $store
                );
            }

            $this->cache()->save($result, $this->getCacheKey($store));
        }

        // Filter out unavailable methods, based on supplied quote so the result
        // from this should not be cached.
        $this->filterUnavailableMethod($result, $quote);

        return $result;
    }

    /**
     * Retrieve Resurs Bank payment method instance.
     *
     * @param string $code
     * @param null $store
     * @return Resursbank_Checkout_Model_Payment_Method_Resursbank_Default
     */
    private function getResursBankMethodInstance($code, $store = null)
    {
        /** @var Resursbank_Checkout_Model_Payment_Method_Resursbank_Default $instance */
        $instance = Mage::getModel(
            'resursbank_checkout/payment_method_resursbank_default'
        )->setStore($store)
        ->setCode((string) $code);

        // Set sort order.
        $sortOrder = (int) $instance->getConfigData(
            'sort_order',
            $store
        );

        $instance->setSortOrder($sortOrder);

        return $instance;
    }

    /**
     * Filter unavailable methods based on supplied quote.
     *
     * @param array $methods
     * @param null $quote
     * @throws Mage_Core_Model_Store_Exception
     */
    private function filterUnavailableMethod(array &$methods, $quote = null)
    {
        /** @var Resursbank_Checkout_Model_Payment_Method_Resursbank_Default $method */
        foreach ($methods as $index => $method) {
            if (!$method->isAvailable($quote)) {
                unset($methods[$index]);
            }
        }
    }

    /**
     * Retrieve collection of payment methods available to supplied store view.
     *
     * @param null $store
     * @return Resursbank_Checkout_Model_Resource_Method_Collection
     */
    private function getResursBankMethodCollection($store = null)
    {
        /** @var Resursbank_Checkout_Model_Api_Credentials $credentials */
        $credentials = Mage::getModel('resursbank_checkout/api_credentials')
            ->loadFromConfig(Mage::app()->getStore($store));

        /** @var Resursbank_Checkout_Model_Account $account */
        $account = Mage::getModel('resursbank_checkout/account')
            ->loadByCredentials($credentials);

        /** @var Resursbank_Checkout_Model_Resource_Method_Collection $methods */
        return Mage::getModel('resursbank_checkout/account_method')
            ->getCollection()
            ->addFieldToFilter('account_id', $account->getId());
    }

    /**
     * Retrieve cache helper instance.
     *
     * @return Resursbank_Checkout_Helper_Cache
     */
    private function cache()
    {
        return Mage::helper('resursbank_checkout/cache');
    }

    /**
     * Retrieve cache key based on store view.
     *
     * @param null $store
     * @return string
     * @throws Mage_Core_Model_Store_Exception
     */
    private function getCacheKey($store = null)
    {
        return self::CACHE_KEY_METHOD_LIST .
            Mage::app()->getStore($store)->getId();
    }
}
