<?php
/**
 * Copyright 2016 Resurs Bank AB
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

class Resursbank_Checkout_Helper_Log extends Mage_Core_Helper_Abstract
{
    /**
     * General log file. This contains all things which do not belong elsewhere.
     */
    const GENERAL = 'resursbank_general.log';

    /**
     * Cache log file. This contains information about data read from/written to
     * Magento's cache.
     */
    const CACHE = 'resursbank_cache.log';

    /**
     * API log file. Contains outgoing API calls and responses.
     */
    const API = 'resursbank_api.log';

    /**
     * Callback log file. Contains incoming callback calls.
     */
    const CALLBACK = 'resursbank_callback.log';

    /**
     * ECom log file. Contains outgoing calls through the ECom library.
     * After-shop functions.
     */
    const ECOM = 'resursbank_ecom.log';

    /**
     * Log something.
     *
     * @param string|array|Exception $message
     * @param null|string $file
     * @param bool $force
     * @return $this
     */
    public function entry($message, $file = null, $force = false)
    {
        $file = (string)$file;

        if (empty($file)) {
            $file = self::GENERAL;
        }

        if ($message instanceof Exception) {
            $message = "\n----------------------------------------------------------------------------"
                . "--------------------------------------------------------------------------------"
                . "\nFile: "
                . $message->getFile()
                . "\nLine: "
                . $message->getLine()
                . "\nMessage: "
                . $message->getMessage()
                . "\n------------------------------------------------------------------------------"
                . "------------------------------------------------------------------------------\n"
                . $message->getTraceAsString();
        }

        Mage::log((string)$message, null, $file, $force);

        return $this;
    }
}
