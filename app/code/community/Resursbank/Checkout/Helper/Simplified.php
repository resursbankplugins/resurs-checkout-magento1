<?php
/**
 * Copyright 2016 Resurs Bank AB
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 * Helper methods specifically meant for Simplified Flow.
 *
 * Class Resursbank_Checkout_Helper_Simplified
 */
class Resursbank_Checkout_Helper_Simplified extends Mage_Core_Helper_Abstract
{
    /**
     * @return Resursbank_Checkout_Model_Api_Adapter_Simplified
     */
    public function getAdapter()
    {
        return Mage::getModel('resursbank_checkout/api_adapter_simplified');
    }

    /**
     * Retrieve customer type based on input made during checkout process.
     *
     * @return string
     */
    public function getCustomerTypeFromSession()
    {
        return $this->getAdapter()->getIsCompany() ?
            Resursbank_Checkout_Model_Api::CUSTOMER_TYPE_COMPANY :
            Resursbank_Checkout_Model_Api::CUSTOMER_TYPE_PRIVATE;
    }
}
