<?php
/**
 * Copyright 2016 Resurs Bank AB
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 * Functionality specifically made for callback handling.
 *
 * Class Resursbank_Checkout_Helper_Callback
 */
class Resursbank_Checkout_Helper_Callback extends Resursbank_Checkout_Helper_Data
{
    /**
     * Number of characters in randomly generated callback salt.
     */
    const SALT_LENGTH = 64;

    /**
     * Characters eligible for salt.
     */
    const SALT_CHARSET = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789';

    /**
     * Name of callback received column on order table.
     *
     * @var string
     */
    const CALLBACK_RECEIVED_COLUMN = 'resursbank_callback_received';

    /**
     * Dispatches an event before a callback is processed.
     *
     * @param string $callback
     * @param string $paymentId
     * @return $this
     * @throws Exception
     */
    public function dispatchBeforeCallbackEvent($callback, $paymentId)
    {
        Mage::dispatchEvent(
            (
                'resursbank_checkout_callback_' .
                strtolower((string)$callback) .
                '_before'
            ),
            array(
                'order' => $this->getOrderFromRequest($paymentId),
                'parameters' => Mage::app()->getRequest()->getParams()
            )
        );

        return $this;
    }

    /**
     * Dispatches an event after a callback has been processed.
     *
     * @param string $callback
     * @param string $paymentId
     * @return $this
     * @throws Exception
     */
    public function dispatchAfterCallbackEvent($callback, $paymentId)
    {
        Mage::dispatchEvent(
            (
                'resursbank_checkout_callback_' .
                strtolower((string)$callback) .
                '_after'
            ),
            array(
                'order' => $this->getOrderFromRequest($paymentId),
                'parameters' => Mage::app()->getRequest()->getParams()
            )
        );

        return $this;
    }

    /**
     * Retrieve order object from callback parameter paymentId.
     *
     * @param string $paymentId
     * @return Mage_Sales_Model_Order
     * @throws Exception
     */
    public function getOrderFromRequest($paymentId)
    {
        $paymentId = (string)$paymentId;

        if (empty($paymentId)) {
            throw new Exception('No payment reference supplied.');
        }

        /** @var Mage_Sales_Model_Order $order */
        $order = Mage::getModel('sales/order')
            ->loadByAttribute(
                'increment_id',
                $paymentId
            );

        if (!($order instanceof Mage_Sales_Model_Order) || !$order->getId()) {
            throw new Exception('Failed to locate referenced order.');
        }

        return $order;
    }

    /**
     * Cancel order.
     *
     * @param Mage_Sales_Model_Order $order
     * @return $this
     * @throws Exception
     */
    public function annulOrder(Mage_Sales_Model_Order $order)
    {
        $order->cancel()->save();

        return $this;
    }

    /**
     * Add order comment.
     *
     * @param Mage_Sales_Model_Order $order
     * @param string $comment
     * @throws Exception
     */
    public function addOrderComment(Mage_Sales_Model_Order $order, $comment)
    {
        if (!is_string($comment)) {
            throw new Exception('Comment must be strings.');
        }

        $order->addStatusHistoryComment($comment, false);
        $order->save();
    }

    /**
     * Set order status.
     *
     * @param Mage_Sales_Model_Order $order
     * @param string $status
     * @return $this
     * @throws Exception
     */
    public function setOrderStatus(Mage_Sales_Model_Order $order, $status)
    {
        $order->setStatus($status)->save();

        return $this;
    }

    /**
     * Set callback received flag.
     *
     * @param Mage_Sales_Model_Order $order
     * @param boolean $value
     * @return $this
     */
    public function setOrderCallbackFlag(Mage_Sales_Model_Order $order, $value)
    {
        $order->setData(self::CALLBACK_RECEIVED_COLUMN, (bool) $value)->save();

        return $this;
    }

    /**
     * Send order confirmation email.
     *
     * @param Mage_Sales_Model_Order $order
     * @return $this
     */
    public function sendOrderEmail(Mage_Sales_Model_Order $order)
    {
        $order->sendNewOrderEmail();

        return $this;
    }

    /**
     * Retrieve SALT key for current API account.
     *
     * @return string
     * @throws Exception
     */
    public function getSalt()
    {
        return $this->accountHelper()->getCurrentAccount()->getSalt();
    }

    /**
     * Generate SALT key and associate with current API account.
     *
     * @return string
     * @throws Exception
     */
    public function generateSalt()
    {
        $salt = '';

        /** @var Resursbank_Checkout_Model_Account $account */
        $account = $this->accountHelper()->getCurrentAccount();

        if ($account->getId()) {
            $salt = $this->strRand(
                self::SALT_LENGTH,
                self::SALT_CHARSET
            );

            $account->setSalt($salt)->save();
        }

        return $salt;
    }

    /**
     * Retrieve instance of account helper.
     *
     * @return Resursbank_Checkout_Helper_Account
     */
    private function accountHelper()
    {
        return Mage::helper('resursbank_checkout/account');
    }
}
