<?php
/**
 * Copyright 2016 Resurs Bank AB
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 * Class Resursbank_Checkout_Helper_Cache
 */
class Resursbank_Checkout_Helper_Cache extends Mage_Core_Helper_Abstract
{
    /**
     * General cache tag for everything processed using our module.
     *
     * @var string
     */
    const CACHE_TAG = 'resursbank_checkout';

    /**
     * Save cache data.
     *
     * @param mixed $data
     * @param string $id
     * @param array $tags
     * @param int $lifetime (in seconds)
     * @return Resursbank_Checkout_Helper_Cache
     */
    public function save($data, $id, $tags = array(), $lifetime = null)
    {
        try {
            if ($this->isCacheEnabled()) {
                $data = serialize($data);

                $tags[] = self::CACHE_TAG;

                $this->log(
                    'Saving cache with id "' .
                    $id .
                    '", data length "'.
                    strlen($data) .
                    '" and tags "' .
                    implode(', ', $tags) . '"'
                );

                Mage::getModel('core/cache')->save(
                    $data,
                    $id,
                    $tags,
                    $lifetime
                );
            }
        } catch (Exception $e) {
            $this->log($e);
        }

        return $this;
    }

    /**
     * Load cache data.
     *
     * @param string $id
     * @param null $default
     * @return mixed
     */
    public function load($id, $default = null)
    {
        $result = $default;

        if ($this->isCacheEnabled()) {
            try {
                $this->log('Loading cache with id ' . $id);

                $result = Mage::getModel('core/cache')->load($id);

                if (is_string($result)) {
                    $result = unserialize($result);
                }
            } catch (Exception $e) {
                $this->log($e);
            }
        }

        return $result;
    }

    /**
     * Clean cache tags.
     *
     * @param array $tags
     * @return Resursbank_Checkout_Helper_Cache
     */
    public function clean(array $tags = [])
    {
        Mage::getModel('core/cache')->clean(
            array_merge($tags, [self::CACHE_TAG])
        );

        return $this;
    }

    /**
     * Check if extension caching is enabled.
     *
     * @param string $type
     * @return bool
     */
    public function isCacheEnabled($type = '')
    {
        return Mage::app()->useCache(
            (empty($type) ? strtolower(self::CACHE_TAG) : $type)
        );
    }

    /**
     * Log message.
     *
     * @param string $message
     * @return $this
     */
    public function log($message)
    {
        Mage::helper('resursbank_checkout/log')->entry(
            $message,
            Resursbank_Checkout_Helper_Log::CACHE
        );

        return $this;
    }
}
