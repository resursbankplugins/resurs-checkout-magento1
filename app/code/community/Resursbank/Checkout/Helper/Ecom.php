<?php
/**
 * Copyright 2016 Resurs Bank AB
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

//define('ECOM_SKIP_AUTOLOAD', true);
require_once(Mage::getBaseDir() . '/lib/Resursbank/Checkout/vendor/autoload.php');

/**
 * Class Resursbank_Checkout_Helper_Ecom
 */
class Resursbank_Checkout_Helper_Ecom extends Resursbank_Checkout_Helper_Data
{
    /**
     * @param Resursbank_Checkout_Model_Api_Credentials|null $credentials
     * @return \Resursbank\RBEcomPHP\ResursBank
     * @throws Mage_Core_Model_Store_Exception
     */
    public function getConnection(
        Resursbank_Checkout_Model_Api_Credentials $credentials = null
    ) {
        $this->resolveCredentials($credentials);

        $connection = new \Resursbank\RBEcomPHP\ResursBank(
            $credentials->getUsername(),
            $credentials->getPassword(),
            $credentials->getEnvironment()
        );

        // Setup in-depth connection information.
        $this->setConnectionFlowInformation($connection);

        return $connection;
    }

    /**
     * Resolve API credentials from current store view if none are provided.
     *
     * @param Resursbank_Checkout_Model_Api_Credentials|null $credentials
     * @throws Mage_Core_Model_Store_Exception
     */
    private function resolveCredentials(
        Resursbank_Checkout_Model_Api_Credentials &$credentials = null
    ) {
        if (is_null($credentials)) {
            $credentials = Mage::getModel('resursbank_checkout/api_credentials')
                ->loadFromConfig(Mage::app()->getStore($this->getStoreId()));
        }
    }

    /**
     * Assign the configured flow (API service) and relevant parameters to the
     * currently initiated connection instance on this singleton.
     *
     * @param \Resursbank\RBEcomPHP\ResursBank $connection
     * @throws Exception
     */
    private function setConnectionFlowInformation(
        \Resursbank\RBEcomPHP\ResursBank &$connection
    ) {
        /** @var Resursbank_Checkout_Model_Api $api */
        $api = Mage::getModel('resursbank_checkout/api');

        // As of ECom 1.1.39-stable, simplified flow also supports payment provider methods
        $connection->setSimplifiedPsp(true);
        $connection->setUserAgent($api->getUserAgent());
        $connection->setAutoDebitableTypes(false);
        // Fix for a bug where articles are not merged together
        // $connection->setFlag('ALWAYS_RENDER_TOTALS');

        // Set preferred flow to be used for outgoing connections (API).
        if ($this->useSimplifiedFlow()) {
            // Assign Simplified Flow API service.
            $connection->setPreferredPaymentFlowService(
                \Resursbank\RBEcomPHP\RESURS_FLOW_TYPES::SIMPLIFIED_FLOW
            );
        } elseif ($this->useHostedFlow()) {
            // Assign Hosted Flow API service.
            $connection->setPreferredPaymentFlowService(
                \Resursbank\RBEcomPHP\RESURS_FLOW_TYPES::HOSTED_FLOW
            );
        } else {
            // Assign Checkout Flow API service.
            $connection->setPreferredPaymentFlowService(
                \Resursbank\RBEcomPHP\RESURS_FLOW_TYPES::RESURS_CHECKOUT
            );
        }
    }

    /**
     * Retrieve environment to use for ECom calls.
     *
     * @return int
     * @throws Mage_Core_Model_Store_Exception
     */
    public function getEnvironment()
    {
        return Mage::helper('resursbank_checkout')->getApiModel()->getEnvironment() === 'production' ?
            \Resursbank\RBEcomPHP\RESURS_ENVIRONMENTS::PRODUCTION :
            \Resursbank\RBEcomPHP\RESURS_ENVIRONMENTS::TEST;
    }

    /**
     * Check if ECom integration is enabled.
     *
     * @param int|null $storeId
     * @return bool
     * @throws Mage_Core_Model_Store_Exception
     */
    public function isEnabled($storeId = null)
    {
        return (
            parent::isEnabled($storeId) &&
            Mage::getStoreConfigFlag(
                'resursbank_checkout/ecom/enabled',
                (
                    is_null($storeId) ?
                        $this->getStoreId() :
                        $storeId
                )
            )
        );
    }

    /**
     * Check if a provided payment session has a certain status applied.
     *
     * @param \Resursbank\RBEcomPHP\resurs_payment $session
     * @param string $status
     * @return bool
     */
    public function paymentSessionHasStatus(
        $session,
        $status
    ) {
        $status = strtoupper((string) $status);

        return (isset($session->status) &&
            (
                (
                    is_array($session->status) &&
                    in_array($status, $session->status)
                ) ||
                (
                    is_string($session->status) &&
                    strtoupper($session->status) === $status
                )
            )
        );
    }

    /**
     * Get callback event id based on it's name.
     *
     * @param string $name
     * @return int
     */
    public function getCallbackEventId($name)
    {
        switch ($name) {
            case 'unfreeze':
                $result = \Resursbank\RBEcomPHP\RESURS_CALLBACK_TYPES::UNFREEZE;
                break;
            case 'annulment':
                $result = \Resursbank\RBEcomPHP\RESURS_CALLBACK_TYPES::ANNULMENT;
                break;
            case 'automaticFraudControl':
                $result = \Resursbank\RBEcomPHP\RESURS_CALLBACK_TYPES::AUTOMATIC_FRAUD_CONTROL;
                break;
            case 'finalization':
                $result = \Resursbank\RBEcomPHP\RESURS_CALLBACK_TYPES::FINALIZATION;
                break;
            case 'test':
                $result = \Resursbank\RBEcomPHP\RESURS_CALLBACK_TYPES::TEST;
                break;
            case 'update':
                $result = \Resursbank\RBEcomPHP\RESURS_CALLBACK_TYPES::UPDATE;
                break;
            case 'booked':
                $result = \Resursbank\RBEcomPHP\RESURS_CALLBACK_TYPES::BOOKED;
                break;
            default:
                $result = \Resursbank\RBEcomPHP\RESURS_CALLBACK_TYPES::NOT_SET;
        }

        return $result;
    }

    /**
     * Retrieve cache helper.
     *
     * @return Resursbank_Checkout_Helper_Cache
     */
    public function cache()
    {
        return Mage::helper('resursbank_checkout/cache');
    }

    /**
     * Check if there is a payment attached to an order.
     *
     * Please note that we will receive a SoapFault exception if the API is
     * offline. If the exception is an instance of SoapFault, or if it's not
     * validated as an Exception caused by a missing payment reference, we
     * forward the exception upstream.
     *
     * @param Mage_Sales_Model_Order $order
     * @return bool
     * @throws Exception
     */
    public function validatePayment(Mage_Sales_Model_Order $order)
    {
        $payment = null;

        try {
            if ($this->validatePaymentMethod($order)) {
                // Set store id in context to API calls for correct credentials.
                Mage::helper('resursbank_checkout')->setStoreId(
                    $order->getStoreId()
                );
                if (method_exists($this, 'setStoreId')) {
                    $this->setStoreId($order->getStoreId());
                }

                $payment = $this->getConnection()->getPayment(
                    $order->getIncrementId(),
                    true
                );
            }
        } catch (Exception $e) {
            // If there is no payment we will receive an Exception from ECom.
            if (!$this->validateMissingPaymentException($e)) {
                throw $e;
            }
        }

        return $payment !== null;
    }

    /**
     * Check if the first 11 characters in the payment method code matches
     * Resursbank_Checkout_Model_Payment_Method_Resursbank_Default::CODE_PREFIX
     * and therefore is a payment method provided by Resurs Bank.
     *
     * @param Mage_Sales_Model_Order $order
     * @return bool
     */
    public function validatePaymentMethod(
        Mage_Sales_Model_Order $order
    ) {
        // Resurs Bank payment method code prefix.
        $prefix = Resursbank_Checkout_Model_Payment_Method_Resursbank_Default::CODE_PREFIX;

        // Retrieve prefix from the payment method used on the provided order,
        // if any.
        $orderMethodPrefix = substr(
            $order->getPayment()->getMethod(),
            0,
            strlen($prefix)
        );

        return $orderMethodPrefix === $prefix;
    }

    /**
     * Validate that an Exception was thrown because a payment was actually
     * missing.
     *
     * @param Exception $e
     * @return bool
     */
    public function validateMissingPaymentException(Exception $e)
    {
        return (
            $e->getCode() === 3 ||
            $e->getCode() === 8
        );
    }

    /**
     * Tests if the API is responsive by fetching available payment methods.
     * There might be a better way of doing this.
     *
     * @return boolean
     */
    public function isApiOnline()
    {
        $result = true;

        try {
            $this->getConnection()->getPaymentMethods();
        } catch (Exception $e) {
            $result = false;
        }

        return $result;
    }
}
