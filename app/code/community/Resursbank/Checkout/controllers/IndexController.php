<?php
/**
 * Copyright 2016 Resurs Bank AB
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

require_once(
    Mage::getModuleDir('controllers', 'Mage_Checkout') .
    DS .
    'OnepageController.php'
);

class Resursbank_Checkout_IndexController extends Mage_Checkout_OnepageController
{
    /**
     * @var Resursbank_Checkout_Helper_Data
     */
    protected $helper;

    /**
     * Constructor.
     *
     * Setup commonly used resources.
     */
    protected function _construct()
    {
        parent::_construct();

        $this->helper = Mage::helper('resursbank_checkout');
    }

    /**
     * @return Mage_Checkout_OnepageController
     * @throws Mage_Core_Model_Store_Exception
     */
    public function preDispatch()
    {
        if (!$this->helper->isEnabled()) {
            $this->_redirect('checkout');
        }

        $action = Mage::app()->getRequest()->getActionName();

        if ($action === 'saveBilling') {
            $this->parseJsonParameter('billing');
        } elseif ($action === 'saveShipping') {
            $this->parseJsonParameter('shipping');
        } elseif ($action === 'savePayment') {
            $this->parseJsonParameter('payment');
            $this->correctPaymentMethod();
        }

        return parent::preDispatch();
    }

    /**
     * Index action, sets default address information. starts payment session.
     */
    public function indexAction()
    {

        try {
            if (!$this->helper->getApiModel()->paymentSessionInitialized()) {
                // Assign default address information.
                $this->assignDefaultAddressInformationToQuote();

                // Assign default payment method.
                $this->assignDefaultPaymentMethod();

                // Assign default shipping method.
                $this->assignDefaultShippingMethod();

                // Initialize payment session (if needed).
                $this->helper->initPaymentSession();
            }
        } catch (Exception $e) {
            // Debug log.
            $this->helper->debugLog($e);
        }

        // Run parent action.
        parent::indexAction();
    }

    /**
     * Save order object.
     */
    public function saveOrderAction()
    {
        if ($this->_expireAjax()) {
            return;
        }

        $result = Mage::helper('resursbank_checkout/ajax')
            ->getActionResultShell();

        try {
            if (!$this->_validateFormKey()) {
                Mage::throwException('Invalid form key');
            }

            /**
             * @var Mage_Sales_Model_Quote
             */
            $quote = $this->getOnepage()->getQuote();

            // Collect totals on quote (must proceed order saving).
            $quote->collectTotals();

            // Run parent action.
            parent::saveOrderAction();

            // Extract errors form response body (from parent function).
            $errors = $this->extractErrorsFromParentResponseBody(
                $this->getResponse()->getBody()
            );

            // Send back grand total so that the order isn't placed at payment
            // admin if it's 0.
            $result['message']['grand_total'] = $quote->getGrandTotal();

            if (count($errors)) {
                $result['message']['error'] = $errors;
            } else {
                // Render and append updated checkout elements to $result.
                $this->renderUpdatedElements($result);

                // Clear redirect and collect messages.
                Mage::helper('resursbank_checkout/ajax')->collectMessages(
                    $result['message']
                );

                // Clear payment session information from checkout session.
                $this->helper->clearPaymentSession();
            }
        } catch (Exception $e) {
            $result['message']['error'][] = $e->getMessage();
        }

        // Correct response headers.
        Mage::helper('resursbank_checkout/ajax')->correctResponseHeaders($this);

        $this->getResponse()->setHeader('Content-type', 'application/json');
        $this->getResponse()->setBody(Zend_Json::encode($result));
    }

    /**
     * Store billing address on quote object.
     *
     * Expected parameters:
     *
     *  'billing' => array(
     *      'company'           => '',                  // Optional
     *      'email'             => 'test@info.com',     // Required
     *      'street'            => array(
     *          'First street line', 'Second street line'
     *      ),                                          // Required
     *      'vat_id'            => '',                  // Optional
     *      'city'              => 'New York',          // Required
     *      'region'            => 'New York',          // Optional
     *      'postcode'          => '12312',             // Required
     *      'telephone'         => '123123123',         // Required
     *      'fax'               => '',                  // Optional
     *      'firstname'         => 'Carl',              // Required
     *      'lastname'          => 'Jan',               // Required
     *      'country_id'        => 'se'                 // Required,
     *      'use_for_shipping'  => 1                    // Use billing address.
     *  );
     */
    public function saveBillingAction()
    {
        if ($this->_expireAjax()) {
            return;
        }

        $result = Mage::helper('resursbank_checkout/ajax')
            ->getActionResultShell();

        try {
            if (!$this->_validateFormKey()) {
                Mage::throwException('Invalid form key');
            }

            // Run parent action.
            parent::saveBillingAction();

            // Extract errors form response body (from parent function).
            $errors = $this->extractErrorsFromParentResponseBody(
                $this->getResponse()->getBody()
            );

            if (count($errors)) {
                $result['message']['error'] = $errors;
            } else {
                // Render and append updated checkout elements to $result.
                $this->renderUpdatedElements($result);

                // Clear redirect and collect messages.
                Mage::helper('resursbank_checkout/ajax')->collectMessages(
                    $result['message']
                );
            }
        } catch (Exception $e) {
            $result['message']['error'][] = $e->getMessage();
        }

        // Correct response headers.
        Mage::helper('resursbank_checkout/ajax')->correctResponseHeaders($this);

        $this->getResponse()->setHeader('Content-type', 'application/json');
        $this->getResponse()->setBody(Zend_Json::encode($result));
    }

    /**
     * Store shipping address on quote object:
     *
     * Expected parameters:
     *
     *  'shipping' => array(
     *      'company'       => '',                  // Optional
     *      'street'        => array(
     *          'First street line', 'Second street line'
     *      ),                                      // Required
     *      'vat_id'        => '',                  // Optional
     *      'city'          => 'New York',          // Required
     *      'region'        => 'New York',          // Optional
     *      'postcode'      => '12312',             // Required
     *      'telephone'     => '123123123',         // Required
     *      'fax'           => '',                  // Optional
     *      'firstname'     => 'Carl',              // Required
     *      'lastname'      => 'Jan',               // Required
     *      'country_id'    => 'se'                 // Required
     *  );
     */
    public function saveShippingAction()
    {
        if ($this->_expireAjax()) {
            return;
        }

        $result = Mage::helper('resursbank_checkout/ajax')
            ->getActionResultShell();

        try {
            if (!$this->_validateFormKey()) {
                Mage::throwException('Invalid form key');
            }

            // Append telephone number to shipping address information. No
            // telephone field exist for shipping address.
            $this->appendTelephoneNumberToShippingAddress();

            // Run parent action.
            parent::saveShippingAction();

            // Extract errors form response body (from parent function).
            $errors = $this->extractErrorsFromParentResponseBody(
                $this->getResponse()->getBody()
            );

            if (count($errors)) {
                $result['message']['error'] = $errors;
            } else {
                // Render and append updated checkout elements to $result.
                $this->renderUpdatedElements($result);

                // Clear redirect and collect messages.
                Mage::helper('resursbank_checkout/ajax')->collectMessages(
                    $result['message']
                );
            }
        } catch (Exception $e) {
            $result['message']['error'][] = $e->getMessage();
        }

        // Correct response headers.
        Mage::helper('resursbank_checkout/ajax')->correctResponseHeaders($this);

        $this->getResponse()->setHeader('Content-type', 'application/json');
        $this->getResponse()->setBody(Zend_Json::encode($result));
    }

    /**
     * Store payment method on quote.
     *
     * Expected parameters:
     *
     *  'payment' => array(
     *      'method' => 'checkmo' // Required
     *  );
     */
    public function savePaymentAction()
    {
        if ($this->_expireAjax()) {
            return;
        }

        $result = Mage::helper('resursbank_checkout/ajax')
            ->getActionResultShell();

        try {
            if (!$this->_validateFormKey()) {
                Mage::throwException('Invalid form key');
            }

            // Run parent action.
            parent::savePaymentAction();

            // Extract errors form response body (from parent function).
            $errors = $this->extractErrorsFromParentResponseBody(
                $this->getResponse()->getBody()
            );

            if (count($errors)) {
                $result['message']['error'] = $errors;
            } else {
                // Render and append updated checkout elements to $result.
                $this->renderUpdatedElements($result);

                // Clear redirect and collect messages.
                Mage::helper('resursbank_checkout/ajax')->collectMessages(
                    $result['message']
                );
            }
        } catch (Exception $e) {
            $result['message']['error'][] = $e->getMessage();
        }

        // Correct response headers.
        Mage::helper('resursbank_checkout/ajax')->correctResponseHeaders($this);

        $this->getResponse()->setHeader('Content-type', 'application/json');
        $this->getResponse()->setBody(Zend_Json::encode($result));
    }

    /**
     * Store shipping method on quote.
     *
     * Expected parameters:
     *
     *  'shipping_method' => 'flatrate_flatrate' // Required
     */
    public function saveShippingMethodAction()
    {
        if ($this->_expireAjax()) {
            return;
        }

        $result = Mage::helper('resursbank_checkout/ajax')
            ->getActionResultShell();

        try {
            if (!$this->_validateFormKey()) {
                Mage::throwException('Invalid form key');
            }

            // Run parent action.
            parent::saveShippingMethodAction();

            // Extract errors form response body (from parent function).
            $errors = $this->extractErrorsFromParentResponseBody(
                $this->getResponse()->getBody()
            );

            if (count($errors)) {
                $result['message']['error'] = $errors;
            } else {
                // Render and append updated checkout elements to $result.
                $this->renderUpdatedElements($result);

                // Clear redirect and collect messages.
                Mage::helper('resursbank_checkout/ajax')->collectMessages(
                    $result['message']
                );
            }
        } catch (Exception $e) {
            $result['message']['error'][] = $e->getMessage();
        }

        // Correct response headers.
        Mage::helper('resursbank_checkout/ajax')->correctResponseHeaders($this);

        $this->getResponse()->setHeader('Content-type', 'application/json');
        $this->getResponse()->setBody(Zend_Json::encode($result));
    }
    
    /**
     * Extract errors from parent function response body (useful to extract
     * errors when applying billing/shipping address).
     *
     * @param string $body
     * @return array
     */
    public function extractErrorsFromParentResponseBody($body)
    {
        $result = array();

        $data = Mage::helper('core')->jsonDecode($body);

        if (is_array($data) && isset($data['error'])) {
            if (is_int($data['error'])) {
                if (isset($data['message'])) {
                    $result = is_array($data['message']) ?
                        $data['message'] :
                        (array) $data['message'];
                }
            } elseif (is_string($data['error'])) {
                $result[] = $data['error'];
            } elseif (is_bool($data['error'])) {
                if ($data['error'] && isset($data['error_messages'])) {
                    $result = is_array($data['error_messages']) ?
                        $data['error_messages'] :
                        (array) $data['error_messages'];
                }
            }
        }

        return $result;
    }

    /**
     * Render all (relevant) blocks that has been changed.
     *
     * @param array $result
     * @return $this
     */
    public function renderUpdatedElements(array &$result)
    {
        if (isset($result['elements'])) {
            $this->loadLayout('resursbank_checkout_index_index');

            $elements = $this->helper->renderCheckoutElements(
                $this->getLayout(),
                true
            );

            if (count($elements)) {
                $result['elements'] = $elements;
            }
        }

        return $this;
    }

    /**
     * Assign default address information to the quote object.
     *
     * @return $this
     */
    public function assignDefaultAddressInformationToQuote()
    {
        if ($this->helper->quoteIsMissingAddress()) {
            $this->helper->quoteAssignDefaultAddress();
        }

        return $this;
    }

    /**
     * Assign default shipping method (this only applies if there is only one
     * shipping method available).
     *
     * @return $this
     * @throws Exception
     */
    public function assignDefaultShippingMethod()
    {
        if ($this->helper->getQuote()) {
            $this->helper->getQuote()
                ->getShippingAddress()
                ->collectShippingRates()
                ->save();

            $methods = $this->helper->getQuote()
                ->getShippingAddress()
                ->getGroupedAllShippingRates();

            if (count($methods) === 1) {
                foreach ($methods as $code => $method) {
                    if (is_array($method) && isset($method[0])) {
                        $this->getOnepage()
                            ->saveShippingMethod($method[0]->getCode());

                        $this->getOnepage()
                            ->getQuote()
                            ->collectTotals()
                            ->save();
                    }
                }
            }
        }

        return $this;
    }

    /**
     * Assign default payment method (this ensures that all shipping methods
     * are accurately displayed for some reason).
     *
     * @return $this
     */
    public function assignDefaultPaymentMethod()
    {
        $this->getOnepage()->savePayment(array(
            'method' => 'resursbank_default'
        ));

        return $this;
    }

    /**
     * Convert a stringified JSON parameter to an array.
     *
     * @param string $parameter
     * @return $this
     */
    public function parseJsonParameter($parameter)
    {
        $parameter = (string) $parameter;
        $parameters = Mage::app()->getRequest()->getParams();

        if (isset($parameters[$parameter])) {
            Mage::app()->getRequest()->setParam(
                $parameter,
                (array) json_decode($parameters[$parameter])
            );

            Mage::app()->getRequest()->setPost(
                $parameter,
                (array) json_decode($parameters[$parameter])
            );
        }

        return $this;
    }

    /**
     * Correct the name of the submitted payment method.
     *
     * @return $this
     */
    public function correctPaymentMethod()
    {
        $param = Mage::app()->getRequest()->getParam('payment');

        if (is_array($param) && isset($param['method'])) {
            /** @var Resursbank_Checkout_Model_Api_Credentials $credentials */
            $credentials = Mage::getModel('resursbank_checkout/api_credentials')
                ->loadFromConfig();

            $method = Resursbank_Checkout_Model_Payment_Method_Resursbank_Default::CODE_PREFIX;

            if (!$credentials->hasCredentials()) {
                $method .= 'default';
            } else {
                $method .= (string) $param['method'] .
                    '_' . $credentials->getMethodSuffix();
            }

            Mage::app()->getRequest()->setParam(
                'payment',
                array('method' => strtolower($method))
            );

            Mage::app()->getRequest()->setPost(
                'payment',
                array('method' => strtolower($method))
            );
        }

        return $this;
    }

    /**
     * Append telephone number to shipping address, copying the one applied for billing address. This is re required
     * since there is no telephone field for the shipping address in the firame form.
     *
     * @return $this
     */
    public function appendTelephoneNumberToShippingAddress()
    {
        $address = Mage::app()->getRequest()->getParam('shipping');

        if (!isset($address['telephone']) &&
            $this->getOnepage()->getQuote() &&
            $this->getOnepage()->getQuote()->getBillingAddress()
        ) {
            $address['telephone'] = $this->getOnepage()
                ->getQuote()
                ->getBillingAddress()
                ->getData('telephone');

            Mage::app()->getRequest()->setParam('shipping', $address);
            Mage::app()->getRequest()->setPost('shipping', $address);
        }

        return $this;
    }

    /**
     * Collect item prices.
     */
    public function collectItemPricesAction()
    {
        if ($this->_expireAjax()) {
            return;
        }

        $result = Mage::helper('resursbank_checkout/ajax')
            ->getActionResultShell();

        try {
            if (!$this->_validateFormKey()) {
                Mage::throwException('Invalid form key');
            }

            // Render and append updated checkout elements to $result.
            $this->renderUpdatedElements($result);

            // Collected item rows.
            $result['items'] = array();

            // Get request parameters.
            $items = $this->helper->getQuote()->getAllItems();

            if (count($items)) {
                /** @var Mage_Checkout_Helper_Data $helper */
                $helper = Mage::helper('checkout');

                /** @var Mage_Sales_Model_Order_Item $item */
                foreach ($items as $item) {
                    $result['items'][] = array(
                        'item_id' => $item->getId(),
                        'item_total' => $helper->formatPrice(
                            $item->getRowTotalInclTax()
                        ),
                        'item_total_excl_tax' => $helper->formatPrice(
                            $item->getRowTotal()
                        ),
                        'item_price' => $helper->formatPrice(
                            $item->getPriceInclTax()
                        ),
                        'item_price_excl_tax' => $helper->formatPrice(
                            $item->getPrice()
                        )
                    );
                }
            }
        } catch (Exception $e) {
            $result['message']['error'][] = $e->getMessage();
        }

        // Correct response headers.
        Mage::helper('resursbank_checkout/ajax')->correctResponseHeaders($this);

        $this->getResponse()->setHeader('Content-type', 'application/json');
        $this->getResponse()->setBody(Zend_Json::encode($result));
    }
}
