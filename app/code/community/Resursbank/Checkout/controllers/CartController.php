<?php
/**
 * Copyright 2016 Resurs Bank AB
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

require_once(
    Mage::getModuleDir('controllers', 'Mage_Checkout') .
    DS .
    'CartController.php'
);

class Resursbank_Checkout_CartController extends Mage_Checkout_CartController
{
    /**
     * @var Resursbank_Checkout_Helper_Data
     */
    protected $helper;

    /**
     * Constructor.
     *
     * Setup commonly used resources.
     */
    protected function _construct()
    {
        parent::_construct();
        
        $this->helper = Mage::helper('resursbank_checkout');
    }

    /**
     * Remove an item from shopping cart.
     */
    public function deleteAction()
    {
        if ($this->expireAjax()) {
            return;
        }

        $result = Mage::helper('resursbank_checkout/ajax')
            ->getActionResultShell();

        try {
            // Run parent method.
            parent::deleteAction();

            // Render and append updated checkout elements to $result.
            $this->renderUpdatedElements($result);

            // Add other information to $result.
            $result['id'] = (int) $this->getRequest()->getParam('id');
            $result['cart_qty'] = Mage::getSingleton('checkout/cart')
                ->getSummaryQty();

            // Clear redirect and collected messages (avoid messages being shown duplicate times when navigating).
            Mage::helper('resursbank_checkout/ajax')->collectMessages(
                $result['message']
            );
        } catch (Exception $e) {
            $result['message']['error'][] = $e->getMessage();
        }

        // Correct response headers.
        Mage::helper('resursbank_checkout/ajax')->correctResponseHeaders($this);

        // Return result.
        $this->getResponse()->setHeader('Content-type', 'application/json');
        $this->getResponse()->setBody(Zend_Json::encode($result));
    }

    /**
     * Update an item in the shopping cart.
     */
    public function updateAction()
    {
        if ($this->expireAjax()) {
            return;
        }

        $result = Mage::helper('resursbank_checkout/ajax')
            ->getActionResultShell();

        $id = $this->getRequest()->getParam('id');

        try {
            /** @var Mage_Sales_Model_Quote_Item $quoteItem */
            $quoteItem = $this->_getCart()->getQuote()->getItemById((int) $id);

            // Validate submitted quantity (we cannot rely on Magento's native
            // functions to do this because an error thrown during validation
            // would be stored within a session to be rendered during the next
            // page load). It will also cause an infinite redirect loop if a
            // validation error is thrown in combination with redirecting the
            // shopping cart page to the checkout page unless we do it this way.
            $this->helper->validateQty($quoteItem, $this->getRequest()
                ->getParam('qty'));

            // Run parent action.
            if (!$this->helper->legacySetup()) {
                parent::ajaxUpdateAction();
            } else {
                $this->legacyAjaxUpdate();
            }

            // Render and append updated checkout elements to $result.
            $this->renderUpdatedElements($result);

            // Add other information to $result.
            $result['item_id'] = $id;

            $result['item_total'] = Mage::helper('checkout')
                ->formatPrice($quoteItem->getRowTotalInclTax());

            $result['item_total_excl_tax'] = Mage::helper('checkout')
                ->formatPrice($quoteItem->getRowTotal());

            $result['item_price'] = Mage::helper('checkout')
                ->formatPrice($quoteItem->getPriceInclTax());

            $result['item_price_excl_tax'] = Mage::helper('checkout')
                ->formatPrice($quoteItem->getPrice());

            $result['cart_qty'] = Mage::getSingleton('checkout/cart')
                ->getSummaryQty();

            // Clear redirect and collect messages.
            Mage::helper('resursbank_checkout/ajax')->collectMessages(
                $result['message']
            );
        } catch (Exception $e) {
            $result['message']['error'][] = $e->getMessage();
        }

        // Correct response headers.
        Mage::helper('resursbank_checkout/ajax')->correctResponseHeaders($this);

        $this->getResponse()->setHeader('Content-type', 'application/json');
        $this->getResponse()->setBody(Zend_Json::encode($result));
    }

    /**
     * Update an item in the shopping cart.
     */
    public function couponAction()
    {
        if ($this->expireAjax()) {
            return;
        }

        $result = Mage::helper('resursbank_checkout/ajax')
            ->getActionResultShell();

        try {
            if (!$this->_validateFormKey()) {
                Mage::throwException('Invalid form key');
            }

            // Run parent action.
            parent::couponPostAction();

            // Render and append updated checkout elements to $result.
            $this->renderUpdatedElements($result);

            // Clear redirect and collect messages.
            Mage::helper('resursbank_checkout/ajax')->collectMessages(
                $result['message']
            );
        } catch (Exception $e) {
            $result['message']['error'][] = $e->getMessage();
        }

        // Correct response headers.
        Mage::helper('resursbank_checkout/ajax')->correctResponseHeaders($this);

        $this->getResponse()->setHeader('Content-type', 'application/json');
        $this->getResponse()->setBody(Zend_Json::encode($result));
    }

    /**
     * Render all (relevant) blocks that has been changed.
     *
     * @param array $result
     * @return $this
     */
    public function renderUpdatedElements(array &$result)
    {
        if (!isset($result['elements']) || !is_array($result['elements'])) {
            $result['elements'] = array();
        }

        $this->loadLayout('resursbank_checkout_index_index');

        $elements = $this->helper->renderCheckoutElements(
            $this->getLayout(),
            true
        );

        if (count($elements)) {
            $result['elements'] = $elements;
        }

        return $this;
    }

    /**
     * Validate ajax request and redirect on failure
     *
     * Copied from app/code/core/Mage/Checkout/controllers/OnepageController.php
     * (CE 1.9.2.4)
     *
     * @return bool
     */
    protected function expireAjax()
    {
        /** @var Mage_Checkout_Model_Type_Onepage $onepage */
        $onepage = $this->helper->getOnepage();

        if (!$onepage->getQuote()->hasItems()
            || $onepage->getQuote()->getHasError()
            || $onepage->getQuote()->getIsMultiShipping()
        ) {
            $this->ajaxRedirectResponse();
            return true;
        }
        $action = strtolower($this->getRequest()->getActionName());
        if (Mage::getSingleton('checkout/session')->getCartWasUpdated(true)
            && !in_array($action, array('index', 'progress'))
        ) {
            $this->ajaxRedirectResponse();
            return true;
        }
        return false;
    }

    /**
     * Send Ajax redirect response
     *
     * Copied from app/code/core/Mage/Checkout/controllers/OnepageController.php
     * (CE 1.9.2.4)
     *
     * @return $this
     */
    protected function ajaxRedirectResponse()
    {
        $this->getResponse()
            ->setHeader('HTTP/1.1', '403 Session Expired')
            ->setHeader('Login-Required', 'true')
            ->sendResponse();

        return $this;
    }

    /**
     * This function was copied from Magento 1.9.2.4 (self :: ajaxUpdateAction).
     *
     * We require this for older Magento versions.
     */
    public function legacyAjaxUpdate()
    {
        if (!$this->_validateFormKey()) {
            Mage::throwException('Invalid form key');
        }
        $id = (int)$this->getRequest()->getParam('id');
        $qty = $this->getRequest()->getParam('qty');
        $result = array();
        if ($id) {
            try {
                $cart = $this->_getCart();
                if (isset($qty)) {
                    $filter = new Zend_Filter_LocalizedToNormalized(
                        array(
                            'locale' => Mage::app()
                                ->getLocale()
                                ->getLocaleCode()
                        )
                    );
                    $qty = $filter->filter($qty);
                }

                $quoteItem = $cart->getQuote()->getItemById($id);
                if (!$quoteItem) {
                    Mage::throwException($this->__('Quote item is not found.'));
                }
                if ($qty == 0) {
                    $cart->removeItem($id);
                } else {
                    $quoteItem->setQty($qty)->save();
                }
                $this->_getCart()->save();

                $this->loadLayout();

                $result['qty'] = $this->_getCart()->getSummaryQty();

                if (!$quoteItem->getHasError()) {
                    $result['message'] = $this->__(
                        'Item was updated successfully.'
                    );
                } else {
                    $result['notice'] = $quoteItem->getMessage();
                }
                $result['success'] = 1;
            } catch (Exception $e) {
                // Debug log.
                $this->helper->debugLog($e);

                $result['success'] = 0;
                $result['error'] = $this->__('Can not save item.');
            }
        }

        $this->getResponse()->setHeader('Content-type', 'application/json');
        $this->getResponse()->setBody(
            Mage::helper('core')->jsonEncode($result)
        );
    }
}
