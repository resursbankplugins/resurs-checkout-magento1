<?php
/**
 * Copyright 2016 Resurs Bank AB
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 * Class Resursbank_Checkout_Adminhtml_Checkout_PartPaymentController
 */
class Resursbank_Checkout_Adminhtml_Checkout_PartPaymentController extends Mage_Adminhtml_Controller_Action
{
    /**
     * @var Resursbank_Checkout_Helper_Data
     */
    protected $helper;

    /**
     * Setup commonly used resources.
     */
    protected function _construct()
    {
        parent::_construct();

        $this->helper = Mage::helper('resursbank_checkout');
    }

    /**
     * Takes a payment method id and returns data about that payment method if
     * it's eligible for part payment. This data contains, for instance, a
     * method's factors, and its min and max limits.
     *
     * NOTE: This method is meant to be used with AJAX requests. It expects a
     * "paymentMethodId" parameter.
     */
    public function fetchAction()
    {
        $result = Mage::helper('resursbank_checkout/ajax')
            ->getActionResultShell();

        try {
            /** @var Resursbank_Checkout_Model_Account_Method $method */
            $method = $this->getMethod();

            if ($method->getId()) {
                $result = array_merge(
                    $result,
                    [
                        'methodExists' => true,
                        'factors' => $this->duration()->toOptionArray(
                            $method->getId()
                        ),
                        'minLimit' => $method->getMinOrderTotal(),
                        'maxLimit' => $method->getMaxOrderTotal()
                    ]
                );
            } else {
                $result = array_merge(
                    $result,
                    [
                        'methodExists' => false,
                        'factors' => $this->duration()->toOptionArray()
                    ]
                );
            }
        } catch (Exception $e) {
            $result['message']['error'][] = $e->getMessage();

            $this->helper->debugLog(
                $e,
                Resursbank_Checkout_Helper_Log::GENERAL
            );
        }

        $this->getResponse()->setHeader('Content-type', 'application/json');
        $this->getResponse()->setBody(Zend_Json::encode($result));
    }

    /**
     * Retrieve instance of part payment duration source model.
     *
     * @return Resursbank_Checkout_Model_System_Config_Source_PartPayment_Duration
     */
    private function duration()
    {
        return Mage::getModel('resursbank_checkout/system_config_source_partPayment_duration');
    }

    /**
     * Retrieve instance of payment method corresponding to supplied id.
     *
     * @return Resursbank_Checkout_Model_Account_Method
     */
    private function getMethod()
    {
        $id = (int) $this->getRequest()->getParam(
            'paymentMethodId',
            0
        );

        /** @var Resursbank_Checkout_Model_Account_Method $method */
        $method = Mage::getModel('resursbank_checkout/account_method');

        if ($id !== 0) {
            $method->load($id);
        }

        return $method;
    }
}
