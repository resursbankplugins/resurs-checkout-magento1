<?php
/**
 * Copyright 2016 Resurs Bank AB
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 * Class Resursbank_Checkout_Adminhtml_Checkout_CallbackController
 */
class Resursbank_Checkout_Adminhtml_Checkout_CallbackController extends Mage_Adminhtml_Controller_Action
{
    /**
     * Register callback URLs.
     */
    public function registerAction()
    {
        try {
            Mage::helper('resursbank_checkout/callback')
                ->registerApiCallbacks();

            Mage::getSingleton('adminhtml/session')->addSuccess(
                'Callback URLs were successfully registered.'
            );
        } catch (Exception $e) {
            // Create log entry with the actual Exception.
            Mage::helper('resursbank_checkout/log')->entry($e);

            // Display generic error message.
            Mage::getSingleton('adminhtml/session')->addError(
                'Failed to register URLs. Please check the log for more info.'
            );
        }

        // Redirect back to config section.
        $this->_redirect(
            'adminhtml/system_config/edit/section/resursbank_checkout',
            Mage::helper('resursbank_checkout')->getBackendUrlParams()
        );
    }

    /**
     * Callback test trigging settings.
     *
     * @param $key
     * @param $value
     */
    private function setTriggerData($key, $value)
    {
        Mage::getConfig()->saveConfig(
            'resursbank_checkout/api/callback_test_' . $key,
            $value
        );
    }

    /**
     * Initialize remote callback trigger.
     *
     * @throws Exception
     */
    public function triggerTestAction()
    {
        $ecom = Mage::helper('resursbank_checkout/ecom');
        $this->setTriggerData('end', 0);
        $triggerResponse = false;
        
        try {
            $triggerResponse = $ecom->getConnection()->triggerCallback();
        } catch (\Exception $triggerException) {
            // Do nothing.
        }
        
        if ((bool)$triggerResponse) {
            $this->setTriggerData('start', time());
            $this->setTriggerData('active', true);
        } else {
            $this->setTriggerData('start', 0);
            $this->setTriggerData('active', false);
        }

        header('Content-Type: application/json; charset=UTF-8;');
        echo json_encode(array(
            'triggerResponse' => $triggerResponse
        ));
        die;
    }

    /**
     * Check if admin is allowed to perform this action.
     *
     * @return boolean
     */
    protected function _isAllowed()
    {
        return Mage::getSingleton('admin/session')
            ->isAllowed('system/config/resursbank_checkout');
    }
}
