<?php
/**
 * Copyright 2016 Resurs Bank AB
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

use Resursbank\RBEcomPHP\RESURS_PAYMENT_STATUS_RETURNCODES;

/**
 * Route incoming callbacks.
 *
 * Class Resursbank_Checkout_CallbackController
 */
class Resursbank_Checkout_CallbackController extends Mage_Core_Controller_Front_Action
{
    /**
     * Payment (order) id.
     *
     * @var string
     */
    protected $paymentId;

    /**
     * Secret digest, to verify the source of incoming calls.
     *
     * @var string
     */
    protected $digest;

    /**
     * Checks if the inbound parameters for an url belongs to a real payment-based callback or a test.
     *
     * @return bool
     */
    public function canSkipPaymentControl()
    {
        $return = false;

        $paramTest = (string)Mage::app()->getRequest()->getParam('param1');
        $paramPoll = (string)Mage::app()->getRequest()->getParam('fetch');

        if (!empty($paramTest) || !empty($paramPoll)) {
            $return = true;
        }

        return $return;
    }

    /**
     * Apply order status based on incoming callback.
     *
     * @return Mage_Core_Controller_Front_Action
     * @throws Exception
     */
    public function preDispatch()
    {
        $this->paymentId = (string)Mage::app()->getRequest()->getParam('paymentId');
        $this->digest = (string)Mage::app()->getRequest()->getParam('digest');

        // Skip orderstatus checking when tests are running.
        if ($this->canSkipPaymentControl()) {
            return parent::preDispatch();
        }

        $status = $this->getOrderStatus();

        switch (true) {
            case $status & (RESURS_PAYMENT_STATUS_RETURNCODES::PAYMENT_PROCESSING):
                Mage::getModel('resursbank_checkout/callback')->setOrderStatus(
                    $this->paymentId,
                    'confirmed'
                );
                break;
            case $status & (RESURS_PAYMENT_STATUS_RETURNCODES::PAYMENT_COMPLETED):
                Mage::getModel('resursbank_checkout/callback')->setOrderStatus(
                    $this->paymentId,
                    'completed'
                );
                break;
            case $status & (RESURS_PAYMENT_STATUS_RETURNCODES::PAYMENT_ANNULLED):
                Mage::getModel('resursbank_checkout/callback')->setOrderStatus(
                    $this->paymentId,
                    'canceled'
                );
                break;
            case $status & (RESURS_PAYMENT_STATUS_RETURNCODES::PAYMENT_CREDITED):
                Mage::getModel('resursbank_checkout/callback')->setOrderStatus(
                    $this->paymentId,
                    'canceled'
                );
                break;
            default:
                // Do nothing to unknown statuses
                break;
        }

        return parent::preDispatch();
    }

    /**
     * Payment is unfrozen, which means it can be captured.
     */
    public function unfreezeAction()
    {
        try {
            Mage::getModel('resursbank_checkout/callback')->unfreeze(
                $this->paymentId,
                $this->digest
            );
        } catch (Exception $e) {
            $this->log($e);
        }
    }

    /**
     * Payment has been booked by Resursbank. This means the payment has been
     * unfrozen and is preparing to be finalized.
     */
    public function bookedAction()
    {
        try {
            Mage::getModel('resursbank_checkout/callback')->booked(
                $this->paymentId,
                $this->digest
            );
        } catch (Exception $e) {
            $this->log($e);
        }
    }

    /**
     * Payment has been finalized by Resursbank. This means the client has been
     * debited by Resursbank.
     */
    public function finalizationAction()
    {
        try {
            Mage::getModel('resursbank_checkout/callback')->finalization(
                $this->paymentId,
                $this->digest
            );
        } catch (Exception $e) {
            $this->log($e);
        }
    }

    /**
     * Payment passed/failed automatic fraud screening from Resursbank.
     */
    public function automaticFraudControlAction()
    {
        try {
            Mage::getModel('resursbank_checkout/callback')
                ->automaticFraudControl(
                    $this->paymentId,
                    $this->digest,
                    $this->getFraudControlResult()
                );
        } catch (Exception $e) {
            $this->log($e);
        }
    }

    /**
     * Payment has been fully annulled by Resursbank. This can for example occur
     * if a fraud screening fails.
     */
    public function annulmentAction()
    {
        try {
            Mage::getModel('resursbank_checkout/callback')->annulment(
                $this->paymentId,
                $this->digest
            );
        } catch (Exception $e) {
            $this->log($e);
        }
    }

    /**
     * Payment has been updated at Resursbank.
     */
    public function updateAction()
    {
        try {
            Mage::getModel('resursbank_checkout/callback')->update(
                $this->paymentId,
                $this->digest
            );
        } catch (Exception $e) {
            $this->log($e);
        }
    }

    /**
     * Receiving something from Resurs Bank ends the task.
     */
    public function testAction()
    {
        // Act here, on received callback.
        $this->setTriggerData('end', time());
        $this->setTriggerData('last_success', time());
        $this->setTriggerData('active', false);
    }

    /**
     * Respond to the status of test callback trigger.
     */
    public function pollAction()
    {
        $return = array(
            'start' => $this->getTriggerData('start'),
            'end' => $this->getTriggerData('end'),
            'last_success' => $this->getTriggerData('last_success'),
            'active' => $this->getTriggerData('active'),
        );
        
        header('Content-Type: application/json; charset=UTF-8;');
        echo json_encode($return);
        die;
    }

    /**
     * Quick fetch configuration based on test callback triggers.
     *
     * @param string $key
     * @return bool
     */
    public function getTriggerData($key)
    {
        return Mage::getStoreConfig(
            "resursbank_checkout/api/callback_test_{$key}"
        );
    }

    /**
     * Callback test trigging settings.
     *
     * @param string $key
     * @param mixed $value
     */
    private function setTriggerData($key, $value)
    {
        Mage::getConfig()->saveConfig(
            'resursbank_checkout/api/callback_test_' . $key,
            $value
        );
    }

    /**
     * Retrieve order status based on current payment information.
     *
     * @return mixed
     * @throws Exception
     */
    public function getOrderStatus()
    {
        /** @var Resursbank_Checkout_Helper_Ecom $ecom */
        $ecom = Mage::helper('resursbank_checkout/ecom');

        return $ecom->getConnection()->getOrderStatusByPayment($this->paymentId);
    }

    /**
     * Retrieve relevant event data (used when retrieving order status based on
     * payment info).
     *
     * @return string
     */
    protected function getEventData()
    {
        $result = '';

        if ($this->getRequest()->getActionName() === 'automaticFraudControl') {
            $result = (string)$this->getRequest()->getParam('result');
        }

        return $result;
    }

    /**
     * Retrieve received result from automatic fraud control.
     *
     * @return string
     */
    public function getFraudControlResult()
    {
        return (string)Mage::app()->getRequest()->getParam('result');
    }

    /**
     * Add log entry.
     *
     * @param string|Exception $message
     * @return $this
     */
    public function log($message)
    {
        Mage::helper('resursbank_checkout/callback')
            ->debugLog($message, Resursbank_Checkout_Helper_Log::CALLBACK);

        return $this;
    }
}
