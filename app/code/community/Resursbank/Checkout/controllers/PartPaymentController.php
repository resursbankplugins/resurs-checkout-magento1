<?php
/**
 * Copyright 2016 Resurs Bank AB
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 * Class Resursbank_Checkout_PartPaymentController
 */
class Resursbank_Checkout_PartPaymentController extends Mage_Core_Controller_Front_Action
{
    /**
     * @var Resursbank_Checkout_Helper_Data
     */
    protected $helper;

    /**
     * @var Resursbank_Checkout_Helper_Ecom
     */
    protected $ecom;

    /**
     * Setup commonly used resources.
     */
    protected function _construct()
    {
        parent::_construct();

        $this->helper = Mage::helper('resursbank_checkout');
        $this->ecom = Mage::helper('resursbank_checkout/ecom');
    }

    /**
     * Returns cost of purchase HTML for the modal used on product pages.
     */
    public function getCostOfPurchaseAction()
    {
        $result = Mage::helper('resursbank_checkout/ajax')
            ->getActionResultShell();

        try {
            if (!$this->_validateFormKey()) {
                Mage::throwException('Invalid form key');
            }

            /** @var Resursbank_Checkout_Model_Account_Method $method */
            $method = $this->getPaymentMethod();
            $methodId = $method->getRawData('id');

            if ($method->isActive()) {
                $result['html'] = $this->ecom
                    ->getConnection()
                    ->getCostOfPurchase(
                        $method->getRawData('id'),
                        $this->getPrice()
                    );
            }
        } catch (Exception $e) {
            $result['message']['error'][] = $e->getMessage();

            $this->helper->debugLog(
                $e,
                Resursbank_Checkout_Helper_Log::API
            );
        }

        $this->getResponse()->setHeader('Content-type', 'application/json');
        $this->getResponse()->setBody(Zend_Json::encode($result));
    }

    /**
     * Retrieve price for cost of purchase HTML.
     *
     * @return float
     */
    private function getPrice()
    {
        // Optional parameter. Price for cost of purchase HTML.
        $price = (float) $this->getRequest()->getParam('price', '');

        // Optional parameter. Use quote total instead of supplied "price".
        $useCheckoutPrice = json_decode(
            $this->getRequest()->getParam('useCheckoutPrice', false)
        );

        if ($useCheckoutPrice) {
            $price = (float) Mage::getSingleton('checkout/session')
                ->getQuote()
                ->getGrandTotal();
        }

        return $price;
    }

    /**
     * Retrieve instance of payment method corresponding to supplied code.
     *
     * @return Resursbank_Checkout_Model_Account_Method
     */
    private function getPaymentMethod()
    {
        $paymentMethodCode = (string) $this->getRequest()->getParam(
            'paymentMethodCode',
            ''
        );

        /** @var Resursbank_Checkout_Model_Account_Method $method */
        $method = Mage::getModel('resursbank_checkout/account_method')
            ->getCollection()
            ->addFieldToFilter('code', $paymentMethodCode)
            ->getLastItem();

        if (!$method instanceof Resursbank_Checkout_Model_Account_Method) {
            $method = Mage::getModel('resursbank_checkout/account_method');
        }

        return $method;
    }
}
