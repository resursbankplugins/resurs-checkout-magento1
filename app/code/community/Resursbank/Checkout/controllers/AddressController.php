<?php
/**
 * Copyright 2016 Resurs Bank AB
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 * Handle address related actions.
 *
 * Class Resursbank_Checkout_AddressController
 */
class Resursbank_Checkout_AddressController extends Mage_Core_Controller_Front_Action
{
    /**
     * Fetch address information using SSN.
     */
    public function fetchAction()
    {
        /** @var Resursbank_Checkout_Helper_Data $helper */
        $helper = Mage::helper('resursbank_checkout');

        /** @var Resursbank_Checkout_Helper_Ajax $ajax */
        $ajax = Mage::helper('resursbank_checkout/ajax');

        // Retrieve basic response structure.
        $result = $ajax->getActionResultShell();

        /** @var Resursbank_Checkout_Model_Api_Adapter_Simplified $adapter */
        $adapter = Mage::getModel('resursbank_checkout/api_adapter_simplified');

        try {
            // Validate form key.
            if (!$this->_validateFormKey()) {
                Mage::throwException('Invalid form key');
            }

            // Retrieve request data.
            $ssn = (string) $this->getRequest()->getParam('ssn');
            $country = (string) $this->getRequest()->getParam('country');

            // Store SSN in session for later communication with API during
            // payment creation.
            $adapter->setSsn($ssn);

            if ($ssn !== '') {
                /** @var Resursbank_Checkout_Model_Api_Adapter_Simplified_Address $address */
                $address = $adapter->getAddress(
                    $ssn,
                    $country,
                    Mage::helper('resursbank_checkout/simplified')
                        ->getCustomerTypeFromSession()
                );

                // Add address data to result.
                $result['address'] = json_encode($address->getData());
            }
        } catch (Exception $e) {
            $result['message']['error'] =
                $helper->__(
                    'Something went wrong while fetching your address. ' .
                    'Please fill out the form manually to continue.'
                );

            $helper->debugLog(
                $e,
                Resursbank_Checkout_Helper_Log::API
            );
        }

        // Correct response headers.
        $ajax->correctResponseHeaders($this);

        // Respond.
        $this->getResponse()->setHeader('Content-type', 'application/json');
        $this->getResponse()->setBody(Zend_Json::encode($result));
    }

    /**
     * Handles our customer type data in the session.
     */
    public function setIsCompanyAction()
    {
        /** @var Resursbank_Checkout_Helper_Data $helper */
        $helper = Mage::helper('resursbank_checkout');

        /** @var Resursbank_Checkout_Helper_Ajax $ajax */
        $ajax = Mage::helper('resursbank_checkout/ajax');

        // Retrieve basic response structure.
        $result = $ajax->getActionResultShell();

        try {
            /** @var Resursbank_Checkout_Model_Api_Adapter_Simplified $adapter */
            $adapter = Mage::getModel('resursbank_checkout/api_adapter_simplified');
            $type = (string) $this->getRequest()->getParam('type');

            if ($type === Resursbank_Checkout_Model_Api::CUSTOMER_TYPE_COMPANY) {
                $adapter->setIsCompany(true);
                $adapter->unsetSsn();
            } else if ($type === Resursbank_Checkout_Model_Api::CUSTOMER_TYPE_PRIVATE) {
                $adapter->unsetIsCompany();
            }
        } catch (Exception $e) {
            $result['message']['error'] =
                $helper->__(
                    'Something went wrong while updating the customer type ' .
                    'for the SSN field. Please fill out the form manually to ' .
                    'continue.'
                );

            $helper->debugLog(
                $e,
                Resursbank_Checkout_Helper_Log::API
            );
        }

        // Correct response headers.
        $ajax->correctResponseHeaders($this);

        // Respond.
        $this->getResponse()->setHeader('Content-type', 'application/json');
        $this->getResponse()->setBody(Zend_Json::encode($result));
    }
}
