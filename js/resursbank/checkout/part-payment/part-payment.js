/**
 * Copyright 2016 Resurs Bank AB
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/*  ===================================================
    PartPayment
    =================================================== */
(function ($, Classy) {
    Classy.define({
        name: 'PartPayment',

        init: function PartPayment (me) {
            /**
             * Takes a price and converts in to a part payment price.
             * 
             * @returns {number} The price converted to a part payment price.
             */
            me.toPrice = function (price, duration) {
                return Math.round((price / duration) * 100) / 100;
            };

            /**
             * Takes a string/number and updates the part payment price text.
             * 
             * @param {string|number} price
             * @returns {object} jQuery object.
             */
            me.setPrice = function (price) {
                return me.getPriceEl().text(formatCurrency(price, optionsPrice.priceFormat));
            };

            /**
             * Returns the part payment price element of the product page.
             * 
             * @returns {object} jQuery object.
             */
            me.getPriceEl = function () {
                return $('.resursbank-part-payment-price');
            };

            /**
             * Updates the part payment price. It takes an optional string that says whether the 
             * price should be with or without tax.
             * 
             * @param {string} taxType - Either "excl" for excluding tax, or "incl" for including 
             * tax. Defaults to "incl".
             * 
             * @returns {object} Part payment price element wrapped in a jQuery object.
             */
            me.updatePrice = function (price, duration) {
                return me.setPrice(me.toPrice(price, duration));
            };

            /**
             * Get part payment information for a product or quote total.
             *
             * @param {object} ajaxQueue
             * @param {string} baseUrl
             * @param {string} formKey
             * @param {string} paymentMethodCode
             * @param {number} duration
             * @param {boolean} [useCheckoutPrice] - Optional. If true, the
             * controller will use the quote total.
             * @param {number} [price] - Optional. Price to use to generate
             * the part payment information. Overridden by useCheckoutPrice.
             * @return {Promise} Will return a JQuery.Deferred() promise object
             * that, when resolved, will have the updated part payment HTML as
             * its argument. When rejected, it will have an array of error
             * messages as argument.
             */
            me.getCostOfPurchase = function (
                ajaxQueue,
                baseUrl,
                formKey,
                paymentMethodCode,
                duration,
                useCheckoutPrice,
                price
            ) {
                var deferred = $.Deferred();

                ajaxQueue.queue({
                    chain: 'resursbank-checkout',
                    url: baseUrl + 'partPayment/getCostOfPurchase',
                    parameters: {
                        form_key: formKey,
                        paymentMethodCode: paymentMethodCode,
                        useCheckoutPrice: useCheckoutPrice,
                        price: price
                    },

                    onSuccess: function (response) {
                        var data = response.responseJSON;

                        if (data.message.error.length) {
                            deferred.reject(data.message.error);
                        } else {
                            deferred.resolve(data.html);
                        }
                    },

                    onFailure: function (response) {
                        var data = response.responseJSON;

                        deferred.reject(data);
                    }
                }).run('resursbank-checkout');

                return deferred.promise();
            }
        }
    });
}(
    jQuery, 
    RESURSBANK.Classy
));