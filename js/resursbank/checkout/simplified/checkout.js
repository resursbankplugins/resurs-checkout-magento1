/**
 * Copyright 2016 Resurs Bank AB
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

if (typeof RESURSBANK === 'undefined') {
    var RESURSBANK = {};
}

RESURSBANK.Checkout = (function (me) {
    /**
     * Whether or not [Checkout] has been initialized.
     *
     * @type {boolean}
     */
    var initialized = false;

    /**
     * Base URL.
     *
     * @type {string}
     */
    var baseUrl = '';

    /**
     * Key to pass on as a parameter with AJAX calls.
     *
     * @type {string}
     */
    var formKey = '';

    /**
     * The country code that's been chosen in the config.
     *
     * @type {string}
     */
    var country = '';

    /**
     * An [AjaxQ] instance.
     *
     * @type {AjaxQ}
     */
    me.AjaxQ = null;

    /**
     * An [Address] instance.
     *
     * @type {Address}
     */
    me.address = null;

    /**
     * An initializer.
     *
     * @param {Object} config
     * @param {String} config.baseUrl
     * @param {String} config.formKey
     * @return {me}
     */
    me.init = function (config) {
        if (!initialized) {
            // Initialize properties.
            formKey = config.formKey;
            baseUrl = config.baseUrl;
            country = typeof config.country !== 'string' ? 'SE' : config.country;

            // Setup AJAX queue.
            me.AjaxQ = Classy.create('AjaxQ');
            me.AjaxQ.createChain('resursbank-checkout');

            me.ssn = Classy.create('SimplifiedSsn');
            me.ssn.init(config);

            // Mark as initialized.
            initialized = true;
        }

        return me;
    };

    /**
     * Returns the URL to fetch address information.
     *
     * @returns {String}
     */
    me.getBaseUrl = function () {
        return baseUrl;
    };

    /**
     * Returns the form key that is used with AJAX requests.
     *
     * @returns {String}
     */
    me.getFormKey = function () {
        return formKey;
    };

    /**
     * Returns the country that is chosen in the config.
     *
     * @returns {String}s
     */
    me.getCountry = function () {
        return country;
    };

    return me;
}(Classy.create(null)));
