/**
 * Copyright 2016 Resurs Bank AB
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 * Remember which layout we're running (for FC col1)
 */
Classy.define({
    name: 'SimplifiedSsn',

    init: function SimplifiedSsn(me) {
        /**
         * The whole SSN-field element.
         *
         * @readonly
         * @type {HTMLDivElement}
         */
        var element;

        /**
         * @readonly
         * @type {HTMLInputElement}
         */
        var ssnInput;

        /**
         * @readonly
         * @type {HTMLInputElement[]}
         */
        var customerTypeOptions;

        /**
         * <label> element for the SSN input field.
         *
         * @readonly
         * @type {HTMLLabelElement}
         */
        var fieldLabel;

        /**
         * Label name for the input field when customer is a regular user.
         *
         * @readonly
         * @type {string}
         */
        var naturalLabel;

        /**
         * Label name for the input field when customer is a company.
         *
         * @readonly
         * @type {string}
         */
        var legalLabel;

        /**
         * The fetch button for the SSN field.
         *
         * @readonly
         * @type {HTMLButtonElement}
         */
        var fetchButton;

        /**
         * @type {string}
         */
        var chosenCustomerType;

        /**
         * Whether or not the input is disabled.
         *
         * @type {boolean}
         */
        var disabled = false;

        /**
         * Whether or not we are in the middle of fetching an address.
         *
         * @type {boolean}
         */
        var fetchingAddress;

        /**
         * The chosen country in Resurs Bank's configuration API settings.
         *
         * @type {string}
         */
        var configuredCountry;


        var simplifiedAddress;


        var previousSsn;


        var addressFetched;


        var usingFireCheckout;


        var fireCheckoutLayout;

        /**
         * Returns the value of the SSN input element.
         *
         * @returns {string}
         */
        me.getSsn = function () {
            return ssnInput.value;
        };

        /**
         * Replaces the ajax loader with the input element.
         *
         * @returns {SimplifiedSsn}
         */
        me.showLoader = function () {
            var loaderEl = $('resursbank-checkout-ajax-loader');

            if (loaderEl) {
                loaderEl.show();
            }

            return me;
        };

        /**
         * Replaces the ajax loader with the input element.
         *
         * @returns {SimplifiedSsn}
         */
        me.hideLoader = function () {
            var loaderEl = $('resursbank-checkout-ajax-loader');

            if (loaderEl) {
                loaderEl.hide();
            }

            return me;
        };

        /**
         * Validates a ssn or organisation number. Returns true by default,
         * because some countries don't require a ssn or organisation number.
         *
         * @param {string} value
         * @param {string} country
         * @param {string} customerType
         * @returns {boolean}
         */
        me.validate = function (value, country, customerType) {
            var result = true;

            if (value !== '') {
                if (country === 'SE' || country === 'NO' || country === 'FI') {
                    if (customerType === 'LEGAL') {
                        result = me.validateOrg(value, country);
                    } else if (customerType === 'NATURAL') {
                        result = me.validateSsn(value, country);
                    }
                }
            }

            return result;
        };

        /**
         * Validates an SSN.
         *
         * NOTE: Validates for Sweden, Norway and Finland ONLY.
         *
         * @param {string} ssn
         * @param {string} country
         * @returns {boolean}
         */
        me.validateSsn = function (ssn, country) {
            var result = false;
            var norway = /^([0][1-9]|[1-2][0-9]|3[0-1])(0[1-9]|1[0-2])(\d{2})(\-)?([\d]{5})$/;
            var finland = /^([\d]{6})[\+-A]([\d]{3})([0123456789ABCDEFHJKLMNPRSTUVWXY])$/;
            var sweden = /^(18\d{2}|19\d{2}|20\d{2}|\d{2})(0[1-9]|1[0-2])([0][1-9]|[1-2][0-9]|3[0-1])(\-|\+)?([\d]{4})$/;

            if (country === 'SE') {
                result = sweden.test(ssn);
            } else if (country === 'NO') {
                result = norway.test(ssn);
            } else if (country === 'FI') {
                result = finland.test(ssn);
            }

            return result;
        };

        /**
         * Validates an organisation number.
         *
         * NOTE: Validates for Sweden, Norway and Finland ONLY.
         *
         * @param {string} org
         * @param {string} country
         * @returns {boolean}
         */
        me.validateOrg = function (org, country) {
            var result = false;
            var finland = /^((\d{7})(\-)?\d)$/;
            var sweden = /^(16\d{2}|18\d{2}|19\d{2}|20\d{2}|\d{2})(\d{2})(\d{2})(\-|\+)?([\d]{4})$/;
            var norway = /^([89]([ |-]?[0-9]){8})$/;

            if (country === 'SE') {
                result = sweden.test(org);
            } else if (country === 'NO') {
                result = norway.test(org);
            } else if (country === 'FI') {
                result = finland.test(org);
            }

            return result;
        };

        /**
         * Takes the customer type (private, company) and makes a request to
         * the server, sending in that information.
         *
         * @param {string} type
         */
        me.pushSsnType = function (type) {
            if (typeof type === 'string') {
                me.disable();
                simplifiedAddress.toggleFields(false);

                RESURSBANK.Checkout.AjaxQ.queue({
                    chain: 'resursbank-checkout',
                    url: RESURSBANK.Checkout.getBaseUrl() + 'address/setIsCompany',
                    parameters: {
                        type: type,
                        form_key: RESURSBANK.Checkout.getFormKey()
                    },

                    onSuccess: function (response) {
                        var data = response.responseJSON;

                        if (data.hasOwnProperty('message') &&
                            data.message.hasOwnProperty('error') &&
                            typeof data.message.error === 'string'
                        ) {
                            window.alert(
                                'Resurs Bank Checkout\n\n' + data.message.error
                            );
                        }
                    },

                    onFailure: function (response) {
                        window.alert(
                            'Resurs Bank Checkout\n\n' +
                            'Could not contact the server. Please fill out ' +
                            'form manually.'
                        );
                    },

                    onComplete: function () {
                        simplifiedAddress.toggleFields(true);
                        me.enable();

                        if (chosenCustomerType === 'NATURAL') {
                            simplifiedAddress.toggleCompany(false);
                        }

                        if (doesFireCheckoutExist()) {
                            updateFireCheckoutPaymentMethods();
                        }
                    }
                }).run('resursbank-checkout');
            }
        };

        /**
         * Fetch address information.
         *
         * @returns {SimplifiedSsn}
         */
        me.fetch = function (ssn, country) {
            if ((Validation.validate(ssnInput) || ssn === '') &&
                previousSsn !== ssn &&
                fetchingAddress === false
            ) {
                fetchingAddress = true;

                me.disable();
                me.showLoader();
                simplifiedAddress.toggleFields(false);

                RESURSBANK.Checkout.AjaxQ.queue({
                    chain: 'resursbank-checkout',
                    url: RESURSBANK.Checkout.$call('getBaseUrl') + 'address/fetch',
                    parameters: {
                        ssn: ssn,
                        country: country,
                        form_key: RESURSBANK.Checkout.$call('getFormKey')
                    },

                    onSuccess: function (response) {
                        var data = response.responseJSON;

                        if (data.message.error.length) {
                            window.alert(
                                'Resurs Bank Checkout\n\n' + data.message.error
                            );
                        } else if (!data.hasOwnProperty('address')) {
                            if (country === 'SE') {
                                simplifiedAddress.removeAddress();
                                me.enable();
                                changeRemoveToFetch();
                            } else if (addressFetched) {
                                simplifiedAddress.removeAddress();
                            }

                            addressFetched = false;
                        } else {
                            if (country === 'SE') {
                                simplifiedAddress.applyAddress(JSON.parse(data.address));
                                changeFetchToRemove();
                            }

                            addressFetched = true;
                        }

                        previousSsn = ssn;
                    },

                    onFailure: function (response) {
                        window.alert(
                            'Resurs Bank Checkout\n\n' +
                            'Could not contact the server. Please fill out ' +
                            'the form manually.'
                        );
                    },

                    onComplete: function (response) {
                        if (country !== 'SE') {
                            me.enable();
                        }

                        me.hideLoader();
                        simplifiedAddress.toggleFields(true);

                        fetchingAddress = false;

                        if (doesFireCheckoutExist()) {
                            updateFireCheckoutPaymentMethods();
                        }
                    }
                }).run('resursbank-checkout');
            }

            return me;
        };

        /**
         * Disables the SSN input element and stops observing it of changes.
         *
         * @returns {SimplifiedSsn}
         */
        me.disable = function () {
            if (!disabled) {
                disabled = true;
                ssnInput.disabled = true;
                customerTypeOptions.forEach(function (radio) {
                    radio.disabled = true;
                });
            }

            return me;
        };

        /**
         * Enables the SSN input element and observes it of changes.
         *
         * @returns {SimplifiedSsn}
         */
        me.enable = function () {
            if (disabled) {
                disabled = false;
                ssnInput.disabled = false;
                customerTypeOptions.forEach(function (radio) {
                    radio.disabled = false;
                });
            }

            return me;
        };

        /**
         * Changes the remove button to a fetch button.
         */
        function changeRemoveToFetch() {
            fetchButton.textContent = Translator.translate('Fetch address');
            fetchButton.stopObserving('click');
            fetchButton.observe('click', onFetch);
        }

        /**
         * Changes the fetch button to a remove button.
         */
        function changeFetchToRemove() {
            fetchButton.textContent = Translator.translate('Not you?');
            fetchButton.stopObserving('click');
            fetchButton.observe('click', onNotYou);
        }

        /**
         * Updates the SSN-field label based on the customer type.
         *
         * @param {string} customerType
         */
        function updateFieldLabel (customerType) {
            if (customerType === 'LEGAL') {
                fieldLabel.textContent = legalLabel;
            } else if (customerType === 'NATURAL') {
                fieldLabel.textContent = naturalLabel;
            }
        }


        function onFetch() {
            me.fetch(me.getSsn(), configuredCountry);
        }

        /**
         * Event handler when the fetch button should remove the fetched
         * address.
         */
        function onNotYou() {
            me.fetch('', configuredCountry);
        }


        /**
         * Checks if FireCheckout module is installed and is activated.
         *
         * @returns {boolean}
         */
        function doesFireCheckoutExist() {
            return typeof window.FireCheckout !== 'undefined' &&
                typeof window.checkout !== 'undefined' &&
                window.checkout instanceof FireCheckout;
        }

        /**
         * Uses FireCheckout to update the list of payment methods.
         */
        function updateFireCheckoutPaymentMethods() {
            var url = window.checkout.urls.billing_address;
            var sections = window.FireCheckout.Ajax
                .getSectionsToUpdate('billing', 'shipping');

            window.checkout.update(
                url,
                FireCheckout.Ajax.arrayToJson(sections)
            );
        }


        function initAddress(config) {
            var fields;

            simplifiedAddress.init();

            fields = simplifiedAddress.getFields();

            if (typeof fields.country !== 'undefined') {
                fields.country.value = config.country;

                me.pushSsnType(chosenCustomerType);
            }
        }

        me.init = function(config) {
            naturalLabel = config.naturalLabel;
            legalLabel = config.legalLabel;
            configuredCountry = config.country;
            chosenCustomerType = '';
            disabled = false;
            fetchingAddress = false;
            addressFetched = false;
            simplifiedAddress = Classy.create('SimplifiedAddress');
            usingFireCheckout = config.usingFireCheckout;
            fireCheckoutLayout = config.fireCheckoutLayout;

            element = $('resursbank-checkout-ssn-input-field');
            fieldLabel = $('resursbank-checkout-ssn-label');
            ssnInput = $('resursbank-checkout-ssn');
            customerTypeOptions = $$('input[name="RBC-ssn-type"]');
            fetchButton = $('RBC-ssn-fetch-button');

            customerTypeOptions.forEach(function (radio) {
                if (radio.type === 'radio') {
                    radio.observe('change', function () {
                        var value = radio.value;

                        me.pushSsnType(value);
                        chosenCustomerType = value;

                        if (ssnInput.value !== '') {
                            Validation.validate(ssnInput);
                        }

                        updateFieldLabel(value);
                    });

                    // Initializing customerType.
                    if (chosenCustomerType === '' && radio.checked) {
                        chosenCustomerType = radio.value;
                    }
                }
            });

            if (usingFireCheckout === true) {
                if (fireCheckoutLayout === 'col1-set') {
                    // We need the FireCheckout to be completely loaded before
                    // we can do anything with the address fields.
                    document.observe('firecheckout:setResponseAfter', function () {
                        if (simplifiedAddress.getInitialized() === false) {
                            initAddress(config);
                        }
                    });
                } else {
                    // If FireCheckout is used, but with another layout.
                    initAddress(config);

                }
            } else {
                // If normal checkout page is used.
                initAddress(config);
            }

            if (configuredCountry === 'SE') {
                fetchButton.observe('click', onFetch);

                simplifiedAddress.getForm().insert({
                    before: $('resursbank-checkout-ssn-input-field')
                });
            }

            // SSN validation.
            Validation.add(
                'validate-RBC-simplified-ssn',
                Translator.translate('Wrong SSN/Org. number.'),
                function (value) {
                    var result = me.validate(
                        value,
                        configuredCountry,
                        chosenCustomerType
                    );

                    return Validation.get('isEmpty').test(value) && result;
                }
            );

            me.hideLoader();
        };
    }
});
