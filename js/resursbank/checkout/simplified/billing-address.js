/**
 * Copyright 2016 Resurs Bank AB
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

Classy.define({
    name: 'SimplifiedAddress',

    /**
     *
     * @param {object} me - Replacement for "this" provided by Classy.
     */
    init: function SimplifiedAddress(me) {
        /**
         * @readonly
         * @type {HTMLFormElement}
         */
        var element;


        var fields;


        var initialized = false;


        /**
         * Returns an object with all the billing form elements.
         *
         * @returns {object | undefined}
         */
        me.getFields = function () {
            var obj;

            if (element instanceof HTMLFormElement) {
                obj = {
                    firstName: element.select('[name="billing[firstname]"]')[0],
                    lastName: element.select('[name="billing[lastname]"]')[0],
                    middleName: element.select('[name="billing[middlename]"]')[0],
                    company: element.select('[name="billing[company]"]')[0],
                    email: element.select('[name="billing[email]"]')[0],
                    address: element.select('[name="billing[street][]"]')[0],
                    address2: element.select('[name="billing[street][]"]')[1],
                    city: element.select('[name="billing[city]"]')[0],
                    stateSelect: element.select('[name="billing[region_id]"]')[0],
                    stateInput: element.select('[name="billing[region]"]')[0],
                    postcode: element.select('[name="billing[postcode]"]')[0],
                    country: element.select('[name="billing[country_id]"]')[0],
                    telephone: element.select('[name="billing[telephone]"]')[0],
                    fax: element.select('[name="billing[fax]"]')[0]
                };
            }

            return obj;
        };

        /**
         * Retrieve billing form element. We first try to obtain the element as
         * it would appear in FireCheckout. If that doesn't exist we fallback to
         * the default element.
         *
         * @return {HTMLFormElement | undefined}
         */
        me.getForm = function () {
            var element = $('firecheckout-form');

            if (!element) {
                element = $('co-billing-form');
            }

            return element;
        };


        me.applyAddress = function (addressObj) {
            if (addressObj.hasOwnProperty('first_name')) {
                fields.firstName.value = addressObj.first_name;
            }

            if (addressObj.hasOwnProperty('last_name')) {
                fields.lastName.value = addressObj.last_name;
            }

            if (addressObj.hasOwnProperty('middle_name')) {
                fields.middleName.value = addressObj.middle_name;
            }

            if (addressObj.hasOwnProperty('city')) {
                fields.city.value = addressObj.city;
            }

            if (addressObj.hasOwnProperty('company')) {
                fields.company.value = addressObj.company;
            }

            if (addressObj.hasOwnProperty('postcode')) {
                fields.postcode.value = addressObj.postcode;
            }

            if (addressObj.hasOwnProperty('street')) {
                if (typeof addressObj.street[0] === 'string') {
                    fields.address.value = addressObj.street[0];
                }

                if (typeof addressObj.street[1] === 'string') {
                    fields.address2.value = addressObj.street[1];
                }
            }
        };

        /**
         * Removes an applied address.
         *
         * @returns {object}
         */
        me.removeAddress = function () {
            var addressEls = getAddressEls();

            Object.keys(addressEls).forEach(function (key) {
                var field = addressEls[key];

                if (field instanceof HTMLInputElement) {
                    if (field.type === 'text') {
                        field.value = '';
                    }
                } else if (field instanceof HTMLSelectElement) {
                    field.selectedIndex = 0;
                }
            });

            return me;
        };

        /**
         * Returns an object with the form elements that should be used when
         * applying/removing a fetched address.
         *
         * @returns {object}
         */
        var getAddressEls = function () {
            return {
                firstName: fields.firstName,
                lastName: fields.lastName,
                middleName: fields.middleName,
                city: fields.city,
                postcode: fields.postcode,
                address: fields.address,
                address2: fields.address2

            };
        };


        me.getInitialized = function () {
            return initialized;
        };


        me.getCountry = function () {
            return (typeof fields !== 'undefined' && typeof fields.country !== 'undefined' ?  fields.country.value : '');
        };

        me.toggleCompany = function (state) {
            fields.company.disabled = !state;

            return me;
        };

        /**
         * Toggles the disabled property of the form elements on the simplified
         * checkout page.
         *
         * @param {boolean} state
         */
        me.toggleFields = function (state) {
            if (typeof state === 'boolean') {
                Object.keys(fields).forEach(function (key) {
                    if (typeof fields[key] !== 'undefined') {
                        fields[key].disabled = !state;
                    }
                });
            }
        };

        me.init = function() {
            if (initialized === false) {
                element = me.getForm();
                fields = me.getFields();
                initialized = true;
            }

            return me;
        }
    }
});
