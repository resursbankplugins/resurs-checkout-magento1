/**
 * Copyright 2016 Resurs Bank AB
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

// Interval based timer
var resursCallbackTestPoll;

window.onload = function () {
    // Initialize test callback triggers when the proper element is present
    if (null !== document.getElementById('testCallbackTriggerStatus')) {
        // Ready to poll
        resursPollRun(true);
    }
}

/**
 * Update status of triggered callback (under the TEST url)
 *
 * @param message
 * @param textColor
 */
function resursUpdateTestTriggerStatus(message, textColor) {
    if (null !== document.getElementById('testCallbackTriggerStatus')) {
        document.getElementById('testCallbackTriggerStatus').innerHTML = '<span style="color:' + textColor + ';font-weight:bold;">' + message + '</span>';
    }
}

/**
 * Shut down interval based trigger
 */
function resursUnPoll() {
    if (resursCallbackTestPoll !== '') {
        clearInterval(resursCallbackTestPoll);
    }
}

/**
 * Trigger test from the admin config button instead of running through the page reloader
 */
function resursTestTrigger() {
    new Ajax.Request(
        resursTriggerBackground, {
            method: 'get',
            onSuccess: function(transport) {
                var response = transport.responseText || '';
                try {
                    var result = JSON.parse(response);
                    if (resursCallbackTestPoll !== '' && result['triggerResponse']) {
                        resursPollRun(true);
                    }
                } catch (e) {
                    resursUpdateTestTriggerStatus(e, '#990000');
                }
            }
        }
    );
}

/**
 * Initialize or repeat request when testing callback reachability
 *
 * @param pollify
 */
function resursPollRun(pollify) {
    new Ajax.Request(resursPollTestUrl, {
        method: 'get',
        onCreate: function (request) {
            Ajax.Responders.unregister(varienLoaderHandler.handler);
        },
        onSuccess: function (transport) {
            Ajax.Responders.register(varienLoaderHandler.handler);
            var response = transport.responseText || '';
            try {
                var result = JSON.parse(response);
                if (typeof result === 'object') {
                    var cbStart = result['start'];
                    var cbEnd = result['end'];
                    var lastSuccess = result['last_success'];

                    if (cbStart > 0) {
                        cbWaitTime = (new Date().getTime() / 1000) - cbStart;
                        var lastSuccessDate = new Date(lastSuccess * 1000);
                        if (cbEnd > 0) {
                            resursUpdateTestTriggerStatus('Test callback received ' + lastSuccessDate, '#009900');
                        } else if (cbWaitTime > 15) {
                            // Stop on long wait
                            resursUpdateTestTriggerStatus('Triggered callback test waited too long (' + cbWaitTime + ' seconds). The callback urls are either misconfigured or unreachable.', '#990000');
                            resursUnPoll();
                        }
                    } else {
                        resursUpdateTestTriggerStatus('To test if callbacks are reachable, click the test button below.', '#000099');
                    }

                    if (result['active']) {
                        if (pollify) {
                            resursUpdateTestTriggerStatus('Waiting for test callback...', '#000099');
                            // Start once and wait
                            resursCallbackTestPoll = setInterval('resursPollRun(false)', 1000);
                        }
                    } else {
                        resursUnPoll();
                    }
                }
            } catch (e) {
                resursUnPoll();
            }
        },
        onFailure: function () {
            resursUnPoll();
        }
    });
}
