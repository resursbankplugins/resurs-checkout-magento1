/**
 * Copyright 2016 Resurs Bank AB
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

RESURSBANK.PartPayment = (function (me) {
    /**
     * States whether initialization has happened.
     *
     * @type {boolean}
     * @private
     */
    var _initialized = false;

    /**
     * Base URL for AJAX calls.
     *
     * @type {string}
     * @private
     */
    var _baseUrl = '';

    /**
     * Default min and max limits for the payment methods.
     *
     * @type {{minLimit: number, maxLimit: number}}
     * @private
     */
    var _defaultLimits = {
        minLimit: 0,
        maxLimit: 0
    };

    /**
     * The chosen payment method cache. We store the min and max limit here.
     *
     * @type {null|object}
     * @private
     */
    var _paymentMethodCache = null;

    /**
     * The element where you choose part payment duration for the selected
     * payment method.
     *
     * @type {HTMLSelectElement|null}
     * @private
     */
    var _durationBox = null;

    /**
     * The element where you choose payment method.
     *
     * @type {HTMLSelectElement|null}
     * @private
     */
    var _paymentMethodBox = null;

    /**
     * The element where you specify the minimum price of the products that
     * part payment should apply for.
     *
     * @type {HTMLElement|null}
     * @private
     */
    var _minPriceField = null;

    /**
     * The element where you specify the maximum price of the products that
     * part payment should apply for.
     *
     * @type {HTMLElement|null}
     * @private
     */
    var _maxPriceField = null;

    /**
     * Checks whether a value is an object.
     *
     * @param {*} value
     * @returns {boolean}
     */
    function isObject(value) {
        return Object.prototype.toString.call(value) === '[object Object]';
    }

    /**
     * Sets the payment method cache.
     *
     * @private
     * @param {number} minLimit
     * @param {number} maxLimit
     */
    function setPaymentMethodCache(minLimit, maxLimit) {
        minLimit = parseFloat(minLimit);
        maxLimit = parseFloat(maxLimit);

        _paymentMethodCache = {
            minLimit: isNaN(minLimit) ? _defaultLimits.minLimit : minLimit,
            maxLimit: isNaN(maxLimit) ? _defaultLimits.maxLimit : maxLimit
        };
    }

    /**
     * Sets the default min and max limit.
     *
     * @private
     * @param {number} minLimit
     * @param {number} maxLimit
     */
    function setDefaultLimits(minLimit, maxLimit) {
        var defaultMinLimit = parseFloat(minLimit);
        var defaultMaxLimit = parseFloat(maxLimit);

        _defaultLimits.minLimit = isNaN(defaultMinLimit) ? 0 : defaultMinLimit;
        _defaultLimits.maxLimit = isNaN(defaultMaxLimit) ? 0 : defaultMaxLimit;
    }

    /**
     * Event handler for when the payment method changes.
     *
     * @private
     * @return
     */
    function onPaymentMethodChange() {
        _durationBox.disabled = true;
        _paymentMethodBox.disabled = true;

        return me.fetchDurationOptions(
            me.getPaymentMethodId(),
            function (response) {
                try {
                    var data = response.responseJSON;

                    updateDurationOptions(
                        data.factors.map(function (factor) {
                            return new Option(factor.label, factor.value);
                        })
                    );

                    if (data.methodExists) {
                        setPaymentMethodCache(
                            data.minLimit,
                            data.maxLimit
                        );
                        setMinPrice(data.minLimit);
                        setMaxPrice(data.maxLimit);
                    } else {
                        setPaymentMethodCache(0, 0);
                        setMinPrice(null);
                        setMaxPrice(null);
                    }
                } catch (error) {
                    console.error(
                        'An error occured when updating the duration options!\n\n',
                        error
                    );
                }
            },
            function () {
                _durationBox.disabled = false;
                _paymentMethodBox.disabled = false;
            }
        );
    }

    /**
     * Takes an array of option elements and replaces the current list of
     * duration options.
     *
     * @private
     * @param {Option[]} factors
     */
    function updateDurationOptions(factors) {
        var currentLen = _durationBox.options.length;
        var factorsLen = factors.length;
        var i;

        for (i = currentLen; i >= 0; i -= 1) {
            _durationBox.remove(i);
        }

        for (i = 0; i < factorsLen; i += 1) {
            _durationBox.add(factors[i]);
        }
    }

    /**
     * Updates the value of the minimum price field.
     *
     * @private
     * @param {number|null} price
     */
    var setMinPrice = function (price) {
        var value = parseFloat(price);

        if (price === null) {
            value = '';
        } else if (_paymentMethodCache !== null) {
            if (isNaN(value) || price < _paymentMethodCache.minLimit) {
                value = _paymentMethodCache.minLimit;
            } else if (price > _paymentMethodCache.maxLimit) {
                value = _paymentMethodCache.maxLimit
            }
        }

        _minPriceField.nodeValue = value;
    };

    /**
     * Updates the value of the maximum price field.
     *
     * @private
     * @param {number|null} price
     */
    var setMaxPrice = function (price) {
        var value = parseFloat(price);

        if (price === null || isNaN(value)) {
            value = '';
        }

        _maxPriceField.nodeValue = value;
    };


    /**
     * Returns the current value of the payment method box.
     *
     * @return {string}
     */
    me.getPaymentMethodId = function () {
        return _paymentMethodBox.value;
    };

    /**
     * Takes a payment method id and calls the PartPayment controller. The
     * controller will return a list of duration options for the given method,
     * and then we will replace our current list of duration options with the
     * new one.
     *
     * @param {string} id - A payment method id.
     * @param {Function} onSuccess
     * @param {Function} onComplete
     * @return {jQuery} The ajax request.
     */
    me.fetchDurationOptions = function (id, onSuccess, onComplete) {
        return new Ajax.Request(_baseUrl, {
            parameters: {
                paymentMethodId: id
            },

            onSuccess: onSuccess,
            onComplete: onComplete,

            onFailure: function (response) {
                console.error(
                    'An error occured serverside when fetching duration options.\n\n',
                    response
                );
            }
        });
    };

    /**
     * Initialization function. Can only be executed once.
     *
     * @param {object} config
     * @param {string} config.baseUrl
     * @param {object|null} config.chosenMethodMinMaxLimit
     * @param {number} config.chosenMethodMinMaxLimit.minLimit
     * @param {number} config.chosenMethodMinMaxLimit.maxLimit
     * @param {object|undefined} config.defaultLimits
     * @param {number} config.defaultLimits.minLimit
     * @param {number} config.defaultLimits.maxLimit
     *
     */
    me.init = function (config) {
        if (_initialized === false) {
            _durationBox = $('resursbank_checkout_part_payment_duration');

            _paymentMethodBox = $(
                'resursbank_checkout_part_payment_payment_method'
            );

            _minPriceField = $$(
                '#row_resursbank_checkout_part_payment_min_price td.value'
            )[0];

            _maxPriceField = $$(
                '#row_resursbank_checkout_part_payment_max_price td.value'
            )[0];

            if (_durationBox !== null &&
                _paymentMethodBox !== null &&
                _minPriceField !== null &&
                _maxPriceField !== null
            ) {
                $(_paymentMethodBox).observe('change', onPaymentMethodChange);

                if (config.hasOwnProperty('defaultLimits') &&
                    isObject(config.defaultLimits)
                ) {
                    setDefaultLimits(
                        config.defaultLimits.minLimit,
                        config.defaultLimits.maxLimit
                    );
                }

                if (config.hasOwnProperty('chosenMethodMinMaxLimit')
                    && isObject(config.chosenMethodMinMaxLimit)
                ) {
                    setPaymentMethodCache(
                        config.chosenMethodMinMaxLimit.minLimit,
                        config.chosenMethodMinMaxLimit.maxLimit
                    );
                }

                if (_paymentMethodBox.value === '') {
                    setMinPrice(null);
                    setMaxPrice(null);
                }
            }

            _baseUrl = typeof config.baseUrl === 'string' ? config.baseUrl : '';
            _initialized = true;
        }

        return me;
    };

    return me;
}(Classy.create(null)));
