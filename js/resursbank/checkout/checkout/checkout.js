/**
 * Copyright 2016 Resurs Bank AB
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

if (typeof RESURSBANK === 'undefined') {
    var RESURSBANK = {};
}

RESURSBANK.Checkout = (function (me) {
    /**
     * Whether or not [Checkout] has been initialized.
     * 
     * @type {Boolean}
     */
    var initialized = false;

    /**
     * Whether or not [finalizeIframeSetup()] has been called.
     *
     * @type {boolean}
     */
    var finalizedIframe = false;

    /**
     * The type of taxes that are displayed on the checkout page.
     * 
     * @type {Object}
     */
    var displayedTax = {};

    /**
     * The iframe element.
     *
     * @type {Element}
     */
    var iframe = null;

    /**
     * The URL to the iframe.
     *
     * @type {String}
     */
    var iframeUrl = '';

    /**
     * If this module is run on a old version of Magento.
     *
     * @type {String}
     */
    var legacySetup = '';

    /**
     * Key to pass on as a parameter with AJAX calls.
     *
     * @type {String}
     */
    var formKey = '';

    /**
     * The base URL.
     *
     * @type {String}
     */
    var baseUrl = '';

    /**
     * Failure URL.
     *
     * @type {String}
     */
    var failureUrl = '';

    /**
     * Failure URL.
     *
     * @type {String}
     */
    var successUrl = '';

    /**
     * A collection of [RESURSBANK.Checkout.Item] objects.
     * 
     * @type {Object}
     */
    var itemCollection = {};

    /**
     * Handles user address changes.
     * 
     * @type {UserAddress}
     */
    var userAddress = null;

    /**
     * A screen overlay for loading purposes.
     * 
     * @type {AjaxOverlay}
     */
    var ajaxOverlay = null;

    /**
     * Handles payment method changes.
     * 
     * @type {PaymentMethod}
     */
    var paymentMethod = null;

    /**
     * Shipping methods.
     *
     * @type {ShippingMethods}
     */
    var shippingMethods = null;

    /**
     * Minicart.
     *
     * @type {Minicart}
     */
    var minicart = null;

    /**
     * Handles the order booking.
     *
     * @type {BookOrder}
     */
    var bookOrder = null;

    /**
     * The discount element.
     *
     * @type {Discount}
     */
    var discount = null;

    /**
     * An ErrorHandler.
     * 
     * @type {ErrorHandler}
     */
    var errorHandler = null;

    /**
     * Receives and delegates messages from the Resursbank_Checkout iframe when it uses window.postMessage().
     *
     * @param {Object} event
     */
    var postMessageDelegator = function (event) {
        var data;
        var origin = event.origin || event.originalEvent.origin;

        if (origin !== iframeUrl ||
            typeof event.data !== 'string' ||
            event.data === '[iFrameResizerChild]Ready') {
                return;
        }

        var jsonTest = {};

        try {
            jsonTest = JSON.parse(event.data);
        } catch (e) {

        }

        data = jsonTest;

        if (data.hasOwnProperty('eventType') && typeof data.eventType === 'string') {
            switch (data.eventType) {
                case 'checkout:user-info-change': userAddressChange(data); break;
                case 'checkout:payment-method-change': paymentMethodChange(data); break;
                case 'checkout:puchase-button-clicked': onBookOrder(data); break;
                case 'checkout:loaded': finalizeIframeSetup(); break;
                case 'checkout:purchase-failed': purchaseFailed(); break;
                default:;
            }
        }
    };

    /**
     * Posts a message to the parent window with postMessage(). The data argument will be sent to the parent window and
     * have an eventType property which is set when this function is called.
     *
     * @param data {Object} Information to be passed to the iframe.
     */
    var postMessage = function (data) {
        var iframeWindow;

        if (iframe && typeof iframeUrl === 'string' && iframeUrl !== '') {
            iframeWindow = iframe.contentWindow || iframe.contentDocument;
            iframeWindow.postMessage(JSON.stringify(data), iframeUrl);
        }
    };

    /**
     * When the iframe is done loading, this method will do some final work in order to make the checkout page and
     * the iframe communicate and work together as intended.
     *
     * NOTE: Should only be called once when the iframe has fully loaded. That is, when it has done all of its AJAX
     * requests and other preparations.
     */
    var finalizeIframeSetup = function () {
        if (!finalizedIframe) {
            // Post the booking rule to Resursbank_Checkout. This post basically says that when a user presses the button
            // to finalize the order, check with the server if the order is ready to be booked.
            postMessage({
                eventType: 'checkout:set-purchase-button-interceptor',
                checkOrderBeforeBooking: true
            });

            finalizedIframe = true;
        }
    };

    /**
     * When the purchase fails (for example because signing doesn't work) we redirect to the order failure page.
     */
    var purchaseFailed = function () {
        window.location = failureUrl;
    };

    /**
     * Takes an object with the new address information and pushes it to the server.
     * 
     * @param {Object} data - The new address information.
     */
    var userAddressChange = function (data) {
        me.AjaxQ.getChain('resursbank-checkout').onDone(function () {}, true);

        userAddress.push(data);
        updateItemPrices();
    };

    /**
     * Takes an object with the new payment method information and pushes it to the server.
     * 
     * @param {Object} data - The payment method information.
     */
    var paymentMethodChange = function (data) {
        me.AjaxQ.getChain('resursbank-checkout').onDone(function () {}, true);

        paymentMethod.$call('push', paymentMethod.$call('prepareData', data));
    };

    /**
     * Callback for when the iframe's purchase button has been clicked.
     *
     * @param {Object} data - Data from the iframe.
     */
    var onBookOrder = function (data) {
        me.AjaxQ.getChain('resursbank-checkout').onDone(function () {}, true);

        bookOrder.$call('push');
    };

    /**
     * Takes an object of elements and updates the
     *
     * @param {Object} elements
     */
    var updateElements = function (elements) {
        if (Object.prototype.toString.call(elements) === '[object Object]') {
            if (elements.hasOwnProperty('resursbank-checkout-shipping-methods-list')) {
                shippingMethods.$call('updateHtml', elements['resursbank-checkout-shipping-methods-list']);
            }

            if (elements.hasOwnProperty('current-coupon-code')) {
                discount.$call('updateHtml', elements['current-coupon-code']);
            }

            if (elements.hasOwnProperty('header-cart')) {
                minicart.$call('updateHtml', elements['header-cart']);
            }
        }
    };

    /**
     * Updates prices for all items.
     */
    var updateItemPrices = function () {
        me.AjaxQ.queue({
            url: baseUrl + 'index/collectItemPrices',
            chain: 'resursbank-checkout',
            method: 'GET',

            parameters: {
                form_key: formKey
            },

            onSuccess: function (response) {
                var data = response.responseJSON;
                var items = data.items || [];

                items.each(function (obj) {
                    var id = obj.item_id;

                    if (itemCollection.hasOwnProperty(id)) {
                        itemCollection[id].$call('updatePrices', {
                            subtotal: obj.item_total,
                            subtotalExcl: obj.item_total_excl_tax,
                            price: obj.item_price,
                            priceExcl: obj.item_price_excl_tax
                        });
                    }
                });

                updateElements(data.elements);
            }
        }).run('resursbank-checkout');
    };

    /**
     * Event handler for when the item has been removed.
     *
     * @param {Item} item - The instance removed.
     * @param {Object} data - Data from the server.
     */
    var onItemRemoved = function (item, data) {
        updateElements(data.elements);
        me.removeItem(item);
    };

    /**
     * Event handler for when the item's quantity updates.
     *
     * @param {Object} prices
     * @param {Object} elements - Object of elements to update.
     * @param {Number} cartQty - The updated cart quantity.
     */
    var onItemQuantityUpdate = function (prices, elements, cartQty) {
        updateElements(elements);
        minicart.$call('updateCount', cartQty);
    };

    /**
     * Event handler for when a discount code has been pushed to the server.
     *
     * @param {Object} data
     */
    var onDiscountPushed = function (data) {
        updateElements(data.elements);
    };

    var onBookOrderPushError = function (data, iframeMessage) {
        postMessage(iframeMessage);
        errorHandler.feedLine('An error occured when booking your order:')
            .feedLines(data.message.error)
            .alert();
    };

    /**
     * Event handler for when an error occurs when pushing the payment method.
     * 
     * @param {Object} data
     */
    var onPaymentMethodPushError = function (data) {
        errorHandler.feedLine('An error occured when processing the chosen payment method:')
            .feedLines(data.message.error)
            .alert();
    };

    /**
     * Event handler for when the user address ahs been pushed to the server.
     * 
     * @param {Object} data 
     */
    var onUserAddressPushed = function (data) {
        updateElements(data.elements);
    };

    /**
     * Event handler for when the user address ahs been pushed to the server.
     *
     * @param {Object} data
     */
    var onUserAddressError = function (data) {
        updateElements(data.elements);
        errorHandler.feedLine('An error occurred while updating your address:')
            .feedLines(data.message.error)
            .alert();
    };

    /**
     * Event handler for when the user address ahs been pushed to the server.
     * 
     * @param {Object} data 
     */
    var onUserAddressPushed = function (data) {
        updateElements(data.elements);
    };

    /**
     * Redirects the user to the order success page.
     *
     * @returns {boolean}
     */
    var redirectToOrderSuccess = function () {
        window.location.replace(successUrl);
        return false;
    };

    /**
     * An [AjaxQ] instance.
     * 
     * @type {AjaxQ}
     */
    me.AjaxQ = null;

    /**
     * An initializer.
     *
     * @param {Object} config
     * @param {String} config.baseUrl
     * @param {String} config.failureUrl
     * @param {String} config.successUrl
     * @param {String} config.iframeUrl
     * @param {Boolean} config.legacySetup - Whether or not this is an old version of Magento.
     * @param {String} config.formKey
     * @param {Object} config.displayedTax - Which prices that are displayed.
     * @param {Number} config.displayedTax.excl - Price with only excluding tax. Should be 1.
     * @param {Number} config.displayedTax.incl - Price with only including tax. Should be 2.
     * @param {Number} config.displayedTax.both - Price with both including and excluding tax. Should be 3.
     * @param {Number} config.displayedTax.type - Which of "both", "excl" or "incl" that is displayed. Should be one of
     * their respective numbers.
     * @return {me}
     */
    me.init = function (config) {
        if (!initialized) {
            // ================ Initialize properties. ================
            baseUrl = config.baseUrl;
            failureUrl = config.failureUrl;
            successUrl = config.successUrl;
            iframeUrl = config.iframeUrl;
            legacySetup = config.legacySetup;
            formKey = config.formKey;
            displayedTax = config.displayedTax;
            iframe = $$('#resursbank-checkout-container > iframe')[0];

            me.AjaxQ = Classy.create('AjaxQ');
            me.AjaxQ.createChain('resursbank-checkout');

            bookOrder = Classy.create('BookOrder');
            bookOrder.$on('pushed', postMessage);
            bookOrder.$on('push-failed', postMessage);
            bookOrder.$on('push-error', onBookOrderPushError);
            bookOrder.$on('grand-total-zero', redirectToOrderSuccess);

            userAddress = Classy.create('UserAddress');
            userAddress.$on('pushed', onUserAddressPushed);
            userAddress.$on('error', onUserAddressError);

            paymentMethod = Classy.create('PaymentMethod');
            paymentMethod.$on('push-error', onPaymentMethodPushError);

            shippingMethods = Classy.create('ShippingMethods', {
                el: $('resursbank-checkout-shipping-methods-list')
            });

            minicart = Classy.create('Minicart', {
                el: $('header-cart')
            });

            discount = Classy.create('Discount', {
                el: $('discount-code-block')
            });
            discount.$on('pushed', onDiscountPushed);

            errorHandler = Classy.create('ErrorHandler');

            // ================ Initialize [Item] objects. ================
            me.getQuantityInputElements()
                .each(function (input) {
                    var itemId = me.getIdFromQuantityInput(input);
                    var item = Classy.create('Item', {
                        id: itemId,
                        el: me.getItemFromQuantityInput(input)
                    });

                    item.$on('removed', onItemRemoved);
                    item.$on('quantity-updated', onItemQuantityUpdate);

                    itemCollection[itemId] = item;
                });

            // ================ End of initialization. ================
            // Add message listener.
            window.addEventListener('message', postMessageDelegator, false);


            // On reload, apply previously applied discount code, if there is any.
            if (discount.getCode() !== '') {
                discount.pushCode();
            }

            initialized = true;
        }

        return me;
    };

    /**
     * Returns an empty array or an array of the quantity inputs for every product in the cart.
     *
     * @returns {Array}
     */
    me.getQuantityInputElements = function () {
        var qtyInputs = [];
        var regex = /cart\[\d+\]\[qty\]/g;
        var cartTable = $('shopping-cart-table');

        if (cartTable !== null) {
            qtyInputs = cartTable
                .select('input[name*="qty"]')
                .map(function (input) {
                    return input.name.match(regex) !== null ? input : null;
                });
        }

        return qtyInputs;
    };

    /**
     * Returns the id from a quantity input of a product.
     *
     * @param {Element} input
     * @returns {String}
     */
    me.getIdFromQuantityInput = function (input) {
        return input.name.match(/cart\[(\d+)\]/)[1];
    };

    /**
     * Returns the id from a remove button of a product.
     *
     * @param {Element} button
     * @returns {String}
     */
    me.getIdFromRemoveButton = function (button) {
        return button.href.match(/checkout\/cart\/delete\/id\/(\d+)/)[1];
    };

    /**
     * Get the product row element of a quantity input.
     *
     * @param {Element} input
     * @returns {Element}
     */
    me.getItemFromQuantityInput = function (input) {
        return input.up('tr') || null;
    };

    /**
     * Removes an item instance from the [itemCollection].
     * 
     * @param  {Item} item
     * @return {me}
     */
    me.removeItem = function (item) {
        var id = Number(item.id);

        if (typeof id === 'number') {
            item = itemCollection[id];
            item.$ignore('removed', onItemRemoved);
            item.$ignore('quantity-updated', onItemQuantityUpdate);
            item.$call('destroy');

            delete itemCollection[id];
        }

        return me;
    };

    /**
     * Returns the base URL.
     *
     * @returns {String}
     */
    me.getBaseUrl = function () {
        return baseUrl;
    };

    /**
     * Returns the form key that is used with AJAX requests.
     *
     * @returns {String}
     */
    me.getFormKey = function () {
        return formKey;
    };

    /**
     * Returns the object with displayed tax rules.
     *
     * @returns {Object}
     */
    me.getDisplayedTax = function () {
        return displayedTax;
    };

    me.getLegacySetupStatus = function () {
        return legacySetup;
    };

    return me;
}(Classy.create(null)));
