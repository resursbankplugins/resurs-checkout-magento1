/**
 * Copyright 2016 Resurs Bank AB
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

Classy.define({
    name: 'UserAddress',

    init: function UserAddress (me) {
        me.previousBilling = null;
        me.previousShipping = null;

        /**
         * Checks the data of the billing information, to make sure that each required field has a value.
         *
         * @param {object} info
         * @returns {boolean}
         */
        var validateBillingInfo = function (info) {
            return Object.prototype.toString.call(info) === '[object Object]'
                && typeof info.city === 'string' && info.city !== ''
                && typeof info.email === 'string' && info.email !== ''
                && typeof info.firstname === 'string' && info.firstname !== ''
                && typeof info.lastname === 'string' && info.lastname !== ''
                && typeof info.postcode === 'string' && info.postcode !== ''
                && typeof info.telephone === 'string' && info.telephone !== ''
                && Array.isArray(info.street) && typeof info.street[0] === 'string' && info.street[0] !== ''
        };

        /**
         * Checks the data of the shipping information, to make sure that each required field has a value.
         *
         * @param {object} info
         * @returns {boolean}
         */
        var validateShippingInfo = function (info) {
            return Object.prototype.toString.call(info) === '[object Object]'
                && typeof info.city === 'string' && info.city !== ''
                && typeof info.firstname === 'string' && info.firstname !== ''
                && typeof info.lastname === 'string' && info.lastname !== ''
                && typeof info.postcode === 'string' && info.postcode !== ''
                && Array.isArray(info.street) && typeof info.street[0] === 'string' && info.street[0] !== ''
        };

        /**
         * Sends the billing information to the server with an AJAX call.
         *
         * @param data {Object}
         * @return {me}
         */
        me.pushBilling = function (data) {
            var billingAddress = data.billing;
            var useForShipping = billingAddress.use_for_shipping === 1;
            var jsonBillingAddress = JSON.stringify(data.billing);

            data.form_key = RESURSBANK.Checkout.$call('getFormKey');
            data.billing = jsonBillingAddress;

            if (validateBillingInfo(billingAddress)
                && (me.previousBilling !== data.billing
                    || (useForShipping && me.billing !== me.previousShipping)
                )
            ) {
                RESURSBANK.Checkout.AjaxQ.queue({
                    chain: 'resursbank-checkout',
                    url: RESURSBANK.Checkout.$call('getBaseUrl') + 'index/saveBilling',
                    parameters: data,

                    onSuccess: function (response) {
                        var data = response.responseJSON;

                        me.previousBilling = jsonBillingAddress;

                        if (useForShipping) {
                            me.previousShipping = jsonBillingAddress;
                        }

                        if (data.message.error.length) {
                            me.$emit('error', data);
                        } else {
                            me.$emit('pushed', data);
                        }
                    },

                    onFailure: function (response) {
                        var data = response.responseJSON;
                    }
                }).run('resursbank-checkout');
            }

            return me;
        };

        /**
         * Sends the shipping information to the server with an AJAX call.
         *
         * @param data {Object}
         * @return {me}
         */
        me.pushShipping = function (data) {
            var shippingAddress = data.shipping;
            var jsonShippingAddress = JSON.stringify(data.shipping);

            data.form_key = RESURSBANK.Checkout.$call('getFormKey');
            data.shipping = jsonShippingAddress;

            if (validateShippingInfo(shippingAddress) && me.previousShipping !== data.shipping) {
                RESURSBANK.Checkout.AjaxQ.queue({
                    chain: 'resursbank-checkout',
                    url: RESURSBANK.Checkout.$call('getBaseUrl') + 'index/saveShipping',

                    parameters: data,

                    onSuccess: function (response) {
                        var data = response.responseJSON;

                        me.previousShipping = jsonShippingAddress;

                        if (data.message.error.length) {
                            me.$emit('error', data);
                        } else {
                            me.$emit('pushed', data);
                        }
                    },

                    onFailure: function (response) {
                        var data = response.responseJSON;
                    }
                })
                    .run('resursbank-checkout');
            }

            return me;
        };

        /**
         * Check if a shipping address has been set.
         *
         * @param {Object} data
         * @returns {Boolean}
         */
        me.useBillingForShipping = function (data) {
            return !data.hasOwnProperty('delivery');
        };

        /**
         * Receives and handles the changed user address data.
         * 
         * @param  {Object} address 
         * @return {me}
         */
        me.push = function (address) {
            if (me.useBillingForShipping(address)) {
                me.pushBilling(me.prepareBillingInfo(address));
            } else {
                me.pushShipping(me.prepareShippingInfo(address));
            }

            return me;
        };

        /**
         * Prepares the users shipping information to be sent to the server. Can alter names and add properties
         * that the server expects to get.
         *
         * @param {Object} data
         * @returns {Object} The corrected user information, ready to be sent to the server.
         */
        me.prepareShippingInfo = function (data) {
            var info = {
                shipping: {
                    street: []
                }
            };

            $H(data.delivery).each(function (pair) {
                switch (pair.key) {
                    case 'address': info.shipping.street[0] = pair.value; break;
                    case 'addressExtra': info.shipping.street[1] = pair.value; break;
                    default: info.shipping[me.getCorrectInfoName(pair.key)] = pair.value;
                }
            });

            return info;
        };

        /**
         * Prepares the users billing information to be sent to the server. Can alter names and add properties
         * that the server expects to get.
         *
         * @param {Object} data
         * @returns {Object} The corrected user information, ready to be sent to the server.
         */
        me.prepareBillingInfo = function (data) {
            var info = {
                billing: {
                    street: [],
                    use_for_shipping: me.useBillingForShipping(data) ? 1 : 0
                }
            };

            $H(data.address).each(function (pair) {
                switch (pair.key) {
                    case 'address': info.billing.street[0] = pair.value; break;
                    case 'addressExtra': info.billing.street[1] = pair.value; break;
                    default: info.billing[me.getCorrectInfoName(pair.key)] = pair.value;
                }
            });

            return info;
        };

        /**
         * Takes a name of a property of user information and returns the corrected version of that property name.
         *
         * @param {String} name The name of the property to correct.
         * @returns {String}
         */
        me.getCorrectInfoName = function (name) {
            var correctedName = '';

            switch (name) {
                case 'surname': correctedName = 'lastname'; break;
                case 'postal': correctedName = 'postcode'; break;
                case 'countryCode': correctedName = 'country_id'; break;
                default: correctedName = name;
            }

            return correctedName;
        };
    }
});
