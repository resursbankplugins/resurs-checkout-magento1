/**
 * Copyright 2016 Resurs Bank AB
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

Classy.define({
    name: 'AjaxQChain',

    init: function AjaxQChain (config, me) {
        var init = function (config) {
            me.$apply(config)
        };

        /**
         * Loops the [me.onDoneCallbacks] array and fires each callback.
         */
        var fireOnDoneCallbacks = function () {
            while (me.onDoneCallbacks.length) {
                me.onDoneCallbacks.shift()();
            }
        };

        /**
         * Takes a call object and returns a new [onComplete] callback for the call. This new callback then makes sure
         * to call each call in the queue.
         * 
         * @param {Object} callObj - A call object.
         */
        var onCallComplete = function (callObj) {
            var onComplete = callObj.onComplete;

            return function () {
                try {
                    var nextCall = getNextCall();

                    if (typeof onComplete === 'function') {
                        onComplete();
                    }

                    me.ongoingCall = null;

                    if (me.running && nextCall) {
                        me.call(nextCall);
                    }
                    else if (!me.hasCalls()) {
                        fireOnDoneCallbacks();
                        me.stop();
                    }
                } catch (e) {
                    console.log('error:', e);
                }
            };
        };

        /**
         * Removes the next call in the queue and returns it.
         * 
         * @return {Object|Undefined}
         */
        var getNextCall = function () {
            var callObj;

            if (me.hasCalls()) {
                callObj = me.calls.shift();
                callObj.onComplete = onCallComplete(callObj);
            }

            return callObj;
        };

        /**
         * The name of the chain.
         *
         * @type {String}
         */
        me.name = null;

        /**
         * The queued calls.
         *
         * @type {Array}
         */
        me.calls = [];

        /**
         * Whether or not the chain is running.
         * 
         * @type {Boolean}
         */
        me.running = false;

        /**
         * A call that is currently in progress.
         * 
         * @type {Object}
         */
        me.ongoingCall = null;

        /**
         * Callbacks to fire when the queue has finished.
         * 
         * @type {Array}
         */
        me.onDoneCallbacks = [];

        /**
         * Runs through the entire chain of calls.
         *
         * @return {this}
         */
        me.run = function () {
            if (this.hasCalls() && !this.running) {
                this.running = true;
                this.call(getNextCall());
            }

            return this;
        };

        /**
         * Destroys the instance.
         */
        me.destroy = function () {
            init = null;
            onCallComplete = null;
            getNextCall = null;
            
            this.$nullify();
        };

        init(config);
    },

    proto: {
        /**
         * Returns [true] if the chain has any calls left in the queue, otherwise [false].
         * 
         * @return {Boolean}
         */
        hasCalls: function () {
            return this.calls.length > 0;
        },

        /**
         * Returns the call at the specified index. Note that the length of [this.queue] is changing when the chain is 
         * running.
         * 
         * @param  {Number} index
         * @return {Object|Undefined}
         */
        getCall: function (index) {
            return this.calls[index];
        },

        /**
         * Stops the queue from running.
         *
         * @returns {this}
         */
        stop: function () {
            this.running = false;

            return this;
        },

        /**
         * Queues an AJAX call.
         *
         * @param {Object} obj
         * @param {String} obj.url - The URL to send the message to.
         * @param {Object} [obj.data] - An object with data to send with the call.
         * @param {Function} [obj.success] - A callback when the AJAX call has been successful.
         * @param {Function} [obj.failure] - A callback when the AJAX call has failed.
         * @param {Function} [obj.complete] - A callback when the AJAX call has completed.
         * @return {this}
         */
        queue: function (obj) {
            if (obj.hasOwnProperty('url') && typeof obj.url === 'string') {
                this.calls.push(obj);
            }

            return this;
        },

        /**
         * Makes an AJAX call.
         *
         * @param {Object} callObj - The AJAX request. This object can hold any information relevant to the call.
         * @param {String} callObj.url - Where the call should be pointed to.
         * @param {*} [callObj.parameters] - Data that should be sent with the call.
         * @param {Function} [callObj.onSuccess] - A callback when the AJAX call has been successful.
         * @param {Function} [callObj.onFailure] - A callback when the AJAX call has failed.
         * @param {Function} [callObj.onComplete] - A callback when the AJAX call has completed.
         * @returns {this}
         */
        call: function (callObj) {
            if (this.ongoingCall === null && callObj) {
                this.ongoingCall = {
                    call: callObj,
                    xhr: new Ajax.Request(callObj.url, callObj)
                };
            }

            return this;
        },

        /**
         * Returns the running state of the chain.
         *
         * @returns {Boolean}
         */
        isRunning: function () {
            return this.running;
        },

        /**
         * Adds a callback to be fired when a run of the queue is complete. [once] needs to be [true] each time you try 
         * to add the same callback if you only want it to be added once.
         * 
         * @param  {Function} callback
         * @param  {Boolean} once - Whether or not to add the callback only once.
         * @return {this} 
         */
        onDone: function (callback, once) {
            var i = 0;
            var len = this.onDoneCallbacks.length;
            var exists = false;

            if (typeof callback === 'function') {
                if (once) {
                    while (i < len) {
                        if (this.onDoneCallbacks[i] === callback) {
                            exists = true;
                            break;
                        }

                        i += 1;
                    }
                }

                if (!exists) {
                    this.onDoneCallbacks.push(callback);
                }
            }

            return this;
        }
    }
});
