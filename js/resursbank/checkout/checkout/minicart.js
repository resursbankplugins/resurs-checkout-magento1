/**
 * Copyright 2016 Resurs Bank AB
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

Classy.define({
    name: 'Minicart',

    init: function Minicart (config, me) {
        /**
         * Initializer.
         *
         * @param {Object} config
         * @param {Element} config.el
         */
        var init = function (config) {
            me.$apply(config);
        };

        /**
         * The product row.
         *
         * @type {Element}
         */
        me.el = null;

        /**
         * Takes a string, strips it of tags and replaces the content of the shipping element with the html in the
         * string.
         *
         * @param {String} content
         * @returns {me}
         */
        me.updateHtml = function (content) {
            if (me.el && typeof content === 'string') {
                me.el.update(content.stripScripts());
            }

            return me;
        };

        /**
         * Updates the count of the minicart (The number inside of the "()" next to the minicart).
         *
         * @param {number|string} newCount
         * @returns {me}
         */
        me.updateCount = function (newCount) {
            var countEl;

            if (me.el) {
                countEl = me.el.up('.header-minicart').select('a[data-target-element="#header-cart"] span.count')[0];

                if (countEl) {
                    countEl.update(newCount + '');
                }
            }

            return me;
        };

        /**
         * Destroys the instance.
         */
        me.destroy = function () {
            me.$nullify();
        };

        init(config);
    }
});
