/**
 * Copyright 2016 Resurs Bank AB
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

Classy.define({
    name: 'Discount',

    init: function Discount (config, me) {
        /**
         * Initializer.
         *
         * @param {Object} config
         * @param {Element} config.el
         */
        var init = function (config) {
            me.$apply(config);

            me.ajaxLoader = Classy.create('AjaxLoader', {});

            me.invalidCodeMessage = Classy.create('ErrorMessage', {
                text: 'Invalid coupon code.'
            });

            me.discountCodeEl = $('current-coupon-code');

            me.discountInput = $('coupon_code');
            me.discountInput.observe('blur', me.pushCode);

            me.discountForm = $('discount-coupon-form');
            me.discountForm.observe('submit', function (event) {
                event.preventDefault();
            });
        };

        /**
         * Whether or not the input is disabled.
         *
         * @type {boolean}
         */
        var disabled = false;

        /**
         * The previously entered code.
         *
         * @type {string}
         */
        var previousCode = '';

        /**
         * The discount coupon list.
         *
         * @type {Element}
         */
        me.el = null;

        /**
         * The discount input element.
         *
         * @type {DiscountInput}
         */
        me.discountInput = null;

        /**
         * The discount form element.
         *
         * @type {null}
         */
        me.discountForm = null;

        /**
         * An ajax loader for ajax calls.
         *
         * @type {AjaxLoader}
         */
        me.ajaxLoader = null;

        /**
         * A message about the invalid code that was entered.
         *
         * @type {ErrorMessage}
         */
        me.invalidCodeMessage = null;

        /**
         * The applied discount block.
         * 
         * @type {Element}
         */
        me.discountCodeEl = null;

        /**
         * Takes a string, strips it of tags and replaces the content of the discount element with the html in the
         * string.
         *
         * @param {String} content
         * @returns {me}
         */
        me.updateHtml = function (content) {
            if (me.discountCodeEl && typeof content === 'string') {
                me.discountCodeEl.update(content.stripScripts());
            }

            return me;
        };

        /**
         * Sends the discount code of the input element to the server with an AJAX call. On success, it will update the
         * mini cart with new HTML inside of #header-cart.
         *
         * @returns {me}.
         */
        me.pushCode = function () {
            var code = me.getCode();

            if (!disabled && code !== previousCode) {
                me.disable();
                me.showLoader();

                RESURSBANK.Checkout.AjaxQ.queue({
                    chain: 'resursbank-checkout',
                    url: RESURSBANK.Checkout.$call('getBaseUrl') + 'cart/coupon/coupon_code/' + code,
                    parameters: {
                        coupon_code: code,
                        form_key: RESURSBANK.Checkout.$call('getFormKey')
                    },

                    onSuccess: function (response) {
                        var data = response.responseJSON;

                        if (data.message.error.length) {
                            me.updateHtml('');
                            me.showInvalidCodeMessage();
                        }
                        else {
                            me.$emit('pushed', data);
                        }
                    },

                    onFailure: function (response) {
                        var data = response.responseJSON;
                    },

                    onComplete: function () {
                        me.enable();
                        me.hideLoader();

                        previousCode = code;
                    }
                }).run('resursbank-checkout');
            }

            return me;
        };

        /**
         * Returns the value of the discount input element.
         *
         * @returns {Number}
         */
        me.getCode = function () {
            return me.discountInput.value;
        };

        /**
         * Disables the discount input element and stops observing it of changes.
         *
         * @returns {me}.
         */
        me.disable = function () {
            if (!disabled) {
                disabled = true;
                me.discountInput.disabled = true;
                me.discountInput.stopObserving('blur', me.pushCode);
            }

            return me;
        };

        /**
         * Enables the discount input element and observes it of changes.
         *
         * @returns {me}.
         */
        me.enable = function () {
            if (disabled) {
                disabled = false;
                me.discountInput.disabled = false;
                me.discountInput.observe('blur', me.pushCode);
            }

            return me;
        };

        /**
         * Display invalid code message.
         *
         * @returns {me}
         */
        me.showInvalidCodeMessage = function () {
            if (me.invalidCodeMessage.shown) {
                me.invalidCodeMessage.$call('stop');
                me.invalidCodeMessage.el.remove();
            }

            me.discountCodeEl.insert({
                before: me.invalidCodeMessage.el
            });

            me.invalidCodeMessage.$call('run', 5000, function () {
                me.invalidCodeMessage.el.remove();
            });

            return me;
        };

        /**
         * Replaces the input element with the ajax loader.
         *
         * @returns {me}
         */
        me.showLoader = function () {
            me.discountInput.replace(me.ajaxLoader.el);

            return me;
        };

        /**
         * Replaces the ajax loader with the input element.
         *
         * @returns {me}
         */
        me.hideLoader = function () {
            me.ajaxLoader.el.replace(me.discountInput);

            return me;
        };

        /**
         * Destroys the instance.
         */
        me.destroy = function () {
            init = null;

            me.ajaxLoader.$call('destroy');
            me.$nullify();
        };

        init(config);
    }
});
