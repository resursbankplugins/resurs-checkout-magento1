/**
 * Copyright 2016 Resurs Bank AB
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

Classy.define({
    name: 'PaymentMethod',

    init: function PaymentMethod (me) {
        /**
         * Previously pushed payment data.
         * 
         * @type {Object}
         */
        var previousPaymentData = null;

        /**
         * Sends the payment method to the server with an AJAX call.
         *
         * @param {Object} data
         * @return {me}
         */
        me.push = function (data) {
            data.form_key = RESURSBANK.Checkout.$call('getFormKey');
            data.payment = JSON.stringify(data.payment);

            if (previousPaymentData !== data.payment) {
                previousPaymentData = data.payment;

                RESURSBANK.Checkout.AjaxQ.queue({
                    chain: 'resursbank-checkout',
                    url: RESURSBANK.Checkout.$call('getBaseUrl') + 'index/savePayment',
                    parameters: data,

                    onSuccess: function (response) {
                        var data = response.responseJSON;

                        if (data.message.error.length) {
                            me.$emit('push-error', data);
                        } else {
                            me.$emit('pushed');
                        }
                    },

                    onFailure: function (response) {
                        var data = response.responseJSON;
                    }
                }).run('resursbank-checkout');
            }

            return me;
        };

        /**
         * Prepares the users payment method to be sent to the server. Can alter names and add properties
         * that the server expects to get.
         *
         * @param {Object} data
         * @returns {Object}
         */
        me.prepareData = function (data) {
            return {
                payment: {
                    method: data.method
                }
            };
        };

        /**
         * Destroys the instance.
         */
        me.destroy = function () {
            previousPaymentData = null;

            me.$nullify();
        };
    }
});
