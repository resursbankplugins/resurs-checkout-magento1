/**
 * Copyright 2016 Resurs Bank AB
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

Classy.define({
    name: 'ShippingMethods',

    init: function ShippingMethods (config, me) {
        var previousShippingMethod = null;

        /**
         * Initializer.
         *
         * @param {Object} config
         * @param {Element} config.el
         */
        var init = function (config) {
            me.$apply(config);

            if (me.el) {
                me.setRadioHandlers();
            }
        };

        /**
         * The shipping methods list.
         *
         * @type {Element}
         */
        me.el = null;

        /**
         * The selected radio button.
         *
         * @type {Element}
         */
        me.selected = null;

        /**
         * Puts click handlers on the radio buttons. When clicking a radio button it will send the selected
         * shipping method to the server with an AJAX call.
         *
         * @returns {me}
         */
        me.setRadioHandlers = function () {
            me.getShippingMethodRadios()
                .each(function (radio) {
                    radio.observe('click', me.clickHandler);
                });

            return me;
        };

        /**
         * Click handler for the radio buttons.
         */
        me.clickHandler = function () {
            if (me.selected !== this) {
                me.select(this)
                    .push();
            }
        };

        /**
         * Sets the selected radio.
         *
         * @param {Element} radio The radio button to select.
         * @returns {me}
         */
        me.select = function (radio) {
            if (radio.checked && radio !== me.selected) {
                me.selected = radio;
            }

            return me;
        };

        /**
         * After an content update, this method selects the new radio button that has the same value as the old
         * radio button. If the option can't be found it will reset shipping.selected to null.
         *
         * @returns {me}
         */
        me.resetSelection = function () {
            var currentSelectedValue = null;

            if (me.selected) {
                currentSelectedValue = me.selected.value;
                me.selected = null;

                me.getShippingMethodRadios()
                    .each(function (radio) {
                        if (radio.checked || radio.value === currentSelectedValue) {
                            radio.checked = true;
                            me.select(radio);
                            throw $break;
                        }
                    });
            }

            return me;
        };

        /**
         * Takes a string, strips it of <script> tags and replaces the content of the shipping element with the html in the
         * string.
         *
         * @param {String} content - The new HTML.
         * @returns {me}
         */
        me.updateHtml = function (content) {
            if (me.el && typeof content === 'string') {
                me.el.update(content.stripScripts());
                me.setRadioHandlers()
                    .resetSelection();
            }

            return me;
        };

        /**
         * Returns an array of the shipping method radio buttons. If none are found, an empty array will be
         * returned.
         *
         * @returns {Array}
         */
        me.getShippingMethodRadios = function () {
            return me.el.select('input[name=shipping_method]') || [];
        };

        /**
         * Sends the selected shipping method to the server.
         *
         * @returns {me}
         */
        me.push = function () {
            if (me.selected && previousShippingMethod !== me.selected.value) {
                me.disable();

                previousShippingMethod = me.selected.value;

                RESURSBANK.Checkout.AjaxQ.queue({
                    chain: 'resursbank-checkout',
                    url: RESURSBANK.Checkout.$call('getBaseUrl') + 'index/saveShippingMethod',

                    parameters: {
                        shipping_method: me.selected.value,
                        form_key: RESURSBANK.Checkout.$call('getFormKey')
                    },

                    onSuccess: function (response) {
                        var data = response.responseJSON;
                    },

                    onFailure: function (response) {
                        var data = response.responseJSON;
                    },

                    onComplete: function () {
                        me.enable();
                    }
                })
                    .run('resursbank-checkout');
            }

            return me;
        };

        /**
         * Disables the radio buttons.
         *
         * @returns {me}.
         */
        me.disable = function () {
            me.getShippingMethodRadios().each(function (radio) {
                radio.disabled = true;
            });

            return me;
        };

        /**
         * Enables the radio buttons.
         *
         * @returns {me}.
         */
        me.enable = function () {
            me.getShippingMethodRadios().each(function (radio) {
                radio.disabled = false;
            });

            return me;
        };

        /**
         * Destroys the instance.
         */
        me.destroy = function () {
            previousShippingMethod = null;
            init = null;

            me.$nullify();
        };

        init(config);
    }
});
