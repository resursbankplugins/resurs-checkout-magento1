/**
 * Copyright 2016 Resurs Bank AB
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

Classy.define({
    name: 'Item',
    
    init: function Item (config, me) {
        /**
         * Initializer.
         * 
         * @param {Object} config
         * @param {Number} config.id
         * @param {Element} config.el
         */
        var init = function (config) {
            me.$apply(config);
            
            me.itemQuantity = Classy.create('ItemQuantity', {
                id: me.id,
                el: me.getQuantityInputElement()
            });

            me.itemPrices = Classy.create('ItemPrices', {
                id: me.id,
                els: me.getPriceElements()
            });

            me.itemRemoveButton = Classy.create('ItemRemoveButton', {
                id: me.id,
                el: me.getRemoveButtonElement()
            });
            
            me.itemQuantity.$on('pushed', me.onQuantityPush);
            me.itemQuantity.$on('changed', me.onQuantityChange);
            me.itemRemoveButton.$on('click', me.onRemoveButtonClick)
            me.itemRemoveButton.$on('pushed', me.onRemove);
        };

        /**
         * The ID of the item.
         * 
         * @type {Number}
         */
        me.id = null;

        /**
         * Element of the item.
         * 
         * @type {Element}
         */
        me.el = null;

        /**
         * If the item and it's child components are disabled.
         * 
         * @type {Boolean}
         */
        me.disabled = false;

        /**
         * The quantity input of the item.
         * 
         * @type {ItemQuantity}
         */
        me.itemQuantity = null;

        /**
         * The remove button of the item.
         * 
         * @type {ItemRemoveButton}
         */
        me.itemRemoveButton = null;

        /**
         * The price elements of the item.
         * 
         * @type {ItemPrices}
         */
        me.itemPrices = null;

        /**
         * Event handler for when quantity has been pushed to the server.
         * 
         * @param {Object} prices
         */
        me.onQuantityPush = function (prices, elements, cartQty) {
            me.itemPrices.$call('update', prices);
            me.$emit('quantity-updated', prices, elements, cartQty);
        };

        /**
         * Event handler for when quantity changes.
         * 
         * @param {Number} newQty
         */
        me.onQuantityChange = function (newQty) {
            if (newQty === 0) {
                me.itemRemoveButton.$call('push');
            } else {
                me.itemQuantity.$call('push', newQty);
            }
        };

        /**
         * Event handler for when the remove button is clicked.
         */
        me.onRemoveButtonClick = function () {
            me.disable();
        };

        /**
         * Event handler for when the item has been removed.
         * 
         * @param {Object} data
         */
        me.onRemove = function (data) {
            me.$emit('removed', me, data);
        };
         
        init(config);
    },

    proto: {
        /**
         * Returns the remove button of the item, or [undefined] it it can't be found.
         *
         * @return {Element|Undefined}
         */
        getRemoveButtonElement: function () {
            // 1.9 have 2 remove buttons. This is the correct button for 1.9
            var el = this.el.select('td.product-cart-remove a.btn-remove.btn-remove2')[0];

            // If there is no 1.9 button, then another version of Magento is used, so select this button instead.
            // A simple [legacySetup] check is not possible to discern between 1.x versions, due to it having only a
            // true/false value.
            if (!el) {
                el = this.el.select('td a.btn-remove.btn-remove2')[0];
            }

            return el;
        },

        /**
         * Returns the quantity input element of the item, or [undefined] it it can't be found.
         *
         * @return {Element|Undefined}
         */
        getQuantityInputElement: function () {
            var me = this;

            return RESURSBANK.Checkout.$call('getQuantityInputElements').filter(function (inputEl) {
                return me.el.contains(inputEl);
            })[0];
        },

        /**
         * Returns the price elements displayed for the item. If some elements doesn't exist, they will have the value
         * of [null].
         * 
         * @return {Object}
         */
        getPriceElements: function () {
            var displayedTax = RESURSBANK.Checkout.$call('getDisplayedTax');
            var priceEls = this.el.select('.cart-price');
            var els = {};

            if (displayedTax.type === displayedTax.both) {
                els.price = priceEls[1];
                els.priceExcl = priceEls[0];
                els.subtotal = priceEls[3];
                els.subtotalExcl = priceEls[2];
            } else if (displayedTax.type === displayedTax.incl) {
                els.price = priceEls[0];
                els.priceExcl = null;
                els.subtotal = priceEls[1];
                els.subtotalExcl = null;
            } else if (displayedTax.type === displayedTax.excl) {
                els.price = null;
                els.priceExcl = priceEls[0];
                els.subtotal = null;
                els.subtotalExcl = priceEls[1];
            }

            return els;
        },

        /**
         * Updates the prices based on the object passed in. Each property should be a string, which are the new 
         * content for the price elements.
         * 
         * @param  {Object} prices - The prices to update.
         * @param  {String} [prices.price] - Price including tax.
         * @param  {String} [prices.priceExcl] - Price excluding tax.
         * @param  {String} [prices.subtotal] - Subtotal including tax.
         * @param  {String} [prices.subtotalExcl] - Subtotal excluding tax.
         * @return {this}
         */
        updatePrices: function (prices) {
            this.itemPrices.$call('update', prices);

            return this;
        },

        /**
         * Disables the item and its components.
         * 
         * @return {this}
         */
        disable: function () {
            if (!this.disabled) {
                this.itemQuantity.$call('disable');
                this.itemRemoveButton.$call('disable');

                this.disabled = true;
            }

            return this;
        },

        /**
         * Enables the item and its components.
         * 
         * @return {this}
         */
        enable: function () {
            if (this.disabled) {
                this.itemQuantity.$call('enable');
                this.itemRemoveButton.$call('enable');
                this.disabled = false;
            }

            return this;
        },

        /**
         * Destroys the item instance and its components.
         */
        destroy: function () {
            this.itemQuantity.$ignore('updated');
            this.itemQuantity.$ignore('changed');

            this.itemRemoveButton.$call('push');
            this.itemRemoveButton.$call('destroy');
            this.itemQuantity.$call('destroy');
            this.itemPrices.$call('destroy');

            this.el.remove();

            this.$nullify();
        }
    }
});
