/**
 * Copyright 2016 Resurs Bank AB
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

Classy.define({
    name: 'ItemPrices',
    init: function ItemPrices (config, me) {
        /**
         * Initializer.
         *
         * @param {Object} config
         * @param {Number} config.id
         * @param {Object} config.els - The price elements.
         * @param {Element} config.els.price - Element. Price including tax.
         * @param {Element} config.els.priceExcl - Element. Price excluding tax.
         * @param {Element} config.els.subtotal - Element. Subtotal including tax.
         * @param {Element} config.els.subtotalExcl - Element. Subtotal excluding tax.
         */
        var init = function (config) {
            me.$apply(config);
        };

        /**
         * The ID of the item.
         * 
         * @type {Number}
         */
        me.id = null;

        /**
         * An object of price elements.
         * 
         * @type {Object}
         */
        me.els = null;

        init(config);
    },

    proto: {
        /**
         * Updates the prices. Takes an object and each property should be a string, which are the new content for the 
         * price elements.
         * 
         * @param  {Object} prices - The prices to update.
         * @param  {String} [prices.price] - Price including tax.
         * @param  {String} [prices.priceExcl] - Price excluding tax.
         * @param  {String} [prices.subtotal] - Subtotal including tax.
         * @param  {String} [prices.subtotalExcl] - Subtotal excluding tax.
         * @return {this}
         */
        update: function (prices) {
            if (prices.price) {
                this.updatePriceIncl(prices.price);
            }

            if (prices.priceExcl) {
                this.updatePriceExcl(prices.priceExcl);
            }

            if (prices.subtotal) {
                this.updateSubtotalIncl(prices.subtotal);
            }

            if (prices.subtotalExcl) {
                this.updateSubtotalExcl(prices.subtotalExcl);
            }

            return this;
        },

        /**
         * Updates the [this.els.price] element with new content.
         * 
         * @param  {String} content
         * @return {this}
         */
        updatePriceIncl: function (content) {
            if (this.els.price && typeof content === 'string') {
                this.els.price.update(content.stripScripts());
            }

            return this;
        },

        /**
         * Updates the [this.els.priceExcl] element with new content.
         * 
         * @param  {String} content
         * @return {this}
         */
        updatePriceExcl: function (content) {
            if (this.els.priceExcl && typeof content === 'string') {
                this.els.priceExcl.update(content.stripScripts());
            }

            return this;
        },

        /**
         * Updates the [this.els.subtotal] element with new content.
         * 
         * @param  {String} content
         * @return {this}
         */
        updateSubtotalIncl: function (content) {
            if (this.els.subtotal && typeof content === 'string') {
                this.els.subtotal.update(content.stripScripts());
            }

            return this;
        },

        /**
         * Updates the [this.els.subtotalExcl] element with new content.
         * 
         * @param  {String} content
         * @return {this}
         */
        updateSubtotalExcl: function (content) {
            if (this.els.subtotalExcl && typeof content === 'string') {
                this.els.subtotalExcl.update(content.stripScripts());
            }

            return this;
        },

        /**
         * Destroys the item instance and its components.
         */
        destroy: function () {
            this.$nullify();
        }
    }
});
