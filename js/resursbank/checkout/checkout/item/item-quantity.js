/**
 * Copyright 2016 Resurs Bank AB
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

Classy.define({
    name: 'ItemQuantity', 
    init: function ItemQuantity (config, me) {
        /**
         * Initializer.
         *
         * @param {Object} config
         * @param {Number} config.id
         * @param {Element} config.el
         */
        var init = function (config) {
            me.$apply(config);

            me.setPreviousQuantity(me.getQuantity());
            me.el.observe('change', me.onChange);

            me.invalidQuantityMessage = Classy.create('ErrorMessage', {
                text: Translator.translate('Invalid quantity')
            });

            me.ajaxLoader = Classy.create('AjaxLoader', {});
        };

        /**
         * The ID of the item.
         * 
         * @type {Number}
         */
        me.id = null;

        /**
         * The quantity input element.
         * 
         * @type {Element}
         */
        me.el = null;

        /**
         * The previous quantity.
         * 
         * @type {Number}
         */
        me.previousQuantity = null;

        /**
         * If the quantity input is disabled.
         * 
         * @type {Boolean}
         */
        me.disabled = false;

        /**
         * A message to display when the user inputs an invalid quantity.
         *
         * @type {Message}
         */
        me.invalidQuantityMessage = null;

        /**
         * An ajax loader for when doing ajax calls.
         *
         * @type {AjaxLoader}
         */
        me.ajaxLoader = null;

        /**
         * On change handler for when the user alters the quantity.
         */
        me.onChange = function () {
            me.$emit('changed', me.getQuantity(), me.previousQuantity);
        };

        init(config);
    },

    proto: {
        /**
         * Disables the quantity input element.
         * 
         * @return {this}
         */
        disable: function () {
            if (!this.disabled) {
                this.disabled = true;
                this.el.disabled = true;
                this.el.stopObserving('change', this.onChange);
            }

            return this;
        },

        /**
         * Enables the quantity input element.
         * 
         * @return {this}
         */
        enable: function () {
            if (this.disabled) {
                this.disabled = false;
                this.el.disabled = false;
                this.el.observe('change', this.onChange);
            }

            return this;
        },

        /**
         * Returns the value of the input element as a number.
         * 
         * @return {Number}
         */
        getQuantity: function () {
            return parseFloat(this.el.value);
        },

        /**
         * Converts the [value] argument to a number and sets the input element's value to that.
         * 
         * @param {number} value - A value that can be converted to a number.
         * @return {this}
         */
        setQuantity: function (value) {
            this.el.value = parseFloat(value);

            return this;
        },

        /**
         * Sets [this.previousQuantity] to [value] after it has been converted to a number.
         * 
         * @param {number} value
         * @return {this}
         */
        setPreviousQuantity : function (value) {
            this.previousQuantity = parseFloat(value);

            return this;
        },

        /**
         * Sends the quantity to the server.
         * 
         * @param  {number} quantity
         * @return {this}
         */
        push: function (quantity) {
            var me = this;

            if (!this.disabled && !isNaN(quantity) && quantity !== 0) {
                me.disable();
                me.showLoader();

                RESURSBANK.Checkout.AjaxQ.queue({
                    url: RESURSBANK.Checkout.$call('getBaseUrl') + 'cart/update/id/' + me.id + '/qty/' + quantity,
                    chain: 'resursbank-checkout',
                    parameters: {
                        itemId: me.id,
                        itemQuantity: quantity,
                        form_key: RESURSBANK.Checkout.$call('getFormKey')
                    },

                    onSuccess: function (response) {
                        var data = response.responseJSON;

                        me.hideLoader();

                        if (data.message.error.length) {
                            me.setQuantity(me.previousQuantity);
                            me.showInvalidQuantityMessage();
                        } else {
                            me.$emit(
                                'pushed',
                                {
                                    subtotal: data.item_total,
                                    subtotalExcl: data.item_total_excl_tax,
                                    price: data.item_price,
                                    priceExcl: data.item_price_excl_tax
                                },
                                data.elements,
                                data.cart_qty
                            );

                            me.setPreviousQuantity(quantity);
                        }
                    },

                    onFailure: function (response) {
                        me.setQuantity(me.previousQuantity);
                        me.hideLoader();
                        me.showInvalidQuantityMessage();
                    },

                    onComplete: function (response) {
                        me.enable();
                    }
                }).run('resursbank-checkout');
            }

            return me;
        },

        /**
         * Shows the message related to an invalid quantity. The message will be shown by the quantity input.
         *
         * @returns {this}
         */
        showInvalidQuantityMessage: function () {
            var me = this;

            if (me.invalidQuantityMessage.shown) {
                me.invalidQuantityMessage.$call('stop');
                me.invalidQuantityMessage.el.remove();
            }

            me.el.insert({
                after: me.invalidQuantityMessage.el
            });

            me.invalidQuantityMessage.$call('run', 5000, function () {
                me.invalidQuantityMessage.el.remove();
            });

            return me;
        },

        /**
         * Replaces the input element with the ajax loader.
         *
         * @returns {this}
         */
        showLoader: function () {
            this.el.replace(this.ajaxLoader.el);

            return this;
        },

        /**
         * Replaces the ajax loader with the input element.
         *
         * @returns {this}
         */
        hideLoader: function () {
            this.ajaxLoader.el.replace(this.el);

            return this;
        },

        /**
         * Destroys the instance.
         */
        destroy: function () {
            this.$nullify();
        }
    }
});
