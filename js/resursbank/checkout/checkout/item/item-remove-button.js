/**
 * Copyright 2016 Resurs Bank AB
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

Classy.define({
    name: 'ItemRemoveButton',

    init: function ItemRemoveButton (config, me) {
        /**
         * Initializer.
         *
         * @param {Object} config
         * @param {Number} config.id
         */
        var init = function (config) {
            me.$apply(config);

            me.el.observe('click', me.onClick);
            me.ajaxLoader = Classy.create('AjaxLoader', {});
        };

        /**
         * The ID of the item.
         * 
         * @type {Number}
         */
        me.id = null;

        /**
         * The remove button.
         * 
         * @type {Element}
         */
        me.el = null;

        /**
         * Whether or not this button is disabled.
         * 
         * @type {Boolean}
         */
        me.disabled = false;

        /**
         * An ajax loader for when doing ajax calls.
         *
         * @type {AjaxLoader}
         */
        me.ajaxLoader = null;

        /**
         * Handler for when the button is clicked.
         * 
         * @param {Event} event
         */
        me.onClick = function (event) {
            event.preventDefault();

            if (!me.disabled) {
                me.push();
            }
        };

        /**
         * Destroys the instance.
         */
        me.destroy = function () {
            init = null;

            me.el.stopObserving('click', me.onClick);
            me.$nullify();
        }

        init(config);
    },

    proto: {
        /**
         * Disables the button.
         *
         * @return {this}
         */
        disable: function () {
            if (!this.disabled) {
                this.disabled = true;
            }

            return this;
        },

        /**
         * Enables the button.
         * 
         * @return {this}
         */
        enable: function () {
            if (this.disabled) {
                this.disabled = false;
            }

            return this;
        },

        /**
         * Pushes the removal of the item to the server.
         * 
         * @return {this}
         */
        push: function () {
            var me = this;
            var baseUrl = RESURSBANK.Checkout.$call('getBaseUrl');

            if (!me.disabled) {
                me.disable();
                me.showLoader();

                RESURSBANK.Checkout.AjaxQ.queue({
                    chain: 'resursbank-checkout',
                    url: baseUrl + 'cart/delete/id/' + me.id,

                    parameters: {
                        itemId: me.id,
                        form_key: RESURSBANK.Checkout.$call('getFormKey')
                    },

                    onSuccess: function (response) {
                        var data = response.responseJSON;

                        if (data.message.error.length) {
                            me.enable();
                        }
                        else if (data.cart_qty === 0) {
                            location.href = baseUrl;
                        }

                        me.hideLoader();
                        me.$emit('pushed', data);
                    },

                    onFailure: function (response) {
                        var data = response.responseJSON;

                        alert("Sorry, but we can't remove the product at this moment. Please refresh and try again.");

                        me.enable();
                        me.hideLoader();
                    },

                    onComplete: function () {
                        me.enable();
                    }
                }).run('resursbank-checkout');
            }

            return me;
        },

        /**
         * Replaces the input element with the ajax loader.
         *
         * @returns {this}
         */
        showLoader: function () {
            this.el.replace(this.ajaxLoader.el);

            return this;
        },

        /**
         * Replaces the ajax loader with the input element.
         *
         * @returns {this}
         */
        hideLoader: function () {
            this.ajaxLoader.el.replace(this.el);

            return this;
        }
    }
});
