/**
 * Copyright 2016 Resurs Bank AB
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/*  ===================================================
    Product.Configurable
    =================================================== */
(function ($, Classy) {
    Classy.define({
        name: 'Product.Configurable',

        init: function ProductConfigurable (me) {
            var customOptions = Classy.create('Product.CustomOptions');

            /**
             * Returns all configurable option elements of the product page.
             * 
             * @returns {object} jQuery object.
             */
            me.getOptionElements = function () {
                return $('[id^="swatch"]');
            };

            /**
             * Returns the product prices for configurable products.
             * 
             * @returns {object}
             */
            me.getPrices = function () {
                // Price including tax is not calculated by Magento for selected configurable 
                // options.
                var tax = 1 + (optionsPrice.currentTax / 100);
                var config = optionsPrice.optionPrices.config;
                
                return {
                    excl: optionsPrice.priceExclTax + (config ? config.price : 0),
                    incl: optionsPrice.priceInclTax + (config ? config.price : 0) * tax
                };
            };
            
            /**
             * Returns the calculated product prices. This would be the prices that is displayed
             * on the product page. Since it seems that there isn't a way to programmatically get
             * these numbers by using Magento's native objects and methods, this function exists.
             * 
             * @returns {object} An object with the calculated product prices.
             */
            me.getCalculatedPrices = function () {
                var prices = me.getPrices();
                var customOptionPrices = customOptions.getAccumulatedPrices();
                
                return {
                    excl: prices.excl + customOptionPrices.excl,
                    incl: prices.incl + customOptionPrices.incl
                };
            };
        }
    });
}(
    jQuery, 
    RESURSBANK.Classy
));
