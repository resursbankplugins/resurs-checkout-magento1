/**
 * Copyright 2016 Resurs Bank AB
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/*  ===================================================
    Product.Type
    =================================================== */
window.addEventListener('load', function () {
    var productType = RESURSBANK.productType;

    Classy.define({
        name: 'Product.Type',

        init: function ProductType (me) {
            /**
             * Returns the product type as a string.
             * 
             * @returns {string}
             */
            me.get = function () { 
                return productType;
            };

            /**
             * Returns a boolean value that states whether the viewed product is a
             * bundled product.
             * 
             * @returns {boolean}
             */
            me.isBundle = function () {
                return productType === 'bundle';
            };

            /**
             * Returns a boolean value that states whether the viewed product is a 
             * configurable product.
             * 
             * @returns {boolean}
             */
            me.isConfigurable = function () {
                return productType === 'configurable';
            };

            /**
             * Returns a boolean value that states whether the viewed product is a 
             * simple product.
             * 
             * @returns {boolean}
             */
            me.isSimple = function () {
                return productType === 'simple';
            };

            /**
             * Returns a boolean value that states whether the viewed product is a 
             * grouped product.
             * 
             * @returns {boolean}
             */
            me.isGrouped = function () {
                return productType === 'grouped';
            };

            /**
             * Returns a boolean value that states whether the viewed product
             * is a downloadable product.
             *
             * @returns {boolean}
             */
            me.isDownloadable = function () {
                return productType === 'downloadable';
            };

            /**
             * Returns a boolean value that states whether the viewed product
             * is a virtual product.
             *
             * @returns {boolean}
             */
            me.isVirtual = function () {
                return productType === 'virtual';
            };
        }
    });
}, false);