/**
 * Copyright 2016 Resurs Bank AB
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

Classy.define({
    name: 'Product',

    init: function Product (config, me) {
        /**
         * Initializer.
         *
         * @param {object} config
         * @param {string} config.productType
         */
        var init = function (config) {
            type = config.productType;
        };

        /**
         * The product type (simple, configurable, grouped, bundle).
         * 
         * @type {string}
         */
        var type = '';

        /**
         * Takes an object and checks if it's empty or not.
         * 
         * @param {object} obj
         * @returns {boolean}
         */
        me.isEmptyObject = function (obj) {
            return Object.keys(obj).length === 0;
        }

        /**
         * Returns all custom option elements of the product page.
         * 
         * @returns {object} jQuery object.
         */
        me.getCustomOptionElements = function () {
            return $('.product-custom-option');
        };

        /**
         * Returns all bundle elements of the product page.
         * 
         * @returns {object} jQuery object.
         */
        me.getAllBundleElements = function () {
            return me.getBundleOptionElements().add(me.getBundleQtyElements());
        }

        /**
         * Returns all bundle option elements of the product page.
         * 
         * @returns {object} jQuery object.
         */
        me.getBundleOptionElements = function () {
            return $('.change-container-classname');
        }

        /**
         * Returns all bundle quantity elements of the product page.
         * 
         * @returns {object} jQuery object.
         */
        me.getBundleQtyElements = function () {
            return $('[name^="bundle_option_qty["');
        };

        /**
         * Returns all configurable option elements of the product page.
         * 
         * @returns {object} jQuery object.
         */
        me.getConfigurableOptionElements = function () {
            return $('[id^="swatch"]');
        }

        /**
         * Checks if the viewed product has any custom options.
         * 
         * @returns {boolean}
         */
        me.hasCustomOptions = function () {
            return window.hasOwnProperty('opConfig') &&
                window.opConfig instanceof Product.Options &&
                window.opConfig.hasOwnProperty('config') &&
                !me.isEmptyObject(window.opConfig.config);
        };

        /**
         * Returns a boolean value that states whether the viewed product is a
         * bundled product.
         * 
         * @returns {boolean}
         */
        me.isBundle = function () {
            return type === 'bundle';
        };

        /**
         * Returns a boolean value that states whether the viewed product is a 
         * configurable product.
         * 
         * @returns {boolean}
         */
        me.isConfigurable = function () {
            return type === 'configurable';
        };

        /**
         * Returns a boolean value that states whether the viewed product is a 
         * grouped product.
         * 
         * @returns {boolean}
         */
        me.isGrouped = function () {
            return type === 'grouped';
        };

        /**
         * Returns a boolean value that states whether the viewed product is a 
         * simple product.
         * 
         * @returns {boolean}
         */
        me.isSimple = function () {
            return type === 'simple';
        };

        /**
         * Returns the product type as a string.
         * 
         * @returns {string}
         */
        me.getType = function () {
            return type;
        };

        /**
         * Returns the calculated product prices. This would be the prices that is displayed
         * on the product page. Since it seems that there isn't a way to programmatically get
         * these numbers by using Magento's native objects and methods, this function exists.
         * 
         * @returns {object} An object with the calculated product prices.
         */
        me.getCalculatedProductPrices = function () {
            var productPrices = me.getProductPrices();
            var customOptionPrices = me.getAccumulatedCustomOptionPrices();

            return {
                excl: productPrices.excl + customOptionPrices.excl,
                incl: productPrices.incl + customOptionPrices.incl
            };
        };

        /**
         * Returns an object with the product's price, both including and excluding tax.
         * Takes product type into account, and also if bundled products have dynamic or
         * fixed pricing.
         * 
         * @returns {object} An object with the product price (including and excluding tax).
         */
        me.getProductPrices = function () {
            return me.isBundleProduct() ?
                me.getBundledProductPrices() :
                me.isConfigurableProduct() ?
                me.getConfigurableProductPrices() :
                me.getSimpleProductPrices();
        };

        /**
         * Returns the product prices for simple products.
         * 
         * @returns {object}
         */
        me.getSimpleProductPrices = function () {
            return {
                excl: optionsPrice.priceExclTax,
                incl: optionsPrice.priceInclTax
            };
        };

        /**
         * Returns the product prices for bundled products. Works for products with both
         * dynamic or fixed pricing.
         * 
         * @returns {object}
         */
        me.getBundledProductPrices = function () {
            var opPrices = optionsPrice.optionPrices;
            
            return {
                // Adding [optionsPrice.priceInclTax] and [optionsPrice.priceExclTax] to 
                // calculate correct prices for bundled products with fixed pricing.
                excl: opPrices.bundle + optionsPrice.priceInclTax,
                incl: opPrices.priceInclTax + optionsPrice.priceExclTax
            };
        };

        /**
         * Returns the product prices for configurable products.
         * 
         * @returns {object}
         */
        me.getConfigurableProductPrices = function () {
            // Price including tax is not calculated by Magento for selected configurable 
            // options.
            var tax = 1 + (optionsPrice.currentTax / 100);
            var config = optionsPrice.optionPrices.config;
            
            return {
                excl: optionsPrice.priceExclTax + (config ? config.price : 0),
                incl: optionsPrice.priceInclTax + (config ? config.price : 0) * tax
            }
        };

        /**
         * Calculates the accumulated prices (including and excluding tax) of the selected custom
         * options and returns them in an object with the properties "incl" and "excl".
         * 
         * @returns {object} An object with accumulated prices for both including and excluding
         * tax of the selected custom options. It has the properties "incl" and "excl".
         */
        me.getAccumulatedCustomOptionPrices = function () {
            var customPrices = optionsPrice.customPrices;
            var customPricesArr = Object.keys(customPrices);

            return {
                excl: customPricesArr.reduce(function (acc, key) {
                    return acc + (customPrices[key].hasOwnProperty('excludeTax') ? 
                        customPrices[key].excludeTax : 0);
                }, 0),

                incl: customPricesArr.reduce(function (acc, key) {
                    return acc + (customPrices[key].hasOwnProperty('includeTax') ? 
                        customPrices[key].includeTax : 0)
                }, 0)
            };
        };

        init(config);
    }
});