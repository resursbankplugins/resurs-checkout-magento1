/**
 * Copyright 2016 Resurs Bank AB
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/*  ===================================================
    Product.CustomOptions
    =================================================== */
(function ($, Classy) {
    Classy.define({
        name: 'Product.CustomOptions',

        init: function ProductCustomOptions (me) {
            /**
             * Returns all custom option elements of the product page.
             * 
             * @returns {object} jQuery object.
             */
            me.getElements = function () {
                return $('.product-custom-option');
            };

            /**
             * Checks if the viewed product has any custom options.
             * 
             * @returns {boolean}
             */
            me.hasCustomOptions = function () {
                return window.hasOwnProperty('opConfig') &&
                    window.opConfig instanceof Product.Options &&
                    window.opConfig.hasOwnProperty('config')
                    // !isEmptyObject(window.opConfig.config);
            };

            /**
             * Calculates the accumulated prices (including and excluding tax) of the selected custom
             * options and returns them in an object with the properties "incl" and "excl".
             * 
             * @returns {object} An object with accumulated prices for both including and excluding
             * tax of the selected custom options. It has the properties "incl" and "excl".
             */
            me.getAccumulatedPrices = function () {
                var customPrices = optionsPrice.customPrices;
                var customPricesArr = Object.keys(customPrices);

                return {
                    excl: customPricesArr.reduce(function (acc, key) {
                        return acc + (customPrices[key].hasOwnProperty('excludeTax') ? 
                            customPrices[key].excludeTax : 0);
                    }, 0),

                    incl: customPricesArr.reduce(function (acc, key) {
                        return acc + (customPrices[key].hasOwnProperty('includeTax') ? 
                            customPrices[key].includeTax : 0)
                    }, 0)
                };
            };
        }
    });
}(
    jQuery, 
    RESURSBANK.Classy
));
