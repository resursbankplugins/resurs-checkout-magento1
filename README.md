# Resurs Bank - Magento 1 module - Checkout

## Description

A payment and checkout module from Resurs Bank.

## Upgrading

If you use version 3.5.0.6 or lower and upgrading to 3.5.0.7 or higher, make sure that you remove lib/Resursbank/Checkout/RBEcomPHP since that sub structure belongs to an older version of EComPHP (v1.1.x).

---
### How EComPHP is deployed

This is a sample script of how ecomphp are patched up to the later versions of EComPHP for this module. In short, to synchronize all dependencies we use composer. After the upgrade is done, we remove all traces of .git directories so we will be able to add the vendor requirements as a bulk in the base structure.

    #!/bin/bash
    echo "Preparing library structure..."
    mkdir -p lib/Resursbank/Checkout >/dev/null 2>&1
    cd lib/Resursbank/Checkout
    echo "Clean up cache ..."
    composer clear >/dev/null 2>&1
    echo "Upgrading vendor by removal..."
    rm -fr vendor
    composer require resursbank/ecomphp
    composer up
    echo "Cleaning up unnecessary ignores ..."
    find vendor/ -name .gitignore -exec rm -vf {} \; >/dev/null 2>&1
    echo "Cleaning up .git structures ..."
    find vendor/ -type d -name .git -exec rm -rvf {} \; >/dev/null 2>&1
    echo "Cleaning up tests..."
    find vendor/ -type d -name tests -exec rm -rvf {} \; >/dev/null 2>&1


## Changelog

#### 3.5.0.0

* Address fetching now only works with a Swedish API account.
* SSN validation is now always based on the country supplied in the module
configuration.
* Removed all fields from payment methods when using Hosted Flow.
* Part payment feature now supports the **bundle** product type.
* The modal with part payment information (displayed when clicking the
"Read More" button on product pages) is no longer cached and accounts for all
applied product options (for all product types).
* Part payment configuration is now made distinctive by applied API account.
* New database structure added to store account information, along with payment
methods and annuity factors.
* The system which generated payment method models in the local code pool
(app/code/local/Resursbank) has been replaced with a rewrite of
Mage_Payment_Helper_Data. All payment methods now use the same model
(Resursbank_Checkout_Model_Payment_Method_Resursbank_Default).
* Callback salt key file is no longer generated. Salt values are stored with
the account information in the resursbank_checkout_account table.
* Fixed problem with 0 sum orders not executing the BOOKED callback upon
reaching the success page.
* Improved support for multi-store configurations using individual API
credentials.
* Fixed problem with part payment prices not being calculated accurately when
exceeding 3 digit numbers.
* Payment methods are now filtered on frontend in accordance with the account
configured for the corresponding store view.
* Changed order of part payment settings on configuration page.
* Removed observer which automatically registered callback URLs.
* Removed old obsolete code.
 
**NOTE: After updating from an earlier version of the module you will need to
sync payment methods and register callbacks manually from the administration 
panel because of the changes described above. You should also remove the
directory app/code/local/Resursbank and the file var/.resursbank_key since they
are no longer useful.**

#### 3.5.0.5

* Fixed part payment price calculation for products with dynamic options.

#### 3.5.0.6

* Fixed afterShop issue with substores.
* Updated EComPHP to v1.1.50

#### 3.5.0.7

* Updated EComPHP to v1.3 with auto deployments.

#### 3.5.0.8

* Updated CallbackController to make independent payment statuses instead of trusting callbacks.
* Updated CallbackController to handle statuses bitwise (to avoid statuses like 132, etc).

#### 3.5.0.9

* Ecomlibs update, moved to 3.5.1.1 due to a 3.5.1.0 premerge.

#### 3.5.1.0

* Added support for browser transition after signing payments.

#### 3.5.1.1

* Updated ecomlibs with PHP8 compliance updates (Was prepared for 3.5.0.9).

#### 3.5.1.2

* Updated multistore support for AfterShop flow methods.
